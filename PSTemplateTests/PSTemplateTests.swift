//
//  PSTemplateTests.swift
//  PSTemplateTests
//
//  Created by Edimax on 2021/12/23.
//

import XCTest
@testable import PSTemplate
import SwiftUI
import CryptoKit

class PSTemplateTests: XCTestCase {
    
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
//        addUIInterruptionMonitor(withDescription: "Allow ”Sophos” to use your location?", handler: { alert in
//          alert.buttons["Allow While Using App"].tap()
//          return true
//        })
//        addUIInterruptionMonitor(withDescription: "Sophos", handler: { alert in
//                  alert.buttons["Allow While Using App"].tap()
//                  return true
//                })
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
//        PSTemplate.AppTemplate.self
        
       // XCTAssertEqual(AppTemplate.init(presented: $isSmile))
       // testAddAccessPointsFlow()
       // AppTemplate.init(presented: $isSmile)
     
      //  let string = try view. .inspect().text().string()
       // XCTAssertEqual(string, "Hello, world!")
       
        
        
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            
        }
    }
    func testMain()
    {
        let a = MainViewModel()
        a.checkAddUserResult()
        CreateBody.init(name: "", job: "")
        CreateBodyResponse.init(name: "", job: "", id: "")
        SSDP.init(LOCATION: "", SERVER: "", ST: "", USN: "", Vendor: "", Mac: "", Model: "", Name: "", OperatorMode: "", Identity: "", FirmwareVersion: "", CPUUtilization: "", ConnectedUsers: "", SNR: "", MemoryUtilization: "", WiFiRadio: "", live: "", SOPHOS_INFO: "")
    }
    func testAccessModel()
    {
        MyAccessBody.init(appname: "", mac: "", health: "", client: "", location: "", central: "",wifiradio: "",model: "",unknow: 0)
        NewAccessResponse.init(appname: "", mac: "", health: "", client: "", id: "")
        MyAccessBody.init(appname: "", mac: "", health: "", client: "", location: "", central: "",wifiradio: "",model: "",unknow: 0)
        MyAccessResponse.init(appname: "", mac: "", health: "", client: "", id: "", location: "", central: "")
        login.init(admin_account: "", admin_pw: "", keep: 0)
    }
    func testSystemModel()
    {
        GETRebootresponse.init(error_code: 0, error_msg: "", data: nil)
        reboot.init(reboot_time: "")
        GETresponse.init(error_code: 0, error_msg: "", data: nil)
        InfoSystem.init(model_name: "", system_time: "", product_name: "", uptime: "", firmware_version: "", mac_address: "", management_vlan_id: 0, ip_address: "", default_gateway: "", dns_server: "", dhcp_server: "", ipv6_address: "", ipv6_link_local_address: "")
        SystemBody.init(name: "", value: "")
        GETAdministrationresponse.init(error_code: 0, error_msg: "", data: nil)
        POSTAdministrationresponse.init(error_code: 0, error_msg: "")
        Administration.init(account_name: "", account_security: "", account_confirm_security: "", product_name: "", https_port: 0, management_protocols: nil, snmp_settings: nil)
        managementProtocols.init(https: 0, telnet: 0, ssh: 0, snmp: 0)
        snmpSettings.init(snmp_version: 0, snmp_get_community: "", snmp_set_community: "", snmp_v3_name: "", snmp_v3_security: "", snmp_trap_enabled: 0, snmp_trap_community: "", snmp_trap_manager: "")
        GETLANresponse.init(error_code: 0, error_msg: "", data: nil)
        LANs.init(lanport: [nil], ipv6: nil, ipv4: nil)
        LANport.init(index: 0, duplex: 0, flow_control: 0, az_enabled: 0)
        IPv6.init(ipv6_enabled: 0, ipv6_type: 0, ipv6_predix_len: 0, ipv6_address: "", ipv6_gateway: "")
        IPv4.init(ipv4_type: 0, ipv4_address: "", ipv4_subnet_mask: "", ipv4_default_gateway_type: 0, ipv4_default_gateway: "", ipv4_dns_primary_type: 0, ipv4_dns_primary: "", ipv4_dns_secondary_type: 0, ipv4_dns_secondary: "", dhcp_enabled: 0, dhcp_lease_time: 0, dhcp_start_addr: "", dhcp_end_addr: "", dhcp_domain_name: "", dhcp_dns_primary: "", dhcp_dns_secondary: "")
        POSTLANresponse.init(error_code: 0, error_msg: "")
        LANBody.init(name: "", value: "")
        GET24GBandsresponse.init(error_code: 0, error_msg: "", data: nil)
        GET5GBandsresponse.init(error_code: 0, error_msg: "", data: nil)
        GET6GBandsresponse.init(error_code: 0, error_msg: "", data: nil)
        GET24GSSIDSecurityresponse.init(error_code: 0, error_msg: "", data: nil)
        GET5GSSIDSecurityresponse.init(error_code: 0, error_msg: "", data: nil)
        GET6GSSIDSecurityresponse.init(error_code: 0, error_msg: "", data: nil)
        Bands.init(radio_enable: 0, radio_band: 0, enabled_ssidNum: 0, ssidNames: nil, autochannel_enabled: 0, autochannel_range: 0, autochannel_interval: 0, autochannel_force_enabled: 0, channel: 0, channel_bandwidth: 0, bss_basicrateset: 0)
        SSID.init(ssidname: "", index: 0, vlan_id: 0)
        SSIDSecurity.init(ssid_index: 0, broadcast_ssid: 0, client_isolation: 0, ieee80211k: 0, ieee80211w: 0, load_balance: 0, auth_method: 0, auth_detail: nil, additional_auth: 0, mac_radius_auth: 0, mac_radius_password: "")
        AuthDetail.init(ieee80211r: 0, wpa_type: 0, encrypt_type: 0, wpa_rekey_time: 0, psk_type: 0, psk_value: "")
        WirelessBody.init(name: "", value: "")
        GETClientsresponse.init(error_code: 0, error_msg: "", data: nil)
        ClientList.init(client_list: nil, total_size: 0)
        Clients.init(ssid_name: "", ssid_index: 0, ip_address: "", mac_address: "", tx: "", rx: "", signale_ratio: 0, rssi: 0, connected_time: "", idle_time: 0, vendor: "")
        ClientsBody.init(name: "", value: "")
        LocalTimeBody.init(name: "", value: "")
        NTPTimeServerBody.init(name: "", value: "")
        GETPingresponse.init(error_code: 0, error_msg: "", data: nil)
        ping.init(result: "")
        GETTracerouteresponse.init(error_code: 0, error_msg: "", data: nil)
        traceRoute.init(result: "")
        GETDateAndTimeresponse.init(error_code: 0, error_msg: "", data: nil)
        datetime.init(local_time_year: 0, local_time_month: 0, local_time_day: 0, local_time_hour: 0, local_time_minute: 0, local_time_seconde: 0, time_zone: 0, ntp_enabled: 0, ntp_server_name: "", ntp_update_interval: 0, auto_daylight_save: 0)
        POSTDateAndTimeresponse.init(error_code: 0, error_msg: "")
        POST24GBandresponse.init(error_code: 0, error_msg: "")
        POST5GBandresponse.init(error_code: 0, error_msg: "")
        POST6GBandresponse.init(error_code: 0, error_msg: "")
        POST24GSecurityresponse.init(error_code: 0, error_msg: "")
        POST5GSecurityresponse.init(error_code: 0, error_msg: "")
        POST6GSecurityresponse.init(error_code: 0, error_msg: "")
        POSTsysReloadresponse.init(error_code: 0, error_msg: "", data: nil)
        reload.init(reload_time: "")
        KickClient.init(radio: "", ssid_index: "", client_mac: "", current: false)
        Kick_Client.init(radio: "", ssid_index: "", client_mac: "")
        PUTKickClientresponse.init(error_code: 0, error_msg: "")
    }
    func testAppTemplteFlow() throws {
        @EnvironmentObject var backPress0 : ManageViews
        @StateObject var backPress = ManageViews.shared
        let root = ContentView.init()
        root.environmentObject(backPress)
        root.backPress.page = 1
        @State var isSmile = true
        let view = AppTemplate.init(presented: $isSmile)
       
      //  let view = root.showMainView() as? AppTemplate
       
        let a = view.body
        
        
        
       // XCTAssertTrue(view.waitForExistence(timeout: 120))
        //view.environmentObject(ManageViews())
        view.showLOGINflag = true
        //view.backPress.accessDetail = MyAccessBody(appname: "123", mac: "00:00", health: "Good", client: "5")
        view.showAccess()
        view.AddAllNew()
        view.health(hex: "0xFFFFFF")
        view.drawArrow(client: MyAccessBody(appname: "1234", mac: "555555", health: "Goos", client: "5",location: "",central: "",wifiradio: "",model: "",unknow: 0))
        view.drawArrow(client: MyAccessBody(appname: "1234", mac: "555555", health: "Goos", client: "5",location: "",central: "Sophos",wifiradio: "",model: "",unknow: 0))
        view.showDialog(width: 480, height: 1100, x: 240, y: 550, Content: "test")
        view.showClient(client: "22", central: "")
        view.showClient(client: "22", central: "Sophos")
        view.showLOGINflag = true
        let b = view.showLOGIN(tag: "", width: 480, height: 1100, x: 240, y: 550, Content: "test")
        b.hidden()
       // SideMenu(width: 480, isOpen: true, menuClose: Void(), height: 1100)
        view.openMenu()
        do{
            let aesgcm256KEY:SymmetricKey? = try GenericPasswordStore().readKey(account: "1234456")
            if aesgcm256KEY == nil {
            let key = SymmetricKey(size: .bits256)
            try GenericPasswordStore().storeKey(key, account: "123456")
            }
        } catch let error {}
        
        let exp = XCTestExpectation(description: "AppTemplate")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            XCTAssertTrue(true)
            
            exp.fulfill()
        }
        exp.expectedFulfillmentCount = 1
        
        wait(for: [exp], timeout: 10)
        
    }
    func testAppTemplateModel()
    {
        let model = AppTemplateModel()
        model.getSystem()
        model.update()
        model.updateMy(mac: "00:1e:00:00")
        model.updateMyAll()
        model.updateProgress()
        model.updateMy(mac: "00:00:1e:00:01:af")
       // model.updateMy(mac: "00:1e:00:00")
        let exp = XCTestExpectation(description: "AppTemplateModel")
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            XCTAssertTrue(true)
            
            exp.fulfill()
        }
                exp.expectedFulfillmentCount = 1
        
        wait(for: [exp], timeout: 10)
    }
    func testAppDetailModel()
    {
        let model = AppDetailModel()
        model.updateEditStyle()
        model.putReboot()
        model.getSystem()
        model.getAdministration(productName: "123")
        model.postAdministration()
        model.updateR()
        model.getLANPort()
        model.postLANPort()
        model.initBands()
        model.get24GSSIDSecurity(ssid_index: "1")
        model.get5GSSIDSecurity(ssid_index: "1")
        model.get6GSSIDSecurity(ssid_index: "1")
        model.getSSIDs(model: "AP6 420E")
        model.post24GBands()
        model.post24GSecurity(ssid_index: "1")
        model.post5GBands()
        model.post5GSecurity(ssid_index: "1")
        model.post6GBands()
        model.post6GSecurity(ssid_index: "1")
        model.OnWiFi(wifiradoi: "7", model: "AP6 420E")
        model.OffWiFi(model: "AP6 420E")
        
        model.getWirelessClients(GHz: "2")
        model.getWirelessClients(GHz: "5")
        model.getWirelessClients(GHz: "6")
        model.deleteClients(i: 0, radio: "2", ssid_index: "1", client_mac: "00:1e:00:00")
        model.putKickClient(radio: "2", ssid_index: "1", client_mac: "00:1e:00:00")
        model.initDateTime()
        model.getDateAndTime()
        model.postDateAndTime()
        model.delete(mac: "00:1e:00:00")
        model.pingTest(address: "8.8.8.8",isIPv4:true,isIPv6: false)
        model.tracerouteTest(address: "8.8.8.8")
        model.postsysReload()
        
        let exp = XCTestExpectation(description: "AppDetailModel")
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            XCTAssertTrue(true)
            exp.fulfill()
        }
        exp.expectedFulfillmentCount = 1
        
        wait(for: [exp], timeout: 10)
    }
    func testStyle()
    {
        let A = Color.c0x005CC8
        let B = Color.c0xCED1D5
        let C = LoginTextFieldStyle.self
        let F = MyTableTextFieldStyle.self
        let G = MyWiBandsTableTextFieldStyle.self
        let H = MyNewTableTextFieldStyle.self
        //let I = PlaceholderStyle.self
        let J = Arrow.self
        let K = OverflowContentViewModifier.self
        
      
        customDefault1ToggleStyle.self
      //  customNTPToggleStyle.self
        customNTP1ToggleStyle.self
        customSettingsToggleStyle.self
      //  ColoredToggleStyle.self
        CheckboxStyle.self
        timeString(time: 100)
        
        "8.8.8.8".isIpAddress()
       
        AdaptsToKeyboard.self
        uptimeString(inputDate: "10:00:09")
        systemTimeString(inputDate: "2022/06/23 12:10:15")
        @State var isSmile = true
        customWiBandsToggleStyle(edit: $isSmile)
        MarqueeText.init(text: "")
        MarqueeWiBandsText.init(text: "")
        MarqueeWiBands6GText.init(index: 0)
        MarqueeWiBands5GText.init(index: 0)
        MarqueeWiBands24GText.init(index: 0)
        MarqueeTimeZoneText.init()
        MarqueeSystemIPV6LinkText.init()
        MarqueeSystemIPV6Text.init()
        MarqueeSystemDNSText.init()
        MarqueeSystemText.init()
        MarqueeDetailTitleText.init()
        MarqueeTitleText.init()
    }
    func testManageDatas()
    {
        let data = ManageDatas()
        data.saveToMyAP("", SSDP(LOCATION: "", SERVER: "", ST: "", USN: "", Vendor: "", Mac: "", Model: "", Name: "", OperatorMode: "", Identity: "", FirmwareVersion: "", CPUUtilization: "", ConnectedUsers: "", SNR: "", MemoryUtilization: "", WiFiRadio: "", live: "", SOPHOS_INFO: ""))
        data.getMyAPMatch("")
        data.getLoginMatch("")
        data.saveToLogin("", login(admin_account: "", admin_pw: "", keep: 0))
        data.checkMyAPMatch("", SSDP(LOCATION: "", SERVER: "", ST: "", USN: "", Vendor: "", Mac: "", Model: "", Name: "", OperatorMode: "", Identity: "", FirmwareVersion: "", CPUUtilization: "", ConnectedUsers: "", SNR: "", MemoryUtilization: "", WiFiRadio: "", live: "", SOPHOS_INFO: ""))
        data.checkLoginAmount()
        data.checkMyAPAmount()
        data.checkLoginMatch("", "")
        data.deleteMyap("")
        data.deleteLogin("")
    }
    func testAbout()
    {
        @StateObject var backPress = ManageViews.shared
        let root = ContentView.init()
        root.environmentObject(backPress)
        @State var isSmile = true
        let view = about(presented: $isSmile, width: 480, height: 1100)
        view.environmentObject(root.backPress)
        view.body
    }
    func testSetting()
    {
        @StateObject var backPress = ManageViews.shared
        let root = ContentView.init()
        root.environmentObject(backPress)
        @State var isSmile = true
        let view = settings(presented:$isSmile, width: 480, height: 1100)
        view.environmentObject(root.backPress)
       // view.body
    }
    func testSSDP()
    {
        let a = SSDPService(host: "", response: "")
        a.location
        
    }
    func testIP()
    {
        let result = calculatePressed(subnetMask: "255.255.255.0", IPAddress: "192.168.1.2")
        
        
    }
    func testKeychain()
    {
        
        let a = SSBKeychain()
        a.authenticateUser()
       // a.auth()
       // a.check()
        a.checkAuth()
        a.password()
        
        let COByte = ContiguousBytes.self
        guard let CByte = COByte as? ContiguousBytes else {

            return
        }
        CByte.dataRepresentation
        CByte.withUnsafeBytes { _ in
        }
        
        KeyStoreError("Unable to store item")

    }
    func testSockt()
    {
        SocketReader.Protocol.self
        SocketWriter.Protocol.self
        
        
        SSLError.success
        let error = SSLError.fail(5, "TEST")
        SSLError.retryNeeded
        
        
        guard let err = error as? SSLError else {

            return
        }
        
        err.errCode
        err.description
        
        socket(0, 0, 0).message
        Socket.Address.self
        Socket.Address.self.unix(sockaddr_un())
        Socket.Address.self.ipv4(sockaddr_in())
        Socket.Address.self.ipv6(sockaddr_in6())
        
        Socket.Error.self
        
    }
 
}

