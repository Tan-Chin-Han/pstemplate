//
//  AppDetailsTests.swift
//  PSTemplate
//
//  Created by EDIOS on 2022/5/5.
//

import XCTest
@testable import PSTemplate
import SwiftUI

class AppDetailsTests: XCTestCase {
    var view: AppDetail! = nil
    @State var show = true
    override func setUp() {
        
        view = AppDetail(presented: $show)
        view!.presented = true
        // configure the label, in lieu of loading a storyboard
//        let exp = XCTestExpectation(description: "AppDetail")
//        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
//            XCTAssertTrue(true)
//            
//            exp.fulfill()
//        }
//        exp.expectedFulfillmentCount = 1
//        
//        wait(for: [exp], timeout: 150)
    }

    override func tearDown() {}
    func testDetailFlow() throws {
       // AppDetail.init(presented: $isSmile)
       
        @StateObject var backPress = ManageViews.shared
        let root = ContentView.init()
        root.environmentObject(backPress)
        root.backPress.page = 2
       // root.backPress.accessDetail[0] = MyAccessBody(appname: "1234", mac: "555555", health: "Goos", client: "5")
        root.backPress.Central = false
        root.show = true
        root.showMainView()
        
      //  view.environmentObject(root.backPress)
        view.body
        
        
       // view.environmentObject(ManageViews())
       // view.backPress.Central = true
       // view.backPress.accessDetail = MyAccessBody(appname: "1234", mac: "555555", health: "Goos", client: "5")
        view.Detail.updateEditStyle()
        
        view.showDetail().environmentObject(root.backPress)
        view.showButton(width: 480, height: 1100)
        view.showFunc(width: 480)
        view.System(width: 480)
        view.LAN(width: 480)
        view.WirelessBands(width: 480)
        view.WirelessClients(width: 480)
        view.DateAndTime(width: 480)
        view.pingIP = "8.8.8.8"
        //view.PingTest(width: 480)
        view.tracerouteIP = "8.8.8.8"
        //view.TracerouteTest(width: 480)
        view.showsave(width: 480)
        view.showWait(tag: "", width: 480, height: 1100, x: 240, y: 550, Content: "test")
        view.showRebootSuccess(width: 480)
        view.showLANIPType = true
        view.showLeaseTimeFlag = true
        view.GateWayFromDHCPFlag = true
        view.PriAddrFromDHCPFlag = true
        view.SecAddrFromDHCPFlag = true
        view.showBandsTaggle24G = true
        view.showBandsTaggle5G = true
        view.showBandsTaggle6G = true
        view.showList(width: 480, height: 1100)
        view.showWirelessClientsList(i: 0, width: 480)
        view.showDialog(tag: "", width: 480, height: 1100, x: 240, y: 550, Content: "test")
        view.showRadioDialog(tag: "", width: 480, height: 1100, x: 240, y: 550, Content: "test")
        view.wirelessDetail(detail: view.Detail.wireless24G, bands: 1, width: 480, flag: true, overText: 0)
        view.wirelessDetail5G(detail: view.Detail.wireless5G, bands: 1, width: 480, flag: true, overText: 100)
        view.wirelessDetail6G(detail: view.Detail.wireless6G, bands: 1, width: 480, flag: true, overText: 200)
        
//        let app = XCUIApplication()
//        app.launch()
//        let newAP = app.buttons["pointbuttonPressed"] // .accessibilityIdentifier("pointbuttonPressed")
//        XCTAssertTrue(newAP.waitForExistence(timeout: 120))
//        newAP.tap()
//        let addallaps = app.buttons["addallaps"] // .accessibilityIdentifier("pointbuttonPressed")
//        XCTAssertTrue(addallaps.waitForExistence(timeout: 120))
//        addallaps.tap()
//        let myAP = app.buttons["myAPPressed"] // .accessibilityIdentifier("myAP")
//        XCTAssertTrue(myAP.waitForExistence(timeout: 10))
//        myAP.tap()
//        //let gotoDetail = app.
//        var bindingValue = true
//        let binding = Binding(get: { bindingValue }, set: { bindingValue = $0 })
//        AppTemplate.init(presented: binding)
        
    }
}
