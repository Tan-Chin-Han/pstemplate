//
//  PSTemplateApp.swift
//  PSTemplate
//
//  Created by Edimax on 2021/12/23.
//

import SwiftUI
import os

@main
struct PSTemplateApp: App {
    @StateObject var backPress = ManageViews.shared
    var body: some Scene {
        
        WindowGroup {
            
            ContentView()
                .environmentObject(backPress)
                .onAppear() {
                    antiJailbreak.init().assignJailBreakCheckType(type: .readAndWriteFiles)
                    antiJailbreak.init().assignJailBreakCheckType(type: .systemCalls)
                }
        }
    }
    
}
