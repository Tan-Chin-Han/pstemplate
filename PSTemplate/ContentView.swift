//
//  ContentView.swift
//  PSTemplate
//
//  Created by Edimax on 2021/12/23.
//

import SwiftUI
import CoreData
import Combine

struct ContentView: View {
    @Environment(\.scenePhase) var scenePhase
    @Environment(\.managedObjectContext) private var viewContext
    @StateObject var backPress = ManageViews.shared
    @StateObject var LocalAuth = SSBKeychain()
    @StateObject var datas = ManageDatas()
    @State var show = true
    @State var backgroung = false
    @State var client = SSDPDiscovery()
    @State var SSDPResult = 0 //20220418 enable SSDP function
    @State var flag = false
    @State var unknpwn = 0
    @State var locationManager = LocationManager()

    
    init() {
        print("APP init")
    }
    
    var body: some View {
        NavigationView {
           
            showMainView()
                
            //20220314 disable SSDP function
                .onReceive(Just(SSDPResult)) { a in
                  //  NSLog("SSDPResult \(a): \(SSDPResult)")
                    if backPress.page != 0
                    {
                        if flag == false {
                            if backPress.RebootFlag == false {
                            backPress.SSDPSearchAlert = true
                            NSLog("-------Start SSDPsearch-------")
                            self.SSDPsearch()
                            }
                        }
                    }
                }
         }
        .onAppear {
            for data in datas.checkMyAPAmount() {
                if data.key != ""
                {
                    
                    do {

                        let result = try! datas.decodeLocalStore(from: data.value) as SSDP
                        //print("result.CPUUtilization : \(result.CPUUtilization)")
                        backPress.myAccess.append(MyAccessBody(appname:result.Name ?? "rename" , mac: result.Mac ?? "00:00:1e:00:01:af" , health: result.CPUUtilization ?? "Good" , client: result.ConnectedUsers ?? "10", location: result.LOCATION, central:result.OperatorMode, wifiradio: result.WiFiRadio,model: result.Model,unknow: 7))
                        backPress.newFlag = true
                        backPress.myFlag = true
                        
                    }
                }
            }
        }
        .onReceive(backPress.$page) { page in
            showMainView()
            
//            let name = try? secure.get(forKey: "name")
//           // let face = try? secure.get(forKey: "face")
//            print("secure name : \(name ?? "")")
//            if name == nil {
//                try? secure.set(string: backPress.name, forKey: "name")
//            }
//            let astronauts = Bundle.main.decode("schema.json")
//           // for data in astronauts {
//            print("dhcp_lease_time : \(astronauts.NetworkIPv4Config?.properties?.dhcp_lease_time?.description)")
//          //  }
            
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
        .environmentObject(backPress)
        .preferredColorScheme(.dark)
        .onChange(of: scenePhase) { newPhase in
            print("newPhase :\(newPhase) ")
            if newPhase == .active {
                print("Active")
                backgroung = false
                LocalAuth.checkAuth()
               
            } else if newPhase == .inactive {
                print("Inactive")
                backgroung = false
                LocalAuth.checkAuth()
                
            } else if newPhase == .background {
                print("Background")
                backgroung = true
                LocalAuth.checkAuth()
                
               
            }
        }
    }
    @ViewBuilder
    func showMainView() -> some View {
        if backPress.page == 0
        {
            Text("").fullScreenCover(isPresented: $show, content: {
                MainView.init()
            })
        }
        else if backPress.page == 1
        {
            VStack(spacing: 20) {
                Text("").fullScreenCover(isPresented: $show, content: {
                    ZStack {
                        AppTemplate.init(presented: $show)
                            .opacity(backgroung == true ? 0.0:1.0)
                           
                        GeometryReader { geometry in
                        VStack {
                            
                            HStack {

                            }
                            .frame(height: abs(geometry.size.height/2 - 120))
                            
                            Image("WiFi6")
                                .resizable()
                                .frame(width: 69, height:60 )
                            Text("Wireless")
                                .font(.InterSemiBold36)
                                .foregroundColor(Color(hex:0xFFFFFF))
                            HStack {

                            }
                            .frame(height: abs(geometry.size.height/2 - 30))
                            .background(Color.clear)
                            Image("SophosAP")
                                .padding(.bottom,60)
                            
                        }
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                        .background(Color.c0x005CC8)
                        .edgesIgnoringSafeArea(.all)
                        .navigationBarHidden(true)
                        .opacity(backgroung == true ? 1.0:0.0)
                        }
                    }
                })
                
            }
        }
        else if backPress.page == 2
        {
            VStack(spacing: 20) {
                Text("").fullScreenCover(isPresented: $show, content: {
                    ZStack {
                        AppDetail.init(presented: $show)
                           // .environmentObject(ManageViews())
                            
                        GeometryReader { geometry in
                        VStack {
                            
                            HStack {

                            }
                            .frame(height: abs(geometry.size.height/2 - 120))
                            
                            Image("WiFi6")
                                .resizable()
                                .frame(width: 69, height:60 )
                            Text("Wireless")
                                .font(.InterSemiBold36)
                                .foregroundColor(Color(hex:0xFFFFFF))
                            HStack {

                            }
                            .frame(height: abs(geometry.size.height/2 - 30))
                            .background(Color.clear)
                            Image("SophosAP")
                                .padding(.bottom,60)
                            
                        }
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                        .background(Color.c0x005CC8)
                        .edgesIgnoringSafeArea(.all)
                        .navigationBarHidden(true)
                        .opacity(backgroung == true ? 1.0:0.0)
                        }
                    }
                })
                
            }

          
        }
//        else if backPress.page == 3
//        {
//            VStack(spacing: 20) {
//                Text("").fullScreenCover(isPresented: $show, content: {
//                    about.init(presented: $show)
//                })
//            }
//
//        }
//        else if backPress.page == 4
//        {
//            VStack(spacing: 20) {
//                Text("").fullScreenCover(isPresented: $show, content: {
//                    settings.init(presented: $show)
//                })
//            }
//          
//        }
    }
    func SSDPsearch() {
//        if SSDPResult == 0 ||  SSDPResult == 1 {
//            flag = false
//            NSLog("SSDP 0 , 1 : \(SSDPResult)")
//            client.discoverService(forDuration: 0.1, searchTarget: "www.sophos.com:device:WLANAccessPointDevice:1", port: 1900)
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//              //  NSLog("100 ms")
//                SSDPResult += 1
//            }
//        }
//        else if SSDPResult == 2  && flag == false
//        {
//            flag = true
//            NSLog("SSDP 2 : \(SSDPResult)")
        if flag == false {
            flag = true
            SSDPResult = 0
            client.SSDPResults.removeAll(keepingCapacity: true)
            NSLog("----SSDP discoverService seconds----")
            client.discoverService(forDuration: 3.0, searchTarget: "www.sophos.com:device:WLANAccessPointDevice:1", port: 1900)
            DispatchQueue.main.asyncAfter(deadline: .now() + 13.0) {
                //Start 3次 Discover
                NSLog("10 seconds")
                flag = false
                SSDPResult = 1
            }
            
           
               DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                   NSLog("3 seconds")
//                   do{
//                   let name = try? datas.secure.get(forKey: "face")
//                   print("secure name : \(name ?? "")")
//                   }
//                   catch{}
                   backPress.SSDPSearchAlert = false
                   backPress.SSDPDiscvoer = client.SSDPResults
                   if client.SSDPResults.isEmpty && unknpwn <= 5
                   {
                       unknpwn += 1
                   }
                   if unknpwn == 6
                   {
                       for ap in 0..<backPress.myAccess.count {
                           let a = MyAccessBody(appname: backPress.myAccess[ap].appname ?? "rename", mac: backPress.myAccess[ap].mac ?? "00:00:1e:00:01:af", health: "", client: backPress.myAccess[ap].client ?? "10", location: backPress.myAccess[ap].location ?? "192.168.1.2:10443", central: backPress.myAccess[ap].central ?? "0" ,wifiradio: backPress.myAccess[ap].wifiradio ?? "3",model: backPress.myAccess[ap].model ?? "AP6 420E",unknow: 7)
                           backPress.myAccess[ap] = a
                       }
                   }
                   
                  
                    //filter 3次 Discover 結果
                   if backPress.myAccess.isEmpty
                   {
                       for respone in client.SSDPResults
                        {
                           unknpwn = 0
                            let result = SSDPService(host: respone.key, response: respone.value)
                            if result.Mac != nil
                            {
                                let ssdp = SSDP(LOCATION: result.location, SERVER: result.server, ST: result.searchTarget, USN: result.uniqueServiceName, Vendor: result.Vendor, Mac: result.Mac, Model: result.Model, Name: result.Name, OperatorMode: result.OperatorMode, Identity: result.Identity, FirmwareVersion: result.FirmwareVersion, CPUUtilization: result.CPUUtilization, ConnectedUsers:result.ConnectedUsers, SNR: result.SNR, MemoryUtilization: result.MemoryUtilization, WiFiRadio: result.WiFiRadio, live: "", SOPHOS_INFO: result.SOPHOS_INFO)
                               // if !datas.checkMyAPMatch(result.Mac!,ssdp)
                               // {
                                if backPress.newAccess.filter{$0.mac == result.Mac}.isEmpty && backPress.myAccess.filter{$0.mac == result.Mac}.isEmpty
                                    {
                                        //420 系列 connected user 0-256 220 219 141
                                        //840 系列 connected user 0-512 440 439 282
                                        
                                        let CPUUtilization = Int(result.CPUUtilization ?? "0")!
                                        let MemoryUtilization = Int(result.MemoryUtilization ?? "0")!
                                        let ConnectedUsers = Int(result.ConnectedUsers ?? "0")!
                                        
                                        var health = "Excellent"
                                        if ((result.Model?.contains("420")) != nil){
                                            if CPUUtilization >= 86 || MemoryUtilization >= 86 || ConnectedUsers >= 220
                                            {
                                                health = "Poor"
                                            }
                                            else if (CPUUtilization <= 85 && CPUUtilization >= 56 ) || (MemoryUtilization <= 85 && MemoryUtilization >= 56 ) || ( ConnectedUsers <= 219 && ConnectedUsers >= 141   )
                                            {
                                                health = "Good"
                                            }
                                        }
                                        else if ((result.Model?.contains("840")) != nil){
                                            if CPUUtilization >= 86 || MemoryUtilization >= 86 || ConnectedUsers >= 440
                                            {
                                                health = "Poor"
                                            }
                                            else if (CPUUtilization <= 85 && CPUUtilization >= 56 ) || (MemoryUtilization <= 85 && MemoryUtilization >= 56 ) || ( ConnectedUsers <= 439 && ConnectedUsers >= 282   )
                                            {
                                                health = "Good"
                                            }
                                        }
                                        
                                        backPress.newAccess.append(NewAccessBody(appname:result.Name ?? "rename" , mac: result.Mac ?? "00:00:1e:00:01:af" , health: health , client: result.ConnectedUsers ?? "10",location: result.location ?? "192.168.1.2:10443",central: result.OperatorMode ?? "0",wifiradio: "3",model: "AP6 420E",unknow: 0))
                                        
                                        backPress.newFlag = true
                                    }
                                    
                            }//if result.Mac != nil
                        }//for
                   }
                   else{
                   for ap in 0..<backPress.myAccess.count
                   {
                   var check = false
                   for respone in client.SSDPResults
                    {
                       unknpwn = 0
                        let result = SSDPService(host: respone.key, response: respone.value)
                        if result.Mac != nil
                        {
                            let ssdp = SSDP(LOCATION: result.location, SERVER: result.server, ST: result.searchTarget, USN: result.uniqueServiceName, Vendor: result.Vendor, Mac: result.Mac, Model: result.Model, Name: result.Name, OperatorMode: result.OperatorMode, Identity: result.Identity, FirmwareVersion: result.FirmwareVersion, CPUUtilization: result.CPUUtilization, ConnectedUsers:result.ConnectedUsers, SNR: result.SNR, MemoryUtilization: result.MemoryUtilization, WiFiRadio: result.WiFiRadio, live: "", SOPHOS_INFO: result.SOPHOS_INFO)
                           // if !datas.checkMyAPMatch(result.Mac!,ssdp)
                           // {
                            if backPress.newAccess.filter{$0.mac == result.Mac}.isEmpty && backPress.myAccess.filter{$0.mac == result.Mac}.isEmpty
                                {
                                    //420 系列 connected user 0-256 220 219 141
                                    //840 系列 connected user 0-512 440 439 282
                                    
                                    let CPUUtilization = Int(result.CPUUtilization ?? "0")!
                                    let MemoryUtilization = Int(result.MemoryUtilization ?? "0")!
                                    let ConnectedUsers = Int(result.ConnectedUsers ?? "0")!
                                    
                                    var health = "Excellent"
                                    if ((result.Model?.contains("420")) != nil){
                                        if CPUUtilization >= 86 || MemoryUtilization >= 86 || ConnectedUsers >= 220
                                        {
                                            health = "Poor"
                                        }
                                        else if (CPUUtilization <= 85 && CPUUtilization >= 56 ) || (MemoryUtilization <= 85 && MemoryUtilization >= 56 ) || ( ConnectedUsers <= 219 && ConnectedUsers >= 141   )
                                        {
                                            health = "Good"
                                        }
                                    }
                                    else if ((result.Model?.contains("840")) != nil){
                                        if CPUUtilization >= 86 || MemoryUtilization >= 86 || ConnectedUsers >= 440
                                        {
                                            health = "Poor"
                                        }
                                        else if (CPUUtilization <= 85 && CPUUtilization >= 56 ) || (MemoryUtilization <= 85 && MemoryUtilization >= 56 ) || ( ConnectedUsers <= 439 && ConnectedUsers >= 282   )
                                        {
                                            health = "Good"
                                        }
                                    }
                                    
                                    backPress.newAccess.append(NewAccessBody(appname:result.Name ?? "rename" , mac: result.Mac ?? "00:00:1e:00:01:af" , health: health , client: result.ConnectedUsers ?? "10",location: result.location ?? "192.168.1.2:10443",central: result.OperatorMode ?? "0",wifiradio: "3",model: "AP6 420E",unknow: 0))
                                    
                                    backPress.newFlag = true
                                }
                                else
                                {
                                   
                                  //  let status =  datas.saveToMyAP((result.Mac)!,ssdp)
                                  //  print("saveToMyAP: \((result.Mac)!) \(status)")
//                                    for ap in 0..<backPress.myAccess.count
//                                    {
                                        
                                        if backPress.myAccess[ap].mac == result.Mac
                                        {
                                            let CPUUtilization = Int(result.CPUUtilization ?? "0")!
                                            let MemoryUtilization = Int(result.MemoryUtilization ?? "0")!
                                            let ConnectedUsers = Int(result.ConnectedUsers ?? "0")!
                                            
                                            var health = "Excellent"
                                            if CPUUtilization >= 86 || MemoryUtilization >= 86 || ConnectedUsers >= 220
                                            {
                                                health = "Poor"
                                            }
                                            else if (CPUUtilization <= 85 && CPUUtilization >= 56 ) || (MemoryUtilization <= 85 && MemoryUtilization >= 56 )
                                            {
                                                health = "Good"
                                            }
                                            
                                           // let a = MyAccessBody(appname: result.Name ?? "rename", mac: result.Mac ?? "00:00:1e:00:01:af", health: health, client: result.ConnectedUsers ?? "10", location: result.location ?? "192.168.1.2:10443", central: result.OperatorMode ?? "0" ,wifiradio: result.WiFiRadio ?? "3",model: result.Model ?? "AP6 420E",unknow: 0)
                                            let a = MyAccessBody(appname: result.Name ?? "rename", mac: result.Mac ?? "00:00:1e:00:01:af", health: health, client: result.ConnectedUsers ?? "10", location: result.location ?? "192.168.1.2:10443", central: result.OperatorMode ?? "0" ,wifiradio: result.WiFiRadio ?? "3",model: result.Model ?? "AP6 420E",unknow: 0)
                                            backPress.myAccess[ap] = a
                                            check = true
                                            NSLog("seconds update SSDP disvover")
                                        }
                                //    }//for
                                    
                                }//else
                        }//if result.Mac != nil
                    }//for
                       if check == false
                       {
                           var health = backPress.myAccess[ap].health
                           if (backPress.myAccess[ap].unknow ?? 0) == 5
                           {
                               health = ""
                           }
                           NSLog("unknpwn : \(backPress.myAccess[ap].mac) \(backPress.myAccess[ap].unknow ?? 0) + 1)")
                           let a = MyAccessBody(appname: backPress.myAccess[ap].appname ?? "rename", mac: backPress.myAccess[ap].mac ?? "00:00:1e:00:01:af", health: health, client: backPress.myAccess[ap].client ?? "10", location: backPress.myAccess[ap].location ?? "192.168.1.2:10443", central: backPress.myAccess[ap].central ?? "0" ,wifiradio: backPress.myAccess[ap].wifiradio ?? "3",model: backPress.myAccess[ap].model ?? "AP6 420E",unknow: (backPress.myAccess[ap].unknow ?? 0) + 1)
                           backPress.myAccess[ap] = a
                       }
                   }//for
                   }//else
                }//DispatchQueue.main.asyncAfter(deadline: .now() + 3.0)
        } // if flag == false
    }
}
