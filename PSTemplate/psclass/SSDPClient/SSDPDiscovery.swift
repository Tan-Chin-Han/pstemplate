import Foundation
import SwiftUI

// MARK: Protocols

/// Delegate for service discovery
public protocol SSDPDiscoveryDelegate {
    /// Tells the delegate a requested service has been discovered.
    func ssdpDiscovery(_ discovery: SSDPDiscovery, didDiscoverService service: SSDPService)

    /// Tells the delegate that the discovery ended due to an error.
    func ssdpDiscovery(_ discovery: SSDPDiscovery, didFinishWithError error: Error)

    /// Tells the delegate that the discovery has started.
    func ssdpDiscoveryDidStart(_ discovery: SSDPDiscovery)

    /// Tells the delegate that the discovery has finished.
    func ssdpDiscoveryDidFinish(_ discovery: SSDPDiscovery)
}

public extension SSDPDiscoveryDelegate {
    func ssdpDiscovery(_ discovery: SSDPDiscovery, didDiscoverService service: SSDPService) {
        //service.location
    }

    func ssdpDiscovery(_ discovery: SSDPDiscovery, didFinishWithError error: Error) {}

    func ssdpDiscoveryDidStart(_ discovery: SSDPDiscovery) {}

    func ssdpDiscoveryDidFinish(_ discovery: SSDPDiscovery) {}
}

/// SSDP discovery for UPnP devices on the LAN
public class SSDPDiscovery {

    /// The UDP socket
    private var socket: Socket? = nil

    /// Delegate for service discovery
    public var delegate: SSDPDiscoveryDelegate?

    /// The client is discovering
    public var isDiscovering: Bool {
        get {
            return self.socket != nil
        }
    }
    public var SSDPResults: [String:String] = ["":""]
    public var SSDPUnknown = 0
    // MARK: Initialisation

    public init() {
        //HeliumLogger.use()
    }

    deinit {
        self.stop()
    }
    // MARK: Private functions

    /// Read responses.
    private func readResponses() {
        do {
            
            if self.socket != nil {
                var data = Data()
                let (bytesRead, address) = try self.socket!.readDatagram(into: &data)
                
                if bytesRead > 0 {
                    let (remoteHost, _) = Socket.hostnameAndPort(from: address!)!
                    let response = String(data: data, encoding: .utf8)
                   // let (remoteHost, _) = Socket.hostnameAndPort(from: address!)!
                    print("Received: \(response!) from \(remoteHost)")
                   // self.delegate?.ssdpDiscovery(self, didDiscoverService: SSDPService(host: remoteHost, response: response!))
                    
                    SSDPResults[remoteHost] = response!
                    SSDPUnknown = 0
                   // let result = SSDPService(host: remoteHost, response: response!)
                   // SSDPResults?.append(result)
                    
//                    if result?.Mac != nil
//                    {
//
//
//
//                    let status = ManageDatas().saveToMyAP((result?.Mac)!,SSDP(LOCATION: result?.location, SERVER: result?.server, ST: result?.searchTarget, USN: result?.uniqueServiceName, Vendor: result?.Vendor, Mac: result?.Mac, Model: result?.Model, Name: result?.Name, OperatorMode: result?.OperatorMode, Identity: result?.Identity, FirmwareVersion: result?.FirmwareVersion, CPUUtilization: result?.CPUUtilization, ConnectedUsers:result?.ConnectedUsers, SNR: result?.SNR, MemoryUtilization: result?.MemoryUtilization, WiFiRadio: result?.WiFiRadio, SM_ID: result?.SM_ID, SOPHOS_INFO: result?.SOPHOS_INFO))
//
//                    print("saveToMyAP: \((result?.Mac)!) \(status)")
//                    }
                }
               
                
            }
        } catch let error {
           // print("Socket error: \(error)")
            self.forceStop()
            self.delegate?.ssdpDiscovery(self, didFinishWithError: error)
            
            if SSDPUnknown <= 17
            {
                SSDPUnknown += 1
            }
            if SSDPUnknown == 18
            {
                SSDPResults.removeAll()
            }
        }
    }

    /// Read responses with timeout.
    private func readResponses(forDuration duration: TimeInterval) {
        let queue = DispatchQueue.global()

        queue.async() {
            while self.isDiscovering {
                self.readResponses()
            }
        }

 //       queue.asyncAfter(deadline: .now() + duration) { [weak self] in
 //       queue.asyncAfter(deadline: .now() + duration) { [unowned self] in
          queue.asyncAfter(deadline: .now() + duration) { [unowned self] in
 //       queue.asyncAfter(deadline: .now() + duration) { [unowned self , unowned socket = self.socket] in
            self.stop()
//            if socket != nil {
//                print("Stop SSDP discovery")
//            //    self.forceStop()
//               // self.delegate?.ssdpDiscoveryDidFinish(self)
//            }
        }
    }

    /// Force stop discovery closing the socket.
    private func forceStop() {
        if self.isDiscovering {
            if self.socket != nil {
            
                self.socket?.close()
           
            }
        }
        guard let a = self.socket else{return}
        
        self.socket = nil //20220419 crash
       
    }

    // MARK: Public functions

    /**
        Discover SSDP services for a duration.
        - Parameters:
            - duration: The amount of time to wait.
            - searchTarget: The type of the searched service.
    */
    open func discoverService(forDuration duration: TimeInterval = 10.0, searchTarget: String = "ssdp:all", port: Int32 = 1900) {
        print("Start SSDP discovery for \(Double(duration)) duration...")
        self.delegate?.ssdpDiscoveryDidStart(self)

        let message = "M-SEARCH * HTTP/1.1\r\n" +
            "MAN: \"ssdp:discover\"\r\n" +
            "HOST: 239.255.255.250:\(port)\r\n" +
            "ST: \(searchTarget)\r\n" +
            "MX: \(Int(duration))\r\n\r\n"

        do {
            self.socket = try Socket.create(type: .datagram, proto: .udp)
            try self.socket!.listen(on: 0)

            self.readResponses(forDuration: duration)

          //  print("Send: \(message)")
            try self.socket?.write(from: message, to: Socket.createAddress(for: "239.255.255.250", on: port)!)

        } catch let error {
            print("Socket error: \(error)")
            self.forceStop()
            self.delegate?.ssdpDiscovery(self, didFinishWithError: error)
        }
    }

    /// Stop the discovery before the timeout.
    open func stop() {
        if self.socket != nil {
        //    print("Stop SSDP discovery")
            self.forceStop()
            self.delegate?.ssdpDiscoveryDidFinish(self)
        }
    }
}
