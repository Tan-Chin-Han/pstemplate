import Foundation

public class SSDPService {
    /// The host of service
    public internal(set) var host: String
    /// The value of `LOCATION` header
    public internal(set) var location: String?
    /// The value of `SERVER` header
    public internal(set) var server: String?
    /// The value of `ST` header
    public internal(set) var searchTarget: String?
    /// The value of `USN` header
    public internal(set) var uniqueServiceName: String?
    
    ///Vendor,Mac,Model,Name,OperatorMode,Identity,FirmwareVersion,CPUUtilization,ConnectedUsers,SNR,MemoryUtilization,WiFiRadio
    ///SM_ID:700000123
    ///DEV_TYPE:DEV_TYPE
    ///SOPHOS_INFO:AP001188334455
    /// The value of `Vendor` header
    public internal(set) var Vendor: String?
    public internal(set) var Mac: String?
    public internal(set) var Model: String?
    public internal(set) var Name: String?
    public internal(set) var OperatorMode: String?
    public internal(set) var Identity: String?
    public internal(set) var FirmwareVersion: String?
    public internal(set) var CPUUtilization: String?
    public internal(set) var ConnectedUsers: String?
    public internal(set) var SNR: String?
    public internal(set) var MemoryUtilization: String?
    public internal(set) var WiFiRadio: String?
    public internal(set) var SM_ID: String?
    public internal(set) var SOPHOS_INFO: String?

    // MARK: Initialisation

    /**
        Initialize the `SSDPService` with the discovery response.

        - Parameters:
            - host: The host of service
            - response: The discovery response.
    */
    init(host: String, response: String) {
        self.host = host
        self.location = self.parse(header: "LOCATION", in: response)
        self.server = self.parse(header: "SERVER", in: response)
        self.searchTarget = self.parse(header: "ST", in: response)
        self.uniqueServiceName = self.parse(header: "USN", in: response)
        
        self.Vendor = self.parse(header: "VENDOR", in: response)
        self.Mac = self.parse(header: "MAC", in: response)
        self.Model = self.parse(header: "MODEL", in: response)
        self.Name = self.parse(header: "NAME", in: response)
        self.OperatorMode = self.parse(header: "OPERATORMODE", in: response)
        self.Identity = self.parse(header: "IDENTITY", in: response)
        self.FirmwareVersion = self.parse(header: "FIRMWAREVERSION", in: response)
        self.CPUUtilization = self.parse(header: "CPUUTILTZATION", in: response)
        self.ConnectedUsers = self.parse(header: "CONNECTEDUSERS", in: response)
        self.SNR = self.parse(header: "SNR", in: response)
        self.MemoryUtilization = self.parse(header: "MEMORYUTILIZATION", in: response)
        self.WiFiRadio = self.parse(header: "WIFIRADIO", in: response)
        self.SM_ID = self.parse(header: "SM_ID", in: response)
        self.SOPHOS_INFO = self.parse(header: "SOPHOS_INFO", in: response)
    }

    // MARK: Private functions

    /**
        Parse the discovery response.

        - Parameters:
            - header: The header to parse.
            - response: The discovery response.
    */
    private func parse(header: String, in response: String) -> String? {
        
//        if let range = response.range(of: "\(header): .*", options: .regularExpression) {
//            var value = String(response[range])
//            value = value.replacingOccurrences(of: "\(header): ", with: "")
//            return value
//        }
//        return nil
        
        let responseDict = parseResponseIntoDictionary(response)
        if let value = responseDict[header] {
            return value
        }
        return nil
    }
    private func parseResponseIntoDictionary(_ response: String) -> [String: String] {
           var elements = [String: String]()
           for element in response.split(separator: "\r\n") {
               let keyValuePair = element.split(separator: ":", maxSplits: 1)
               guard keyValuePair.count == 2 else {
                   continue
               }

               let key = String(keyValuePair[0]).uppercased().trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
               let value = String(keyValuePair[1]).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

               elements[key] = value
           }

           return elements
       }
}
