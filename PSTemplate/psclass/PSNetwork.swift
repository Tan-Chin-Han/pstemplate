//
//  PSNetwork.swift
//  pslib
//
//  Created by Edimax on 2021/10/28.
//

import SwiftUI
import Network
import Darwin
import OSLog
//import Socket

class PSNetwork: ObservableObject {
    
    var url:String?
    var method:String?
    var DigestAccount:String?
    var DigestPw:String?
    var body:Data?
   // @Published
    var result:Data?
    @Published var t:Decodable? {
        didSet {
            self.objectWillChange.send()
        }
    }
    
    func getResult<T:Decodable>(model: T.Type,completion:@escaping(Int)->()){
        let _url = URL(string: self.url!)
        var request = URLRequest(url: _url!)
        request.httpMethod = self.method
        if self.method == "POST"
        {
            request.httpBody = self.body
        }
        //The default timeout interval is 60 seconds
       // request.setValue("application/json", forHTTPHeaderField: "Content-Type")
      //  request.httpBody = self.data
       // request.addValue("Digest", forHTTPHeaderField: "Authorization")
        let config = URLSessionConfiguration.default
            //config.isDiscretionary = true
            config.sessionSendsLaunchEvents = true
        
        let adapter = CustomURLSessionAdapter(configuration: config)
        adapter.DigestAccount = DigestAccount
        adapter.DigestPw = DigestPw
        adapter.urlSession.dataTask(with: request) { data, response, error in
       // URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let datas = data else{
                completion(500)
                return
            }
            let str = String(decoding: datas, as: UTF8.self)
            print("data : \(str)")
            //os_log("response error:\(error as! NSObject)")
           // os_log("response:\(str)")
           // if error == nil || (response as! HTTPURLResponse).statusCode != 200 {
                print("(HTTPURLResponse).statusCode: \((response as! HTTPURLResponse).statusCode)")
                let fields = (response as! HTTPURLResponse).allHeaderFields as! [String:String]
            print("fields: \(fields)")
              //  let realm:String = self.getAuthenticationKey(headerInfo: fields,key: "realm")!
              //  let nonce:String = self.getAuthenticationKey(headerInfo: fields,key: "nonce")!
           // }
            
            DispatchQueue.main.async { [self] in //background
                self.result = datas
                
                do{
                    self.t  = try model.init(jsonData: self.result!)
                    
                    completion(200)
                } catch {
                    print(error)
                    completion(400)
                }
            }
        }.resume()
    }
    
//    func SSDPSearch() {
//
//        let searchString = "M-SEARCH * HTTP/1.1\r\n" +
//            "Host: 239.255.255.250:1900\r\n" +
//            "Man: \"ssdp:discover\"\r\n" +
//            "ST: urn:com-comtrend:device:WifiXtendDevice:1\r\n" +
//            "Mx: 3\r\n"
//        let groupSendContent = Data(searchString.utf8)
//        guard let multicastGroup = try? NWMulticastGroup(for: [ .hostPort(host: "239.255.255.250", port: 1900) ]) else {
//            fatalError("Failed to create multicast group")
//        }
//        let connectionGroup = NWConnectionGroup(with: multicastGroup, using: .udp)
//        connectionGroup.setReceiveHandler(maximumMessageSize: 16384, rejectOversizedMessages: true) { message, content, isComplete in
//            print("...Received message from \(String(describing: message.remoteEndpoint))")
//            if let contentS = content, let messageS = String(data: content!, encoding: .utf8) {
//                print("Message: \(messageS)")
//            }
//        //////            print("[NWConnectionGroup][setReceiveHandler] message from \(String(describing: message.remoteEndpoint))")
//        //////                        if let contentData = content,
//        //////                            let strMessage = String(bytes: contentData, encoding: .utf8) {
//        //////                            print("[NWConnectionGroup][setReceiveHandler] content decoded as utf8 string: \(strMessage)")
//        //////                        }
//        //////                        print("[NWConnectionGroup][setReceiveHandler] received: \(String(describing: content?.count)) bytes")
//        //////                        print("[NWConnectionGroup][setReceiveHandler] isComplete: \(isComplete)")
//        //////
//        //////                        message.reply(content: groupSendContent)
//                }
//
//                connectionGroup.stateUpdateHandler = { newState in
//                    print("Group entered state \(String(describing: newState))")
//
//                }
//                connectionGroup.start(queue: .main)
//
//                Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { _ in
//                    connectionGroup.send(content: groupSendContent) { error in
//                        print("Send complete with error \(String(describing: error))")
//                    }
//                }
//
//              //  Timer.scheduledTimer(withTimeInterval: 30, repeats: true) { [self] _ in
//
//        //            var searchSession: SSDPSearchSession?
//        //
//        //            var servicesFound = [SSDPService]()
//
//        //            self.searchSession?.stopSearch()
//
//        //            let configuration = SSDPSearchSessionConfiguration.createMulticastConfiguration(forSearchTarget: "urn:com-comtrend:device:WifiXtendDevice:1")
//        //            self.searchSession = SSDPSearchSession(configuration: configuration)
//        //            self.searchSession?.delegate = self
//        //            self.searchSession?.startSearch()
//        //       // }
//        //        Timer.scheduledTimer(withTimeInterval: 10, repeats: true) { [self] _ in
//        //            self.searchSession?.stopSearch()
//        //            self.searchSession?.startSearch()
//        //        }
//
//
//        //        Timer.scheduledTimer(withTimeInterval: 10, repeats: true) { [self] _ in
//        //            self.client.delegate = self
//        //            self.client.discoverService(forDuration: duration, searchTarget: "urn:com-comtrend:device:WifiXtendDevice:1", port: 1900)
//        //        }
//    }
//    func getAuthenticationKey(headerInfo: [String:String],key: String) -> String?{
//           for item in headerInfo.enumerated(){
//               print("key: \(item.element.0), values : \(item.element.1)")
//
//               let headerKeys = item.element.1.split(separator: ",")
//
//               for item in headerKeys {
//                   if item.contains(key) {
//                       let authinticationKey = item.split(separator: "=")[1]
//                       return String(authinticationKey)
//                   }
//               }
//
//           }
//           return nil
//       }
}


extension Decodable {
    init(jsonData: Data) throws {
        self = try JSONDecoder().decode(Self.self, from:jsonData)
    }
}
class CustomURLSessionAdapter: URLSessionAdapter {

    var DigestAccount:String?
    var DigestPw:String?
//    convenience init() {
//        self.init(configuration: URLSessionConfiguration.default)
//    }

    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if challenge.previousFailureCount == 0 {
            
            let credential = URLCredential(user: DigestAccount!, password: DigestPw!, persistence: .none)
            completionHandler(.useCredential, credential)
        } else {
            challenge.sender?.cancel(challenge)
            completionHandler(.cancelAuthenticationChallenge, nil)
        }
    }

}
