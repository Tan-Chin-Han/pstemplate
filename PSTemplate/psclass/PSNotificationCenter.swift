//
//  PSNotificationCenter.swift
//  pslib
//
//  Created by Edimax on 2021/11/24.
//

import SwiftUI
import UserNotifications

class PSNotificationCenter: NSObject, UNUserNotificationCenterDelegate{
    static let shared = PSNotificationCenter()
    
    //** background **//
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let notificationNmae = Notification.Name(response.notification.request.identifier)
        NotificationCenter.default.post(name: notificationNmae, object: response.notification.request.content)
        completionHandler()
    }
    
    //** foreground **//
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let notificationNmae = Notification.Name(notification.request.identifier)
        NotificationCenter.default.post(name: notificationNmae, object: notification.request.content)
        completionHandler(.sound)
    }
    
    func requestPermission(_ delegate: UNUserNotificationCenterDelegate? = nil, onDeny handler : (()->Void)? = nil ){
        
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings(completionHandler: { settings in
            
            if settings.authorizationStatus == .denied {
                if let handler = handler {
                    handler()
                }
                return
            }
            if settings.authorizationStatus != .authorized {
                center.requestAuthorization(options: [.alert, .sound, .badge]) {
                    _, error in
                    
                    if let error = error {
                        print("error handling \(error)")
                    }
                }
            }
        })
        center.delegate = delegate  ?? self
    }
    func addNotification(id : String, title : String , subtitle : String, sound : UNNotificationSound = UNNotificationSound.default , trigger : UNNotificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false))
    {
        let content = UNMutableNotificationContent()
        content.title = title
        content.subtitle = subtitle
        content.sound = sound
        let request = UNNotificationRequest(identifier: id, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request)
    }
    
    func removeNotification(_ ids : [String])
    {
        UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: ids)
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ids)
    }
    
}
