//
//  SSBKeychain.swift
//  pslib
//
//  Created by Edimax on 2021/10/28.
//

import SwiftUI
import CryptoKit
import Security
import LocalAuthentication

class SSBKeychain: ObservableObject {

    @Published var loggedIn: Bool = false
    @Published var hasChanges: Bool = false

    private var domainState: Data?

//    static func isBiometricAuthenticationAvailable() -> (available:Bool, type: GFLocalAuthenticationBiometricType) {
//        let context = LAContext()
//        var error: NSError?
//        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
//            if #available(iOS 11.0, *) {
//                if context.biometryType == .faceID {
//                    return (true, .biometricTypeFaceID)
//                }
//                else {
//                    return (true, .biometricTypeTouchID)
//                }
//            } else { // we can only have TouchID on iOS < 11
//                return (true, .biometricTypeTouchID)
//            }
//        }
//        else {
//            return (false, .biometricTypeNone)
//        }
//    }
    
    func authenticateUser() {
        let context = LAContext()
        var error: NSError?
        let data = ManageDatas()
        
        
        
        if data.localStore.bool(forKey: "passcode") == false
        {
            context.localizedCancelTitle = ""
            context.localizedFallbackTitle = ""
        }
        
        if data.localStore.bool(forKey:"faceid") == false && data.localStore.bool(forKey:"touchid") == false
        {
            
               if data.localStore.bool(forKey: "passcode")
               {
                 
                   if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
                       
                       print(error as Any)
                       let reason = "Identify yourself!"
                       
                      
                       context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) {
                           [unowned self] success, authenticationError in

                           DispatchQueue.main.async {
                               
                               if success {
                                  // self.runSecretCode()
                                  // data.localStore.set(true,forKey:"passcode")
                                   self.loggedIn = success
                               } else {
        
                                   print(authenticationError.debugDescription)
                               }
                           }
                       }
                   } else {
                       switch error!.code{
                       
                                  case LAError.passcodeNotSet.rawValue:
                                      print("A passcode has not been set")
                                      data.localStore.set("A passcode has not been set.",forKey:"PasscodeLAError")
                                      self.loggedIn = true
                       
                                  default:
                                      // The LAError.TouchIDNotAvailable case.
                                      print("TouchID not available")
                                  }
                   }
               }
               else
               {
                   self.loggedIn = true
               }
           
        }
        else
        {
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            
           
            let reason = "Identify yourself!"

            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) {
                [unowned self] success, authenticationError in

                DispatchQueue.main.async {
                    
                    if success {
                       // self.runSecretCode()
//                        data.localStore.set(true,forKey:"faceid")
//                        data.localStore.set(true,forKey:"touchid")
                        self.loggedIn = success
                    } else {
//                        let ac = UIAlertController(title: "Authentication failed", message: "Sorry!", preferredStyle: .alert)
//                        ac.addAction(UIAlertAction(title: "OK", style: .default))
//                        self.present(ac, animated: true)
                      //  password()
                        
                        
                        print(authenticationError.debugDescription)
                    
                    }
                }
            }
        } else {

            switch error!.code{
            
                       case LAError.biometryNotEnrolled.rawValue:
                           print("No identities are enrolled.")
                            if data.localStore.bool(forKey: "SupportFaceID")
                            {
                                data.localStore.set("Authentication could not start, because biometry has no enrolled identities.",forKey:"FaceIDAuthLAError")
                            }
                            else if data.localStore.bool(forKey: "SupportTouchID")
                            {
                                  data.localStore.set("Authentication could not start, because biometry has no enrolled identities.",forKey:"TouchIDAuthLAError")
                            }
            
                       case LAError.passcodeNotSet.rawValue:
                           print("A passcode has not been set")
            
                       default:
                           // The LAError.TouchIDNotAvailable case.
                           print("TouchID not available")
                       }
            
                       // Optionally the error description can be displayed on the console.
                       print(error?.localizedDescription)
            if data.localStore.bool(forKey: "passcode")
            {
                if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
                    
                    print(error as Any)
                    let reason = "Identify yourself!"
                    
                   
                    context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) {
                        [unowned self] success, authenticationError in

                        DispatchQueue.main.async {
                            
                            if success {
                               // self.runSecretCode()
                               // data.localStore.set(true,forKey:"passcode")
                                self.loggedIn = success
                            } else {
        //                        let ac = UIAlertController(title: "Authentication failed", message: "Sorry!", preferredStyle: .alert)
        //                        ac.addAction(UIAlertAction(title: "OK", style: .default))
        //                        self.present(ac, animated: true)
                              //  password()
                                
                                
                                print(authenticationError.debugDescription)
                            }
                        }
                    }
                } else {
                    switch error!.code{
                    
                               case LAError.passcodeNotSet.rawValue:
                                   print("A passcode has not been set")
                                   data.localStore.set("A passcode has not been set.",forKey:"PasscodeLAError")
                                   self.loggedIn = true
                    
                               default:
                                   // The LAError.TouchIDNotAvailable case.
                                   print("TouchID not available")
                               }
                }
            }
            else
            {
                self.loggedIn = true
            }
        }
        }
    }
    
    func checkAuth()
    {
        let context = LAContext()
        var error: NSError?
        let data = ManageDatas()
        
        data.localStore.set(false,forKey:"SupportFaceID")
        data.localStore.set(false,forKey:"SupportTouchID")
        
        data.localStore.set("TouchID not available \n No identities are enrolled",forKey:"TouchIDAuthLAError")
        data.localStore.set("FaceID not available \n No identities are enrolled",forKey:"FaceIDAuthLAError")
        data.localStore.set("Passcode not available",forKey:"PasscodeLAError")
        
        context.localizedCancelTitle = ""
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            
//            data.localStore.set(true,forKey:"faceid")
//            data.localStore.set(true,forKey:"touchid")
            if context.biometryType == .faceID {
                print("The device supports Face ID")
                data.localStore.set(true,forKey:"SupportFaceID")
                data.localStore.set(false,forKey:"SupportTouchID")
                
                data.localStore.set("TouchID not available",forKey:"TouchIDAuthLAError")
                data.localStore.set("",forKey:"FaceIDAuthLAError")
            }
            else if context.biometryType == .touchID {
                print("The device supports Touch ID")
                data.localStore.set(false,forKey:"SupportFaceID")
                data.localStore.set(true,forKey:"SupportTouchID")
                
                data.localStore.set("FaceID not available",forKey:"FaceIDAuthLAError")
                data.localStore.set("",forKey:"TouchIDAuthLAError")
            }
           
            data.localStore.set("",forKey:"PasscodeLAError")
            
        } else {
 
            switch error!.code{
            
                       case LAError.biometryNotEnrolled.rawValue:
                           print("No identities are enrolled.")
//                           data.localStore.set(false,forKey:"faceid")
//                           data.localStore.set(false,forKey:"touchid")
                if data.localStore.bool(forKey: "SupportFaceID")
                {
                    data.localStore.set("Authentication could not start, because biometry has no enrolled identities.",forKey:"FaceIDAuthLAError")
                }
                else if data.localStore.bool(forKey: "SupportTouchID")
                {
                      data.localStore.set("Authentication could not start, because biometry has no enrolled identities.",forKey:"TouchIDAuthLAError")
                }
            
                       case LAError.passcodeNotSet.rawValue:
                           print("A passcode has not been set")
            
                       default:
                           // The LAError.TouchIDNotAvailable case.
                           print("TouchID not available")
                       }
            
                       // Optionally the error description can be displayed on the console.
                       print(error?.localizedDescription)
            
            if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
                
               // data.localStore.set(true,forKey:"passcode")
                data.localStore.set("",forKey:"PasscodeLAError")
 
            } else {
                switch error!.code{
                
                           case LAError.passcodeNotSet.rawValue:
                               print("A passcode has not been set")
                               data.localStore.set("A passcode has not been set.",forKey:"PasscodeLAError")
                               self.loggedIn = true
                
                           default:
                               // The LAError.TouchIDNotAvailable case.
                               print("TouchID not available")
                           }
            }
        }
    }
//    func auth() {
//        let context = LAContext()
//        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Biometric Auth") {
//            [weak self] (res, err) in
//            DispatchQueue.main.async {
//                self?.loggedIn = res
//                print(res)
//            }
//        }
//    }

//    func check() {
//        let context = LAContext()
//        context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
//        if context.biometryType == .faceID {
//             
//        }
//        else {
//            
//        }
//        checkDomainState(context.evaluatedPolicyDomainState)
//    }

    private func checkDomainState(_ domainState: Data?) {
        if let `domainState` = domainState {
            if domainState != self.domainState {
                hasChanges = true
            } else {
                hasChanges = false
            }
        }
        self.domainState = domainState
        hasChanges = true
    }
    func password() {
        let context = LAContext()
        var error:NSError?

        // edit line - deviceOwnerAuthentication
        guard context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) else {
            //showAlertViewIfNoBiometricSensorHasBeenDetected()
            return
        }

        // edit line - deviceOwnerAuthentication
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil) {

            // edit line - deviceOwnerAuthentication
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: "Password", reply: { (success, error) in
                if success {
                    DispatchQueue.main.async {
                        print("Authentication was successful")
                        self.loggedIn = success
                    }
                }else {
                    DispatchQueue.main.async {
                        //self.displayErrorMessage(error: error as! LAError )
                        print("Authentication was error")
                    }
                }
            })
        }
    }
}

