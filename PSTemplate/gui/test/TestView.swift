//
//  TestView.swift
//  pslib
//
//  Created by Edimax on 2021/12/17.
//

import SwiftUI

struct TestView: View {
    @Environment(\.presentationMode) var presentationMode
    @State private var isPresented = false

    @Binding var presented: Bool
    
    @EnvironmentObject var backPress : ManageViews
    @EnvironmentObject var test : MainViewModel
    
        
    var body: some View {
        NavigationView {
            GeometryReader { geometry in
                VStack {
                    Button(action: {
                        self.presentationMode.wrappedValue.dismiss()
                        
                    }, label: {
                        Text("go to Mainview").background(Color.green)
                    })
                    Button(action: {
                        self.presented = true
                        backPress.page = 0
                    }, label: {
                        Text("goback view").background(Color.green)
                    })
                    
                    Text(backPress.name).background(Color.pink)
                    Button(action: {
                        self.isPresented.toggle()
                    }, label: {
                        Text("Show Modal with full screen").background(Color.pink)
                    }).fullScreenCover(isPresented: $isPresented, content: FullScreenModalView.init)

                }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .background(Color.green)
                .edgesIgnoringSafeArea(.all)
                
            }
         
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .navigationBarBackButtonHidden(true)
       
    }
}
struct FullScreenModalView: View {
     @Environment(\.presentationMode) var presentationMode
     @EnvironmentObject var test : MainViewModel
     @EnvironmentObject var backPress : ManageViews
    
     var body: some View {
        VStack {
            Text("This is a modal view")
            Text(backPress.name)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color.gray)
        .edgesIgnoringSafeArea(.all)
        .onTapGesture {
            presentationMode.wrappedValue.dismiss()
        }
        
    }
}

#if DEBUG
struct TestView_Previews: PreviewProvider {
    
    static var previews: some View {
        Text("")
    }
}
#endif
