//
//  MainView.swift
//  PSTemplate
//
//  Created by Edimax on 2021/12/24.
//

import SwiftUI
import Combine

struct MainView: View {
    @Environment(\.presentationMode) var presentationMode
    @State var fullScreen = false
    @State var show = false
    @EnvironmentObject var backPress : ManageViews
    @StateObject var Main = MainViewModel()
    @State private var thing: String = "Sophos"
    @State private var showPopup: Bool = false
    
    @State private var location: CGPoint = CGPoint(x: 0, y: 0)
    @State private var num = "0"
    
    @StateObject var LocalAuth = SSBKeychain()
    @StateObject var datas = ManageDatas()
    
//    var simpleDrag: some Gesture {
//        DragGesture()
//            .onChanged { value in
//                self.location = value.location
//            }
//    }
    
    var body: some View {
        NavigationView {
            GeometryReader { geometry in
            VStack {
                HStack {

                }
                .frame(height: abs(geometry.size.height/2 - 120))
                
                Image("WiFi6")
                    .resizable()
                    .frame(width: 69, height:60 )
                Text("Wireless")
                    .font(.InterSemiBold36)
                    .foregroundColor(Color(hex:0xFFFFFF))
                HStack {

                }
                .frame(height: abs(geometry.size.height/2 - 60))
                .background(Color.clear)
                
                Image("SophosAP")
                    .padding(.bottom,60)
                //    .padding(.leading, 20)
               
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(Color.c0x005CC8)
            .edgesIgnoringSafeArea(.all)
            .navigationBarHidden(true)
            }
        }
        
        .onAppear(){
           // backPress.page = 1 //20220304 模擬機可以跑
            //let LocalAuth = SSBKeychain()
            LocalAuth.checkAuth()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {

               if datas.localStore.bool(forKey:"faceid") || datas.localStore.bool(forKey:"touchid") || datas.localStore.bool(forKey:"passcode")
               {
                   LocalAuth.authenticateUser()
               }
               else
               {
                   backPress.page = 1
               }
            }
         }
        .onReceive(LocalAuth.$loggedIn) { (result) in
            if result
            {
                print("loggin")
                backPress.page = 1
            }
            else
            {
                print("failed")
                
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .environmentObject(backPress)
        .preferredColorScheme(.dark)
    }
//    @ViewBuilder
//    func showMessage() -> some View {
//        if show {
//            Text("Error !!")
//            .foregroundColor(Color.white)
//            .background(Color.red)
//            .cornerRadius(5)
//
//        }
//    }
//    @ViewBuilder
//    func showDialog() -> some View {
//        if showPopup {
//            ZStack(){}.popup(isPresented: $showPopup, with: $thing) { item in
//               VStack(spacing: 20) {
//                TextField("Name", text: item)
//                   Button {
//                    showPopup = false
//                    show = false
//                   } label: {
//                       Text("Dismiss Popup")
//                   }
//                   HStack {
//                       Image(systemName: "star")
//                       Text("Hello World!")
//                   }
//               }
//               .frame(width: 200)
//               .padding()
//               .background(Color.green)
//               .cornerRadius(8)
//               .position(location)
//               .gesture(simpleDrag)
//            }
//        }
//    }
}
//
//struct MainView_Previews: PreviewProvider {
//    static var previews: some View {
//        MainView()
//    }
//}
