//
//  about.swift
//  PSTemplate
//
//  Created by EDIOS on 2022/3/29.
//

import SwiftUI

struct about: View {
    @Environment(\.presentationMode) var presentationMode
    
    @Binding var presented: Bool
    let width: CGFloat
    let height: CGFloat
    @EnvironmentObject var backPress : ManageViews
    @State var version = ""
    
    var body: some View {
        VStack(spacing:0){
            
            HStack {

            }
            .frame(width: width, height: 100)
            .background(Color.clear)
           
            GridStack(rows: 3, columns: 1) { row, col in
                if row == 0
                {
                    
                    Text("About")
                        .frame(width:width-40,height: 30,alignment: .leading)
                        .font(.InterRegular18)
                        .foregroundColor(Color.c0x242527)
                        .padding(.leading,-10)
                     
                }
                else if row == 1
                {
                    Link(destination: URL(string: "https://www.sophos.com/en-us/legal/sophos-end-user-terms-of-use")!, label: {
                        Text("Sophos End User Terms of Use")
                            .underline()
                            .font(.InterRegular14)
                            .foregroundColor(Color.c0x005BC8)
                            .frame(width: width-40,height: 30,alignment: .leading)
                            .padding(.leading,-10)
                            .accessibilityIdentifier("menu_privatePolicy")
                            
                    })
                    .frame(width: width,height: 40)
                    
                    
                }
                else
                {
                    HStack(spacing:0){
                        Text("Version")
                            .frame(width:width/2,height: 30,alignment: .leading)
                            .font(.InterRegular14)
                            .foregroundColor(Color.c0x242527)
                            
                        Text(version)
                            .frame(width:width/2-30,height: 30,alignment: .trailing)
                            .font(.InterRegular14)
                            .foregroundColor(Color.c0x242527)
                            .padding(.trailing,5)
                            
                    }
                    .frame(width: width)
                }
            }
            
            .frame(width: width, height: 150)
            .background(Color.white)
            
                HStack {

                }
                .frame(width: width, height: self.height-100-150)
                .background(Color.white)
            
        }
        .frame(width: self.width,height: self.height)
        .opacity(self.presented ? 1.0 : 0.0)
        .edgesIgnoringSafeArea(.all)
        .onAppear {
            getVersion()
        }
        
//        VStack(spacing:0) {
//                HStack {
//
//                }
//                .frame(width: width, height: 100)
//                .background(Color.clear)
//
//                VStack(alignment:.leading,spacing:0) {
//                    Text("About")
//                        .frame(width:width,height: 30,alignment: .leading)
//                        .font(.SophosSansRegular_18)
//                        .foregroundColor(Color(hex: 0x818384))
//                        .padding(.top,20)
//                        .padding(.leading,32)
//
//                  //  HStack(spacing:0){
//                        Link(destination: URL(string: "https://www.sophos.com/en-us/legal/sophos-end-user-terms-of-use")!, label: {
//                            Text("Sophos End User Terms of Use")
//                                .font(.SophosSansRegular_14)
//                                .foregroundColor(Color(hex: 0x818384))
//                                .background(Color.black)
//                                .frame(width: width-15-40,alignment: .leading)
//                                .padding(.leading,30)
//
//                            Image("arrowBlack")
//                                .resizable()
//                                .frame(width: 15, height: 15,alignment: .trailing)
//                                .padding(.trailing,5)
//                                .background(Color.red)
//                        })
//                        .frame(width: width,height: 40)
//                        .background(Color.green)
//
//
////                        Text("Sophos End User Terms of Use")
////                       // Link("Sophos End User Terms of Use", destination: URL(string:"https://www.sophos.com/en-us/legal/sophos-end-user-terms-of-use")!)
////                            .frame(width:width/2,height: 30,alignment: .leading)
////                            .font(.SophosSansRegular_14)
////                            .foregroundColor(Color(hex: 0x818384))
////                            .padding(.leading,-20)
////
////
////                        VStack{
////                        Arrow()
////                          .stroke(style: StrokeStyle(lineWidth: 3.0, lineCap: .round))
////                          .fill(Color.black)
////                          .frame(width:40,height:40,alignment: .trailing)
////
////                        }.frame(width:width/2-20,height:40,alignment: .trailing)
////                            .padding(.trailing,30)
//
////                    }
////                    .frame(width: width,height: 50)
////                    .background(Color.white)
////                    .onTapGesture {
////                        print("tap frame")
////                        UIApplication.shared.open(URL(string: "https://www.sophos.com/en-us/legal/sophos-end-user-terms-of-use")!)
////                     }
//
//
//                    HStack(spacing:0){
//                        Text("Version")
//                            .frame(width:width/2-20,height: 30,alignment: .leading)
//                            .font(.SophosSansRegular_14)
//                            .foregroundColor(Color(hex: 0x818384))
//                            .padding(.leading,20)
//
//
//                        Text(version)
//                            .frame(width:width/2-20,height: 30,alignment: .trailing)
//                            .font(.SophosSansRegular_14)
//                            .foregroundColor(Color(hex: 0x818384))
//                            .padding(.trailing,-5)
//
//                    }
//                    .frame(width: width)
//
//                    HStack {
//
//                    }
//                    .frame(width: width, height: self.height-100-150)
//                    .background(Color.clear)
//                }
//                .frame(width: self.width,height: self.height-100)
//                .background(Color.white)
//
//            }
//
//            .frame(width: self.width,height: self.height)
//            .opacity(self.presented ? 1.0 : 0.0)
//            .edgesIgnoringSafeArea(.all)
//            .onAppear {
//                getVersion()
//            }
        
    }
    func getVersion() {
        let dictionary = Bundle.main.infoDictionary!
        let build = dictionary["CFBundleVersion"] as! String
        version = dictionary["CFBundleShortVersionString"] as! String
        version += "(" + build + ")"
    }
}
