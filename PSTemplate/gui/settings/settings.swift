//
//  settings.swift
//  PSTemplate
//
//  Created by EDIOS on 2022/3/29.
//

import SwiftUI
import Combine

struct settings: View {
    @Environment(\.presentationMode) var presentationMode
    @Binding var presented: Bool
    let width: CGFloat
    let height: CGFloat
    @EnvironmentObject var backPress : ManageViews
    
    @State var FaceID = true
    @State var TouchID = true
    @State var Password = true
    @State var showFaceIDAuthAlert = false
    @State var showTouchIDAuthAlert = false
   // @State var showPasscodeAlert = false
    @StateObject var LocalAuth = SSBKeychain()
    @StateObject var datas = ManageDatas()
    @State var face = false
    @State var touch = false
    @State var update = 0
    
    var body: some View {
        ZStack {
        VStack(spacing:0) {
                HStack {
                    
                }
                .frame(width: width, height: 100)
                .background(Color.clear)
                
                VStack(alignment:.leading,spacing:0) {
                    Text("Settings")
                        .frame(width:width,height: 30,alignment: .leading)
                        .font(.InterRegular18)
                        .foregroundColor(Color.c0x242527)
                        .padding(.top,50)
                        .padding(.leading,30)
                    
                    HStack(spacing:0){
                        if datas.localStore.string(forKey: "FaceIDAuthLAError") != ""
                        {
                            Button(action: {
                                self.showFaceIDAuthAlert = true
                            }) {
                                Toggle(isOn: $FaceID)
                                {
                                    Text("Unlock with Face ID")
                                }
                                .toggleStyle(customSettingsToggleStyle(edit: $FaceID,index: 0))
                                .frame(alignment: .center)
                                .padding(.horizontal,20)
                                .disabled(true)
                                .accessibilityIdentifier("OpenfaceID")
                            }
                        }
                        else
                        {
                            Button{
                                if datas.localStore.string(forKey: "FaceIDAuthLAError") == ""
                                {
                                    self.FaceID.toggle()
                                    datas.localStore.set(self.FaceID,forKey:"faceid")
                                }
                                else
                                {
                                    showFaceIDAuthAlert = true
                                }
                            } label: {
                               
                                Toggle(isOn: $FaceID)
                                {
                                    Text("Unlock with Face ID")
                                }
                                .toggleStyle(customSettingsToggleStyle(edit: $FaceID,index: 0))
                                .frame(alignment: .center)
                                .padding(.horizontal,20)
                                .disabled(false)
                                .gesture(drag)
                                .accessibilityIdentifier("OpenfaceID")
                            }
                            
                        }
                        
                    }
                    .frame(width: width)
                    .padding(.leading,10)
                    .background(Color.white)
                    .onReceive(Just(update)) { i in
                        if self.FaceID == false && self.TouchID == false
                        {
                            self.Password = false
                            datas.localStore.set(self.Password,forKey:"passcode")
                           
                        }
                    }
                    
                    
                    HStack(spacing:0){
                        if datas.localStore.string(forKey: "TouchIDAuthLAError") != ""
                        {
                            Button(action: {
                                self.showTouchIDAuthAlert = true
                            }) {
                                Toggle(isOn: $TouchID)
                                {
                                    Text("Unlock with Touch ID")
                                }
                                .toggleStyle(customSettingsToggleStyle(edit: $TouchID,index: 1))
                                .frame(alignment: .center)
                                .padding(.horizontal,20)
                                .disabled(true)
                                .accessibilityIdentifier("OpentouchID")
                            }
                            
                        }
                        else
                        {
                            Button{
                                if datas.localStore.string(forKey: "TouchIDAuthLAError") == ""
                                {
                                    self.TouchID.toggle()
                                    datas.localStore.set(self.TouchID,forKey:"touchid")
                                }
                                else
                                {
                                    showTouchIDAuthAlert = true
                                }
                            } label: {
                               
                                Toggle(isOn: $TouchID)
                                {
                                    Text("Unlock with Touch ID")
                                }
                                .toggleStyle(customSettingsToggleStyle(edit: $TouchID,index: 1))
                                .frame(alignment: .center)
                                .padding(.horizontal,20)
                                .disabled(false)
                            }
                        }
                    }
                    .frame(width: width)
                    .padding(.leading,10)
                    .background(Color.white)
                    
                   /*
                    HStack(spacing:0){
                        
                        if face == true && self.FaceID == false
                        {
                            Button(action: {
                                self.showPasscodeAlert = true
                            }) {
                                Toggle(isOn: $Password)
                                {
                                    Text("Unlock with Password")
                                }
                                .toggleStyle(customSettingsToggleStyle(edit: $Password,index: 2))
                                .frame(alignment: .center)
                                .padding(.horizontal,20)
                                .disabled(true)
                            }
                            .accessibilityIdentifier("cantPW0")
                        
                        }
                        else if touch == true && self.TouchID == false
                        {
                            Button(action: {
                                self.showPasscodeAlert = true
                            }) {
                                Toggle(isOn: $Password)
                                {
                                    Text("Unlock with Password")
                                }
                                .toggleStyle(customSettingsToggleStyle(edit: $Password,index: 2))
                                .frame(alignment: .center)
                                .padding(.horizontal,20)
                                .disabled(true)
                            }
                            .accessibilityIdentifier("cantPW1")
                        }
                        else
                        {
                            Button(action: {
                                if datas.localStore.string(forKey: "PasscodeLAError") == ""
                                {
                                    self.Password.toggle()
                                    datas.localStore.set(self.Password,forKey:"passcode")
                                }
                                else
                                {
                                    self.showPasscodeAlert = true
                                }
                            }) {
                                Toggle(isOn: $Password)
                                {
                                    Text("Unlock with Password")
                                }
                                .toggleStyle(customSettingsToggleStyle(edit: $Password,index: 2))
                                .frame(alignment: .center)
                                .padding(.horizontal,20)
                                
                                
                            }
                            .accessibilityIdentifier("cantPW2")
                        }
                    }
                    .frame(width: width)
                    .background(Color.white)
                    .padding(.leading,10)
                    */
                    
                    HStack {
                        
                    }
                    .frame(width: width, height: self.height-100-150)
                    .background(Color.clear)
                }
                .frame(width: self.width,height: self.height-100)
                .background(Color.white)
                
          
            }
                Alert()
            }
            .frame(width: self.width,height: self.height)
            .opacity(self.presented ? 1.0 : 0.0)
            .edgesIgnoringSafeArea(.all)
            .onAppear {
                
                self.FaceID = datas.localStore.bool(forKey:"faceid")
                self.TouchID = datas.localStore.bool(forKey:"touchid")
                self.Password = datas.localStore.bool(forKey:"passcode")
                           
                if self.FaceID == false
                {
                    if datas.localStore.string(forKey: "FaceIDAuthLAError") == ""
                    {
                        face = true
                    }
                    else
                    {
                        face = false
                    }
                }
                if self.TouchID == false
                {
                    if datas.localStore.string(forKey: "TouchIDAuthLAError") == ""
                    {
                        touch = true
                    }
                    else
                    {
                        touch = false
                    }
                }
                if face == true && self.FaceID == false
                {
                    self.Password = false
                    datas.localStore.set("Unlock with Face ID or Touch ID",forKey:"PasscodeLAError")
                }
                else if touch == true && self.TouchID == false
                {
                    self.Password = false
                    datas.localStore.set("Unlock with Face ID or Touch ID",forKey:"PasscodeLAError")
                }
                
            }
            
    }
    @ViewBuilder
    func Alert()->some View {
        if showFaceIDAuthAlert
        {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                
                VStack() {
                    GeometryReader { geometry in
                        Text(datas.localStore.string(forKey: "FaceIDAuthLAError")!)
                        .multilineTextAlignment(.center)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .padding(.top,20)
                        .padding(.horizontal,15)
                       // .position(x: geometry.size.width/2, y: geometry.size.height/3)
                    
                        HStack(alignment:.center)  {
                        Button{
                            self.showFaceIDAuthAlert = false
                          //  self.showWait = true
                        } label: {
                            Text("Ok")
                                .frame(minWidth: 0, maxWidth: 100, minHeight: 0, maxHeight: 40)
                                .font(.InterMedium14)
                                .foregroundColor(.white)
                                .background(Color.c0x005BC8)
                                .cornerRadius(5)
                        }.frame(width: 100, height: 40)
                                .accessibilityIdentifier("closeFaceErr")
//                            Spacer()
//                            Button{
//                                self.showAlert = false
//                                self.blurAmount = 0
//                            } label: {
//                                ZStack{
//                                RoundedRectangle(cornerRadius: 5)
//                                    .stroke(Color(hex: 0x1987CB), lineWidth: 1)
//                                Text("No")
//                                    .font(.SophosSansRegular_14)
//                                    .foregroundColor(Color(hex:0x055BB5))
//                                }.frame(width: 100, height: 40)
//                            }
                      }//HStack
                    .position(x: geometry.size.width/2, y: geometry.size.height - 35)
                        
                    } // GeometryReader
                }
                .frame(width:180,height: 200,alignment: .center)
                .background(Color.white)
                
                
            }.popup(isPresented: $showFaceIDAuthAlert, with: $showFaceIDAuthAlert) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:200,height: 200,alignment: .center)
           // .position(x: x, y: y)
            .padding()
        }
        else if showTouchIDAuthAlert
        {
            
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                
                VStack() {
                    GeometryReader { geometry in
                        Text(datas.localStore.string(forKey: "TouchIDAuthLAError")!)
                        .multilineTextAlignment(.center)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .padding(.top,20)
                        .padding(.horizontal,15)
                       // .position(x: geometry.size.width/2, y: geometry.size.height/3)
                    
                        HStack(alignment:.center)  {
                        Button{
                            self.showTouchIDAuthAlert = false
                          //  self.showWait = true
                        } label: {
                            Text("Ok")
                                .frame(minWidth: 0, maxWidth: 100, minHeight: 0, maxHeight: 40)
                                .font(.InterRegular14)
                                .foregroundColor(.white)
                                .background(Color.c0x005BC8)
                                .cornerRadius(5)
                        }.frame(width: 100, height: 40)
                                .accessibilityIdentifier("closetouch")
                        
//                            Spacer()
//                            Button{
//                                self.showAlert = false
//                                self.blurAmount = 0
//                            } label: {
//                                ZStack{
//                                RoundedRectangle(cornerRadius: 5)
//                                    .stroke(Color(hex: 0x1987CB), lineWidth: 1)
//                                Text("No")
//                                    .font(.SophosSansRegular_14)
//                                    .foregroundColor(Color(hex:0x055BB5))
//                                }.frame(width: 100, height: 40)
//                            }
                      }//HStack
                    .position(x: geometry.size.width/2, y: geometry.size.height - 35)
                    } // GeometryReader
                }
                .frame(width:180,height: 200,alignment: .center)
                .background(Color.white)
                
                
            }.popup(isPresented: $showTouchIDAuthAlert, with: $showTouchIDAuthAlert) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:200,height: 200,alignment: .center)
           // .position(x: x, y: y)
            .padding()
        }
//        else if self.showPasscodeAlert
//        {
//            ZStack(){
//
//                RoundedRectangle(cornerRadius: 5)
//                        .foregroundColor(.white)
//                        .shadow(radius: 10)
//
//                VStack() {
//                    GeometryReader { geometry in
//                        Text("Unlock with Face ID or Touch ID")
//                        .multilineTextAlignment(.center)
//                        .font(.InterRegular14)
//                        .foregroundColor(Color.c0x242527)
//                        .padding(.top,20)
//                        .padding(.horizontal,20)
//                       // .position(x: geometry.size.width/2, y: geometry.size.height/3)
//
//                        HStack(alignment:.center)  {
//                        Button{
//                            self.showPasscodeAlert = false
//                          //  self.showWait = true
//                            LocalAuth.checkAuth()
//
//                        } label: {
//                            Text("Ok")
//                                .frame(minWidth: 0, maxWidth: 100, minHeight: 0, maxHeight: 40)
//                                .font(.InterMedium14)
//                                .foregroundColor(.white)
//                                .background(Color.c0x005BC8)
//                                .cornerRadius(5)
//                        }.frame(width: 100, height: 40)
//                                .accessibilityIdentifier("closepw")
//
////                            Spacer()
////                            Button{
////                                self.showAlert = false
////                                self.blurAmount = 0
////                            } label: {
////                                ZStack{
////                                RoundedRectangle(cornerRadius: 5)
////                                    .stroke(Color(hex: 0x1987CB), lineWidth: 1)
////                                Text("No")
////                                    .font(.SophosSansRegular_14)
////                                    .foregroundColor(Color(hex:0x055BB5))
////                                }.frame(width: 100, height: 40)
////                            }
//                      }//HStack
//                    .position(x: geometry.size.width/2, y: geometry.size.height - 35)
//                    } // GeometryReader
//                }
//                .frame(width:180,height: 200,alignment: .center)
//                .background(Color.white)
//
//
//            }.popup(isPresented: $showPasscodeAlert, with: $showPasscodeAlert) { item in
//
//            }
//            .background(
//                RoundedRectangle(cornerRadius: 5)
//                        .stroke(Color.c0x696A6B, lineWidth: 1)
//            )
//            .frame(width:200,height: 200,alignment: .center)
//           // .position(x: x, y: y)
//            .padding()
//        }
    }
}
