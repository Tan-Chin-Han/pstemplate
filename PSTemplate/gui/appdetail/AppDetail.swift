//
//  AppDetail.swift
//  PSTemplate
//
//  Created by EDIOS on 2022/1/25.
//

import SwiftUI
import CoreAudio
import Combine
import NetworkExtension
import CryptoKit

struct AppDetail: View {
    
    @Binding var presented: Bool
    @EnvironmentObject var backPress: ManageViews
    @StateObject var Detail = AppDetailModel()
    @State var menuOpen: Bool = false
    @State var editFlag: Bool = false
    @State var funcPressed: Int = 0
    @State private var value = ""
    @State private var value1 = ""
    @State var save: Bool = false
    @State var showWaitRequest: Bool = false
    
    private let LANIPTypeArray = ["Static IP","DHCP"]
    private let FromDHCPArray = ["User-Defined","From DHCP"]
    @State var LANIPType = "Static IP"
    @State var LANIPTypeTaggle: Bool = false
    @State var showLANIPType: Bool = false
    @State var showLeaseTimeFlag: Bool = false
    @State var GateWayFromDHCP = "From DHCP"
    @State var GateWayFromDHCPFlag:Bool = false
    @State var PriAddrFromDHCP = "From DHCP"
    @State var PriAddrFromDHCPFlag:Bool = false
    @State var SecAddrFromDHCP = "From DHCP"
    @State var SecAddrFromDHCPFlag:Bool = false
    
    @State var AuthArraySelected = 0
    //private let AuthArray = ["None","WPA/WPA2 Mixed Mode-AES","WPA2-AES","WPA2/WPA3 Mixed Mode-AES","WPA3-AES","WPA/WPA2 Mixed Mode-EAP-AES","WPA2-EAP-AES","WPA3-EAP-AES","OWE"]
    private let AuthArray = ["None","WPA2-AES","WPA2/WPA3 Mixed Mode-AES","WPA3-AES","WPA2-EAP-AES","WPA3-EAP-AES","OWE"]
    private let Auth6GArray = ["WPA3-AES","WPA3-EAP-AES","OWE"]
    @State var showBandsTaggle24G: Bool = false
    @State var showBandsTaggle5G: Bool = false
    @State var showBandsTaggle6G: Bool = false
    @State var posBandsTaggle:CGPoint = CGPoint(x:0,y:0)
    @State var WiFiBands24G = "1"
    @State var WiFiBands5G = "1"
    @State var WiFiBands6G = "1"
    @State var Wireless24G: Bool = false
    @State var Wireless5G: Bool = false
    @State var Wireless6G: Bool = false
    @State var AuthROW = 0
    @State private var wirelessValue:[String] = []

    @State private var wirelessValue24G:[String] = []
    @State private var wirelessValue5G:[String] = []
    @State private var wirelessValue6G:[String] = []
    @State var showAlert: Bool = false
    @State var showWait: Bool = false
    @State private var blurAmount: CGFloat = 0
    @State var progressValue: Float = 0.0
    @State var RebootSuccess: Bool = false
    @State var RebootErr: Bool = false
    @State var APnotFound: Bool = false
    @State var showCurrentAPReboot:Bool = false
    @State var check:Bool = false
    @State var disableRadio: Bool = false
    @State var showRadioAlert: Bool = false
    @State var showRadioONAlert: Bool = false
    @State var showAuthType24GFlag: Bool = false
    @State var showAuthType5GFlag: Bool = false
    @State var showAuthType6GFlag: Bool = false
    @State var SSIDArray:[String] = []
    @State var showwirelessClients:Bool = false
    @State private var offsets = [CGSize](repeating: CGSize.zero, count: 50)
    
    @State var timeZone = "(GMT-06:00) Central Time (US & Canada)"
    @State var showLocalTimeYear: Bool = false
    @State var showLocalTimeMonth: Bool = false
    @State var showLocalTimeDay: Bool = false
    @State var showLocalTimeHours: Bool = false
    @State var showLocalTimeMinutes: Bool = false
    @State var showLocalTimeSeconds: Bool = false
    @State var showTimeZone: Bool = false
    
    @State var NTPServerType = "User-Defined"
    @State var showNTPServerType: Bool = false
    @State var FromNTPServerlag:Bool = false
    @State var onNTPServer:Bool = false
    @State var FromNTPServer = ""
    @State var DaylightSaving:Bool = false
    
    @State var pingIP = ""
    @State var pingMessage = ""
    @State var NTPinterval = "24"
    
    @State var tracerouteIP = ""
    @State var tracerouteMessage = ""
    
    @State var pingIPplaceholder = "Destination Address"
    @State var tracerouteIPplaceholder = "Destination Address"
    
    @State private var lastHoveredId = ""
    private let overText:[Int] = [0,100,200]
    @GestureState var isTapping = false
    @State private var timer = Timer.publish(every: 0.1, on: .main, in: .common).autoconnect()
    
    @State var newDate = ""
 //    let timerSysInfo = Timer.publish(every: 1, on: .current, in: .common).autoconnect()

   // @State var entry = ApodTimelineProvider.Entry.init(date: Date())
    @State var detail = MyAccessBody(appname: "", mac: "", health: "", client: "",location: "",central: "",wifiradio: "",model: "",unknow: 7 )
    @State var central = false
    @State var showErr:Bool = false
    @State var timeoutCount = 0
    @StateObject var datas = ManageDatas()
    @State var sysReload = 0
    @State var showWaitRadio:Bool = false
    @State var wifiApply:Bool = false
    //@State var delay:Double = 10.0
    @State var showWaitPingRequest:Bool = false
    @State var pskAlert:Bool = false
    @State var SSIDAlert:Bool = false
    @State var enableSSID = 0
    @State var refreshDateandTime:Bool = false
    @State var showWIFIAlert:Bool = false
    @State var updateUptime:Bool = false
    @State var upadteSystime:Bool = false
    @State var sysReloadTime = 0
    @State var sysReloadTimeFlag = false
    @State var sysReloadTimeCout = false
    @State var saveErrFlag = false
    @State var saveErr = ""
    
    @State var requestDate = false
    @State var diableFlag = false
    
    var body: some View {
      
        ZStack(alignment: .top) { // << made explicit alignment to top
            GeometryReader { geometry in
                VStack{
            HStack{
                Image("Menu")
                    .resizable()
                    .onTapGesture {
                        print("SliderMenu")
                        if !self.editFlag && !self.showWaitRequest{
                            if !self.menuOpen {
                                self.menuOpen.toggle()
                                 
                            }
                            
                        }
                    }
                    .frame(width: 18.0, height: 17.0)
                    .padding(.top, 40)
                    .padding(.leading,20)
                    .accessibility(addTraits: .isButton)
                    .accessibilityIdentifier("DetailMenu")
                
                Image("back20")
                    .onTapGesture {
                        
                        if self.editFlag
                        {
                            self.editFlag = false
                            if funcPressed == 0
                            {
                                if !self.showWaitRequest
                                {
                                    Detail.getSystem()
                                  
                                    self.showWaitRequest = true
                                    self.blurAmount = 2
                                 
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                        self.blurAmount = 0.0
                                        self.showWaitRequest = false
                                    }
                                    
                                }
                            }
                            else if funcPressed == 1
                            {
                                if !self.showWaitRequest
                                {
                                    Detail.getLANPort()
                                  
                                    self.showWaitRequest = true
                                    self.blurAmount = 2
                                 
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                        self.blurAmount = 0.0
                                        self.showWaitRequest = false
                                    }
                                    
                                }
                            }
                            else if funcPressed == 2
                            {
                                if !self.showWaitRequest
                                {
                                    Detail.getSSIDs(model: detail.model!)
                                  
                                    self.showWaitRequest = true
                                    self.blurAmount = 2
                                 
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                        self.blurAmount = 0.0
                                        self.showWaitRequest = false
                                    }
                                    
                                }
                            }
                            else if funcPressed == 3
                            {
                                if !self.showWaitRequest
                                {
                                    Detail.getClientsCount = 0
                                    Detail.wirelessClients.removeAll(keepingCapacity: true)
                                    Detail.mobilekick.removeAll(keepingCapacity: true)
                                    Detail.mobileIPSSID.removeAll(keepingCapacity: true)
                                    Detail.getWirelessClients(GHz: "2")
                                    Detail.getWirelessClients(GHz: "5")
                                    Detail.getWirelessClients(GHz: "6")
                                  
                                    self.showWaitRequest = true
                                    self.blurAmount = 2
                                 
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                        self.blurAmount = 0.0
                                        self.showWaitRequest = false
                                    }
                                    
                                }
                            }
                            else if funcPressed == 4
                            {
                                
                                if !self.showWaitRequest
                                {
                                    Detail.getDateAndTime()
                                  
                                    self.showWaitRequest = true
                                    self.blurAmount = 2
                                 
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                        self.blurAmount = 0.0
                                        self.showWaitRequest = false
                                    }
                                    
                                }
                            }
                            
                        }
                        else
                        {
                            if !self.showWaitRequest{
                               backPress.page = 1
                            }
                                
//                            if backPress.showSettingFlag {
//                                backPress.showSettingFlag = false
//                            }
//                            else if  backPress.showAboutFlag
//                            {
//                                backPress.showAboutFlag = false
//                            }
//                            else if !self.showWaitRequest{
//                               backPress.hideMenu = false
//                               backPress.page = 1
//                            }
                        }
                       // Detail.stopPinging()
                        
                    }.frame(width: 30.0, height: 30.0)
                    .padding(.top, 40)
                    .opacity(backPress.showSettingFlag || backPress.showAboutFlag ? 0:1)

                HStack{
                    //Image("sophoslogoWhite121")
                    //Image("sophoslogoWhite121_")
                    Image("icon-sophos-wireless")
                        .resizable()
                        .frame(width:28,height:24)
                    Text("Wireless")
                        .font(.InterSemiBold18)
                        .foregroundColor(Color.white)
                        .padding(.top, 2)
    
                }
                .frame(minWidth: 0,maxWidth: .infinity, maxHeight: 100, alignment: .center)
                .padding(.top, 40)
                .padding(.leading,-100)
               
            }
            .environmentObject(backPress)
            .padding(0)
            .frame(minWidth: 0,maxWidth: .infinity, maxHeight: 100, alignment: .leading)
            .background(Color.c0x001A47)
            
            showDetail().environmentObject(backPress)
                    VStack(spacing: 0) {
            showButton(width:geometry.size.width,height:geometry.size.height)
            showFunc(width:geometry.size.width)
                  //      .padding(.top,-8)
                    }
                        

            Image("shadow")
                .resizable()
                    .aspectRatio(UIImage(named: "shadow")!.size, contentMode: .fill)
                .frame(minWidth: 0,maxWidth: geometry.size.width, maxHeight:5, alignment: .leading)
            ZStack {
            TabView(selection: $funcPressed) {
                    
                GeometryReader { geometry in
                    ScrollView(.vertical) {
                        VStack(alignment:.leading, spacing:0){
                            System(width:geometry.size.width)
                                .gesture(drag)
                        }.frame(alignment: .center)
                            .padding(.bottom,20)
                            .gesture(drag)
                    }
                }.tag(0)
                .gesture(drag)
                
                GeometryReader { geometry in
                    ScrollView(.vertical) {
                        VStack(alignment:.leading, spacing:0){
                            LAN(width:geometry.size.width-40)
                                .gesture(drag)
                        }.frame(alignment: .center)
                            .padding(.bottom,20)
                            .gesture(drag)
                    }
                    .accessibilityIdentifier("lanscroll")
                }.tag(1)
                    .gesture(drag)
                
                GeometryReader { geometry in
                    ScrollView(.vertical) {
                      VStack(alignment:.leading, spacing:0){
                            WirelessBands(width:geometry.size.width-40)
                                .gesture(drag)

                        }.frame(alignment: .center)
                         .padding(.bottom,20)
                         .gesture(drag)
                    }
                    .accessibilityIdentifier("SSIDscroll")
                }.tag(2)
                    .gesture(drag)
                
                GeometryReader { geometry in
                    ScrollView(.vertical) {
                        VStack(alignment:.leading, spacing:0){
                           WirelessClients(width:geometry.size.width-40)
                                .gesture(drag)
                        }.frame(alignment: .center)
                            .padding(.bottom,20)
                            .gesture(drag)
                    }
                }.tag(3)
                    .gesture(drag)
                
                GeometryReader { geometry in
                    ScrollView(.vertical, showsIndicators: false) {
                        
                            VStack(alignment:.leading, spacing:0){
                               DateAndTime(width:geometry.size.width-40)
                                    .gesture(drag)
                            }.frame(alignment: .center)
                                .padding(.bottom,40)
                                .gesture(drag)
                    }
                     .accessibilityIdentifier("Datescroll")
                }.tag(4)
                    .gesture(drag)
                
                GeometryReader { geometry in
                    ScrollView(.vertical) {
                        VStack(alignment:.leading, spacing:0){
                            PingTest(width:geometry.size.width-40)
                                .gesture(drag)
                        }.frame(alignment: .center)
                            .padding(.bottom,20)
                            .gesture(drag)
                    }
                }.tag(5)
                    .gesture(drag)
                
                GeometryReader { geometry in
                    ScrollView(.vertical) {
                        VStack(alignment:.leading, spacing:0){
                           TracerouteTest(width:geometry.size.width-40)
                                .gesture(drag)
                        }.frame(alignment: .center)
                            .padding(.bottom,20)
                            .gesture(drag)
                    }
                }.tag(6)
                    .gesture(drag)
             }
             
             .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
             .onAppear(perform: {
                 
                 UITableView.appearance().alwaysBounceHorizontal = false
              })
             .adaptsToKeyboard()
             .gesture(
                    DragGesture()
                     .onChanged{_ in
                         UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                         self.lastHoveredId = ""
                               
                     }
             )
             .opacity(self.APnotFound ? 0.0:1.0)
             .id(funcPressed)
              
             Text("This AP can no longer be detected It maybe turned off or disconnected\n\n Please check its physical connections")
                    .multilineTextAlignment(.center)
                    .font(.InterRegular18)
                        .frame(width:geometry.size.width-40,alignment: .center)
                        .foregroundColor(Color.c0x242527)
                        .opacity(self.APnotFound ? 1.0:0.0)
                        .position(x: geometry.size.width/2, y: geometry.size.height/4 )
                        //.padding(.top,-30)
                }//ZStack
            
                    
            } // VStack
            .blur(radius: blurAmount,opaque: false)
            .disabled(blurAmount == 0 ? false : true)
//            .onTapGesture{
//
//                self.blurAmount = 0
//
//                self.showLANIPType = false
//                self.showLeaseTimeFlag = false
//                self.GateWayFromDHCPFlag = false
//                self.SecAddrFromDHCPFlag = false
//                self.PriAddrFromDHCPFlag = false
//                self.showBandsTaggle24G = false
//                self.showBandsTaggle5G = false
//                self.showBandsTaggle6G = false
//                self.showAuthType24GFlag = false
//                self.showAuthType5GFlag = false
//                self.showAuthType6GFlag = false
//                self.showLocalTimeYear = false
//                self.showLocalTimeMonth = false
//                self.showLocalTimeDay = false
//                self.showLocalTimeHours = false
//                self.showLocalTimeMinutes = false
//                self.showLocalTimeSeconds = false
//                self.showTimeZone = false
//                self.showNTPServerType = false
//            }
           
                ZStack {
            showsave(width:geometry.size.width)
            showRebootSuccess(width:geometry.size.width)
            showDialog(tag:"reboot",width:geometry.size.width,height:geometry.size.height, x:geometry.size.width / 2 , y:geometry.size.height/2 , Content: "Are you sure you wish to reboot your AP? \nClients will be disconnected")
            showWait(tag: "reboot", width: geometry.size.width, height: geometry.size.height, x: geometry.size.width / 2, y: geometry.size.height/2, Content: "This AP is now rebooting\nPlease wait")
            showRadioDialog(tag: "Radio", width:geometry.size.width,height:geometry.size.height, x:geometry.size.width / 2 , y:geometry.size.height/2 , Content: "Are you sure you wish to disable\nthe radio of your AP? \n\nClients will be disconnected")
            showList(width: geometry.size.width,height:geometry.size.height)
                
                about.init(presented: $backPress.showAboutFlag,width: geometry.size.width, height:geometry.size.height)
                    .animation(.default)
                settings.init(presented: $backPress.showSettingFlag,width: geometry.size.width, height:geometry.size.height)
                    .animation(.default)
                
                    SideMenu(width: 270,isOpen: self.menuOpen,menuClose: self.openMenu,height:geometry.size.height)
                        
            
                }
                .frame(width: geometry.size.width,height:geometry.size.height)
                .background(blurAmount == 0 ? Color.clear:Color.c0xFFFFFF_10)
                .onTapGesture{
                    
                    self.blurAmount = 0

                    self.showLANIPType = false
                    self.showLeaseTimeFlag = false
                    self.GateWayFromDHCPFlag = false
                    self.SecAddrFromDHCPFlag = false
                    self.PriAddrFromDHCPFlag = false
                    self.showBandsTaggle24G = false
                    self.showBandsTaggle5G = false
                    self.showBandsTaggle6G = false
                    self.showAuthType24GFlag = false
                    self.showAuthType5GFlag = false
                    self.showAuthType6GFlag = false
                    self.showLocalTimeYear = false
                    self.showLocalTimeMonth = false
                    self.showLocalTimeDay = false
                    self.showLocalTimeHours = false
                    self.showLocalTimeMinutes = false
                    self.showLocalTimeSeconds = false
                    self.showTimeZone = false
                    self.showNTPServerType = false
                }
                
            
            }
            
            
        }//ZStack
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
        .background(Color.white)
        .edgesIgnoringSafeArea(.all)
        .disabled(showWaitRequest)
        .onAppear(){
            
//            for family in UIFont.familyNames.sorted() {
//                let names = UIFont.fontNames(forFamilyName: family)
//                print("Family: \(family) Font names: \(names)")
//            }
            detail = backPress.accessDetail as MyAccessBody
            central = backPress.Central
            
            if detail.unknow! >= 6
            {
                self.APnotFound = true
                
            }
            Detail.update()
            Detail.myCount = backPress.myFlag
            Detail.MyAccess = backPress.myAccess
            
            /*System*/
            Detail.APAddress = backPress.APaddress
            Detail.DigestAccount = backPress.APaccount
            Detail.DigestPw  = backPress.APpw
            
            if backPress.getLoginResult != nil
            {
                Detail.getSystemResult = backPress.getLoginResult! as GETresponse
                Detail.updateR()
            }
            else
            {
                if self.APnotFound == false {
                Detail.getSystem()
                }
            }
            
           // value = backPress.value
            value1 = backPress.value1
//            if value != ""
//            {
//                Detail.system[0].value = value
//            }
            if value1 != ""
            {
                Detail.system[1].value = value1
            }
            /*LAN*/
//            if self.APnotFound == false {
//            Detail.getLANPort()
//            }
            /*wirelessBands*/
            Detail.initBands()

            if backPress.myWireless != []
            {
                Detail.wireless24G = backPress.myWireless
            }
            wirelessValue24G = Detail.wireless24G.map{$0.value!}
            
            //Detail.updateWireless5G()
            if backPress.myWireless != []
            {
                Detail.wireless5G = backPress.myWireless
            }
            wirelessValue5G = Detail.wireless5G.map{$0.value!}
            
//            Detail.updateWireless6G()
            if support6G.contains(detail.model!){
            if backPress.myWireless != []
            {
                Detail.wireless6G = backPress.myWireless
            }
            wirelessValue6G = Detail.wireless6G.map{$0.value!}
                
            }
            
            if backPress.wirelessType == []
            {
                backPress.wirelessType.append(true)
                backPress.wirelessType.append(true)
              //  if support6G.contains(detail.model!){
                backPress.wirelessType.append(true)
              //  }
            }

                Wireless24G = backPress.wirelessType[0]
                Wireless5G = backPress.wirelessType[1]
            if support6G.contains(detail.model!){
                Wireless6G = backPress.wirelessType[2]
            }
            
            /*0=Disable
            1=2.4G Only
            2=5G Only
            4=6G Only
            3=2.4G+5G
            5=2.4G+6G
            6=5G+6G
            7=2.4G+5G+6G*/
           /* if  detail.wifiradio == "0"
            {
                Wireless24G = false
                Wireless5G = false
                if support6G.contains(detail.model!){
                Wireless6G = false
                }
                disableRadio = true
            }
            else if detail.wifiradio == "1"
            {
                Wireless24G = true
                Wireless5G = false
                if support6G.contains(detail.model!){
                Wireless6G = false
                }
                disableRadio = false
            }
            else if detail.wifiradio == "2"
            {
                Wireless24G = false
                Wireless5G = true
                if support6G.contains(detail.model!){
                Wireless6G = false
                }
                disableRadio = false
            }
            else if detail.wifiradio == "3"
            {
                Wireless24G = true
                Wireless5G = true
                if support6G.contains(detail.model!){
                Wireless6G = false
                }
                disableRadio = false
            }
            else if detail.wifiradio == "4"
            {
                Wireless24G = false
                Wireless5G = false
                if support6G.contains(detail.model!){
                Wireless6G = true
                }
                disableRadio = false
            }
            else if detail.wifiradio == "5"
            {
                Wireless24G = true
                Wireless5G = false
                if support6G.contains(detail.model!){
                Wireless6G = true
                }
                disableRadio = false
            }
            else if detail.wifiradio == "6"
            {
                Wireless24G = false
                Wireless5G = true
                if support6G.contains(detail.model!){
                Wireless6G = true
                }
                disableRadio = false
            }
            else if detail.wifiradio == "7"
            {
                Wireless24G = true
                Wireless5G = true
                if support6G.contains(detail.model!){
                Wireless6G = true
                }
                disableRadio = false
            }*/
            Wireless24G = (detail.wifiradio == "1" || detail.wifiradio == "3" || detail.wifiradio == "5" || detail.wifiradio == "7") ? true:false
            Wireless5G = (detail.wifiradio == "2" || detail.wifiradio == "3" || detail.wifiradio == "6" || detail.wifiradio == "7") ? true:false
            if support6G.contains(detail.model!){
                Wireless6G = (detail.wifiradio == "4" || detail.wifiradio == "5" || detail.wifiradio == "6" || detail.wifiradio == "7") ? true:false
            }
            disableRadio = (detail.wifiradio == "0") ? true:false
            
            if detail.model!.contains("420") {
                SSIDArray = SSID420Array
            }
            else if detail.model!.contains("840") {
                SSIDArray = SSID840Array
            }
            
            /*WirelessClients*/
//            if self.APnotFound == false {
//            Detail.getWirelessClients(GHz:"2")
//            Detail.getWirelessClients(GHz:"5")
//            Detail.getWirelessClients(GHz:"6")
//            }
            
            if backPress.LANType == []
            {
                backPress.LANType.append(false)
            }
            else
            {
                LANIPTypeTaggle = backPress.LANType[0]
            }
            
            /*LocalTime*/
            Detail.initDateTime()
           // Detail.getDateAndTime()
//            Detail.updateLocalTime()
//            Detail.updateNTPTimeServer()
            
            Detail.updateEditStyle()
            
            
        }
        .onReceive(ManageViews.shared.$myAccess) { Result in
           // ForEach(0..<backPress.myAccess.count, id:\.self) { i in
                for i in backPress.myAccess {
                    if i.mac == detail.mac
                    {
                        detail = i
                        backPress.accessDetail = i
                      /*  if  i.wifiradio == "0"
                        {
                            Wireless24G = false
                            Wireless5G = false
                            Wireless6G = false
                            disableRadio = true
                        }
                        else if i.wifiradio == "1"
                        {
                            Wireless24G = true
                            Wireless5G = false
                            Wireless6G = false
                            disableRadio = false
                        }
                        else if i.wifiradio == "2"
                        {
                            Wireless24G = false
                            Wireless5G = true
                            Wireless6G = false
                            disableRadio = false
                        }
                        else if i.wifiradio == "3"
                        {
                            Wireless24G = true
                            Wireless5G = true
                            Wireless6G = false
                            disableRadio = false
                        }
                        else if i.wifiradio == "4"
                        {
                            Wireless24G = false
                            Wireless5G = false
                            Wireless6G = true
                            disableRadio = false
                        }
                        else if i.wifiradio == "5"
                        {
                            Wireless24G = true
                            Wireless5G = false
                            Wireless6G = true
                            disableRadio = false
                        }
                        else if i.wifiradio == "6"
                        {
                            Wireless24G = false
                            Wireless5G = true
                            Wireless6G = true
                            disableRadio = false
                        }
                        else if i.wifiradio == "7"
                        {
                            Wireless24G = true
                            Wireless5G = true
                            Wireless6G = true
                            disableRadio = false
                        }*/
                        Wireless24G = (i.wifiradio == "1" || i.wifiradio == "3" || i.wifiradio == "5" || i.wifiradio == "7") ? true:false
                        Wireless5G = (i.wifiradio == "2" || i.wifiradio == "3" || i.wifiradio == "6" || i.wifiradio == "7") ? true:false
                        if support6G.contains(i.model!){
                            Wireless6G = (i.wifiradio == "4" || i.wifiradio == "5" || i.wifiradio == "6" || i.wifiradio == "7") ? true:false
                        }
                        disableRadio = (i.wifiradio == "0") ? true:false
                        
                        if i.unknow! >= 6
                        {
                            self.APnotFound = true
                        }
                        else
                        {
                            self.APnotFound = false
                        }
                        Detail.APAddress = i.location!
                        
                        backPress.accessDetail.appname = i.appname
                    }
                }
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
        .background(Color.white)
        .edgesIgnoringSafeArea(.all)
    }//body
    
    func openMenu() {
        self.menuOpen.toggle()
    }
    
    @ViewBuilder
    func showDetail()->some View {
        //let detail = backPress.accessDetail as MyAccessBody
       //&& !self.APnotFound
        if central && !self.APnotFound {
                ZStack(alignment:.top){
                    GeometryReader { geometry in
                    Image("myTopCentral150")
                            .frame(maxWidth: .infinity,maxHeight: 80,alignment:.trailing)
                            .padding(.top,25)
                            .padding(.trailing,40)
                        
                    VStack(alignment:.leading){
//                        Text("\(detail.appname!)")
//                            .font(.InterBold24)
                        
                        MarqueeDetailTitleText(backPress: backPress,width: geometry.size.width-120)
                           // .frame(width: geometry.size.width / 2, height: 30, alignment: .leading)
                            .frame(width:geometry.size.width-120, height: 30, alignment: .leading)
                            .font(.InterBold24)
                            .clipped()
                        
                            
                            
                        Text("\(detail.mac!)")
                            .font(.InterRegular14)
                            
                        Text("2.4 GHz | 5GHz | 6GHz")
                            .font(.InterRegular14)
                            
                        HStack {
                            Image("detailClient")
                            Text(" \(detail.client!) connected clients")
                                .font(.InterMedium14)
                        }
                        .padding(.top , -5)
                    }
                    .frame(width:geometry.size.width,alignment:.leading)
                    .padding([.top,.leading],20)
                    
                        HStack {
                    Image("detailDelete15")
                            .resizable()
                            .frame(width:16,height:18,alignment:.trailing)
                        .padding(.top,15)
                        .onTapGesture {
                            if !self.editFlag{
                            if datas.deleteMyap(detail.mac!)
                                {
                                    Detail.delete(mac: detail.mac!)
                                    backPress.myAccess = Detail.MyAccess
                                    backPress.page = 1
                                datas.deleteLogin(detail.mac!)
                                }
                            }
                        }
                        .accessibility(addTraits: .isButton)
                        .accessibilityIdentifier("deleteCentral\(detail.mac!)")
                    }.frame(width:geometry.size.width-15, alignment:.trailing)
                        .background(Color.clear)
                        
                    }//geometry
                }
                .frame(maxHeight: 130 , alignment: .leading)
                .background(Color.c0x005BC8)
                .padding(.top,-10)
                .disabled(self.showWaitRequest)
        }
        else if self.APnotFound {
            ZStack(alignment:.top){
                GeometryReader { geometry in
                Image("myTop150fail")
                        .frame(maxWidth: .infinity,maxHeight: 70,alignment:.trailing)
                        .padding(.top,30)
                        .padding(.trailing,40)
                    
                    VStack(alignment:.leading, spacing:0){
//                    Text("\(detail.appname!)")
//                        .font(.InterBold24)
                        
                        MarqueeDetailTitleText(backPress: backPress,width: geometry.size.width-120)
                         //   .frame(width: geometry.size.width / 2, height: 30, alignment: .leading)
                            .frame(width:geometry.size.width-120,height: 30, alignment: .leading)
                            .font(.InterBold24)
                            .clipped()
                       
                  
                    HStack {
//                        Image("radioDisable")
//                            .resizable()
//                            .frame(width:20,height: 20,alignment:.leading)
                        Text("AP not found")
                            .font(.InterRegular14)
                    }
//                    Text("0")
//                        .font(.InterRegular14)
//                        .foregroundColor(Color(hex: 0x1987CB))
//                        .opacity(0.0)
//
                        
                    HStack {
                        Image("detailClient")
                        Text(" 0 connected clients")
                            .font(.InterMedium14)
                    }
                    .padding(.top , 10)
                }
                .frame(width:geometry.size.width,alignment:.leading)
                .padding([.top,.leading],20)
                
                    HStack {
                Image("detailDelete15")
                        .resizable()
                        .frame(width:16,height:18,alignment:.trailing)
                    .padding(.top,15)
                    .onTapGesture {
                        if !self.editFlag{
                            if datas.deleteMyap(detail.mac!)
                            {
                        Detail.delete(mac: detail.mac!)
                        backPress.myAccess = Detail.MyAccess
                        backPress.page = 1
                            datas.deleteLogin(detail.mac!)
                            }
                        }
                    }
                    .accessibility(addTraits: .isButton)
                    .accessibilityIdentifier("deleteAPnotFound\(detail.mac!)")
                }.frame(width:geometry.size.width-15, alignment:.trailing)
                    .background(Color.clear)
                }//geometry
            }
            .frame(maxHeight: 130 , alignment: .leading)
            .background(Color.c0x005BC8)
            .padding(.top,-10)
            .disabled(self.showWaitRequest)
        }
        else{
        if self.disableRadio
        {
            //let detail = backPress.accessDetail as MyAccessBody
            ZStack(alignment:.top){
                GeometryReader { geometry in
                Image("myTop150fail")
                        .frame(maxWidth: .infinity,maxHeight: 70,alignment:.trailing)
                        .padding(.top,30)
                        .padding(.trailing,40)
                    
                    VStack(alignment:.leading, spacing:0){
//                    Text("\(detail.appname!)")
//                        .font(.InterBold24)
                       
                        MarqueeDetailTitleText(backPress: backPress,width: geometry.size.width-120)
                            .frame(width: geometry.size.width - 120, height: 30, alignment: .leading)
                         //   .frame(width:233,height: 30, alignment: .leading)
                            .font(.InterBold24)
                            .clipped()
                      
                  
                    HStack {
                        Image("radioDisable")
                            .resizable()
                            .frame(width:20,height: 20,alignment:.leading)
                        Text("AP radio is disabled")
                            .font(.InterRegular14)
                    }
//                    Text("0")
//                        .font(.InterRegular14)
//                        .foregroundColor(Color(hex: 0x1987CB))
                        
                    HStack {
                        Image("detailClient")
                        Text(" 0 connected clients")
                            .font(.InterMedium14)
                    }
                    .padding(.top , 10)
                }
                .frame(width:geometry.size.width,alignment:.leading)
                .padding([.top,.leading],20)
                
                    HStack {
                Image("detailDelete15")
                        .resizable()
                        .frame(width:16,height:18,alignment:.trailing)
                    .padding(.top,15)
                    .onTapGesture {
                        if !self.editFlag{
                            if datas.deleteMyap(detail.mac!)
                            {
                        Detail.delete(mac: detail.mac!)
                        backPress.myAccess = Detail.MyAccess
                        backPress.page = 1
                            datas.deleteLogin(detail.mac!)
                            }
                        }
                    }
                    .accessibility(addTraits: .isButton)
                    .accessibilityIdentifier("deleteRadioDisable\(detail.mac!)")
                }.frame(width:geometry.size.width-15, alignment:.trailing)
                    .background(Color.clear)
                }//geometry
            }
            .frame(maxHeight: 130 , alignment: .leading)
            .background(Color.c0x005BC8)
            .padding(.top,-10)
            .disabled(self.showWaitRequest)
        }
        else if self.disableRadio == false && central == false
        {
           // let detail = backPress.accessDetail as MyAccessBody
            ZStack(alignment:.top){
                GeometryReader { geometry in
                Image("myTop150")
                    .frame(maxWidth: .infinity,maxHeight: 80,alignment:.trailing)
                    .padding(.top,15)
                    .padding(.trailing,40)
                    
                VStack(alignment:.leading){
//                    Text("\(detail.appname!)")
//                        .font(.InterBold24)
                   // MarqueeDetailTitleText(text: "\(backPress.accessDetail.appname!)")
//                    ForEach(0..<backPress.myAccess.count, id:\.self) { i in
//                        if backPress.myAccess[i].mac == detail.mac
//                        {
//                            MarqueeDetailTitleText(text: "\(backPress.myAccess[i].appname!)")
//                                .frame(width:233,height: 30, alignment: .leading)
//                                .font(.InterBold24)
//                                .clipped()
//
//                        }
//                    }
//                      //  .frame(width: geometry.size.width / 2, height: 30, alignment: .leading)
                   // ForEach(0..<backPress.myAccess.count, id:\.self) { i in
                  //     if backPress.myAccess[i].mac == detail.mac
                   //     {
                    MarqueeDetailTitleText(backPress: backPress,width: geometry.size.width-120)
                       // .frame(width:233,height: 30, alignment: .leading)
                        .frame(width:geometry.size.width - 120 , height: 30, alignment: .leading )
                        .font(.InterBold24)
                        .clipped()
                        .padding(.top , -10)
                  //      }
                  //  }
                        
                    Text("\(detail.mac!)")
                        .font(.InterRegular14)
                        .padding(.top , -5)
                    

                    
                    if Wireless24G && Wireless5G && Wireless6G
                    {
                        Text("2.4 GHz | 5GHz | 6GHz")
                            .font(.InterRegular14)
                            .padding(.top , -5)
                    }
                    else if Wireless24G && Wireless5G
                    {
                        Text("2.4 GHz | 5GHz")
                            .font(.InterRegular14)
                            .padding(.top , -5)
                    }
                    else if Wireless24G && Wireless6G
                    {
                        Text("2.4 GHz | 6GHz")
                            .font(.InterRegular14)
                            .padding(.top , -5)
                    }
                    else if Wireless5G && Wireless6G
                    {
                        Text("5GHz | 6GHz")
                            .font(.InterRegular14)
                            .padding(.top , -5)
                    }
                    else if Wireless24G
                    {
                        Text("2.4 GHz")
                            .font(.InterRegular14)
                            .padding(.top , -5)
                    }
                    else if Wireless5G
                    {
                        Text("5GHz")
                            .font(.InterRegular14)
                            .padding(.top , -5)
                    }
                    else if Wireless6G
                    {
                        Text("6GHz")
                            .font(.InterRegular14)
                            .padding(.top , -5)
                    }
                    
                        
                    HStack {
                        Image("detailClient")
                        
                        ForEach(0..<backPress.myAccess.count, id:\.self) { i in
                            if backPress.myAccess[i].mac == detail.mac
                            {
                                Text("\(backPress.myAccess[i].client!) connected clients")
                                .font(.InterMedium14)
                                  
                            }
                        }
                        
                    }
                    .padding(.top , -3)
//                    .onReceive(backPress.$myAccess) { Result in
//                       // ForEach(0..<backPress.myAccess.count, id:\.self) { i in
//                            for i in backPress.myAccess {
//                                if i.mac == detail.mac
//                                {
//                                    detail = i
//                                    if  i.wifiradio == "0"
//                                    {
//                                        Wireless24G = false
//                                        Wireless5G = false
//                                        Wireless6G = false
//                                        disableRadio = true
//                                    }
//                                }
//                            }
//                    }
                }
                .frame(width:geometry.size.width,alignment:.leading)
                .padding([.top,.leading],20)
                .onAppear {
                    var currentNetworkInfo: [String: Any] = [:]
                    getNetworkInfo { (wifiInfo) in

                      currentNetworkInfo = wifiInfo
                        NSLog("currentNetworkInfo : \(currentNetworkInfo["SSID"])")
                    }
                    
                    var arr  = self.getIPAddress()
                    NSLog("arr : \(arr)")
                }
                
                    HStack {
                Image("detailDelete15")
                    .resizable()
                    .frame(width:16,height:18,alignment:.trailing)
                    .padding(.top,15)
                    .onTapGesture {
                        if !self.editFlag{
                            if datas.deleteMyap(detail.mac!)
                            {
                        Detail.delete(mac: detail.mac!)
                        backPress.myAccess = Detail.MyAccess
                        backPress.page = 1
                          let a =   datas.deleteLogin(detail.mac!)
                                print(a)
                            }
                        }
                    }
                    .accessibility(addTraits: .isButton)
                    .accessibilityIdentifier("delete\(detail.mac!)")
                        
                  }.frame(width:geometry.size.width-15, alignment:.trailing)
                        .background(Color.clear)
                }//geometry
            }
            .frame(maxHeight: 130 , alignment: .leading)
            .background(Color.c0x005BC8)
            .padding(.top,-10)
            .disabled(self.showWaitRequest)
        }
        }
    }
    @ViewBuilder
    func showButton(width:CGFloat,height:CGFloat)->some View {
        if central
        {
            HStack(spacing:0) {
//                Link("This AP is manageed by Sophos Central. To manage this\nAP please login to Sophos Central by clicking here", destination: URL(string:"https://www.google.com.tw")!)
//                    .underline()
//                    .font(.InterRegular14)
//                    .foregroundColor(Color.white)
//                .frame(width: width , height: 50)
//                .background(Color.c0x242527)
//                .padding(.top,-10)
                
                Link(destination: URL(string: "https://www.sophos.com/en-us/legal/sophos-end-user-terms-of-use")!, label: {
                    Text("This AP is manageed by Sophos Central. To manage this\nAP please login to Sophos Central by clicking here")
                        .underline()
                        .font(.InterRegular14)
                        .foregroundColor(Color.white)
                        .background(Color.c0x242527)
                        .frame(width: width , height: 50)
                        //.padding(.top,-10)
                        .accessibilityIdentifier("gotoSophos")
                        
                })
                 
                .frame(width: width,height: 50)
                .background(Color.c0x242527)
            }
            .padding(.top,-10)
        }
        else if self.APnotFound
        {
            HStack(spacing:0) {
                Image("radioDisable")
                Text("AP not found")
                .foregroundColor(Color.c0x242527)
            }
            .frame(width: width , height: 50)
            .padding(.top,-10)
            .background(Color.white)
        }
        else{
            if self.editFlag == false {
                
                HStack(spacing: 0) {
                    Button{
                        if funcPressed != 3 ||  funcPressed != 5 || funcPressed != 6
                        {
                        self.editFlag = true
                            self.diableFlag = false
                        }
                        else
                        {
                            self.diableFlag = true
                        }
//                        self.Wireless24G = true
//                        self.Wireless5G = true
//                        self.Wireless6G = true
//                        backPress.wirelessType[0] = self.Wireless24G
//                        backPress.wirelessType[1] = self.Wireless5G
//                        backPress.wirelessType[2] = self.Wireless6G
                        backPress.LANType[0] = self.LANIPTypeTaggle
                    } label: {
                        ZStack{
                            RoundedRectangle(cornerRadius: 0)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                            HStack {
                            Image("edit")
                            
                            Text("Edit")
                                //    .frame(minWidth: 0, maxWidth: 70, minHeight: 0, maxHeight: 40,alignment: .leading)
                                .font(.InterRegular18)
                                .foregroundColor(Color.c0x242527)
                            }
                            .frame(alignment:.center)
                            .opacity(diableFlag ? 0.3:1.0)
                            
                        }
                    }.frame(width: width/3 , height: 50)
                        .buttonStyle(PlainButtonStyle())
                        .disabled(diableFlag)
                        
                        
                           
                    Button{
                        self.showAlert = true
                        self.blurAmount = 2
                       // client.SSDPResults.removeAll()
                        self.timer = Timer.publish(every: 0.1, on: .main, in: .common).autoconnect()
                    } label: {
                        ZStack{
                            RoundedRectangle(cornerRadius: 0)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                            HStack {
                        Image("reboot")
                        
                        Text("Reboot")
                            //.frame(minWidth: 0, maxWidth: 70, minHeight: 0, maxHeight: 40,alignment: .leading)
                            .font(.InterRegular18)
                            .foregroundColor(Color.c0x242527)
                            }
                            .frame(alignment:.center)
                        }
                        
                    }.frame(width: width/3 , height: 50)
                        .buttonStyle(PlainButtonStyle())
                        .accessibilityIdentifier("reboot")
                        
                        
                    if self.disableRadio
                   {
                       Button{

                          // self.showRadioONAlert = true
                           self.blurAmount = 2
                           let ssdp = datas.getCurrentEableMatch(detail.mac!)
                           if ssdp.WiFiRadio != "" &&  ssdp.WiFiRadio != "0"
                           {
                               NSLog("OffWiFi :\(ssdp.WiFiRadio)")
                               Detail.getOffWiFiResult = 0
                               Detail.OnWiFi(wifiradoi:ssdp.WiFiRadio!,model: detail.model!)
                               self.check = true
                               
                           }
                           else
                           {
                               Detail.getOffWiFiResult = 0
                               if support6G.contains(detail.model!){
                                   Detail.OnWiFi(wifiradoi:"7",model: detail.model!)
                               }
                               else
                               {
                                   Detail.OnWiFi(wifiradoi:"3",model: detail.model!)
                               }
                               self.check = true
                           }
                           
                       } label: {
                           ZStack{
                               RoundedRectangle(cornerRadius: 0)
                                   .stroke(Color.c0x696A6B, lineWidth: 1)
                               HStack {
                           Image("disable")
                           
                           Text("Enable")
                               //.frame(minWidth: 0, maxWidth: 70, minHeight: 0, maxHeight: 40,alignment: .leading)
                               .font(.InterRegular18)
                               .foregroundColor(Color.c0x242527)
                               }
                               .frame(alignment:.center)
                           }
                       }.frame(width: width/3 , height: 50)
                           .buttonStyle(PlainButtonStyle())
                           .onReceive(Detail.$getOffWiFiResult) { Result in
                               NSLog("OffWiFi Detail.getOffWiFiResult :\(Result)")
                               var requests = 2
                               if support6G.contains(detail.model!)
                               {
                                   requests = 3
                               }
                               if Result == requests && self.check == true
                               {
                                   self.showRadioONAlert = false
                                   blurAmount = 0
//                                    self.disableRadio = false
//                                    self.Wireless24G = true
//                                    self.Wireless5G = true
//                                    self.Wireless6G = true
                                   
                                   Detail.getOffWiFiResult = 0
                                   self.check = false
                                   
                                   Detail.postsysReload()
                                   
                                   self.showWIFIAlert = true
                                   self.blurAmount = 2
                                 //  self.sysReloadflag = true
                                 //  self.sysReloadTime = 0.002
                                   self.sysReloadTimeCout = false
                                   self.sysReloadTime = 0
                               }
                           }
                           .accessibility(addTraits: .isButton)
                           .accessibilityIdentifier("wifienable")
                   }
                   else
                   {
                       Button{
                           
                           self.showRadioAlert = true
                           self.blurAmount = 2
//                           self.disableRadio = true
//                           self.Wireless24G = false
//                           self.Wireless5G = false
//                           self.Wireless6G = false
                           
                       } label: {
                           ZStack{
                               RoundedRectangle(cornerRadius: 0)
                                   .stroke(Color.c0x696A6B, lineWidth: 1)
                               HStack {
                           Image("radioEnable")
                                       .resizable()
                                       .frame(width: 23, height: 21)
                           
                           Text("Disable")
                              // .frame(minWidth: 0, maxWidth: 70, minHeight: 0, maxHeight: 40,alignment: .leading)
                               .font(.InterRegular18)
                               .foregroundColor(Color.c0x242527)
                               }
                               .frame(alignment:.center)
                           }
                       }.frame(width: width/3 , height: 50)
                           .buttonStyle(PlainButtonStyle())
                           .accessibilityIdentifier("wifidisable")
                   }
                  
                }//HStack
                .frame(width: width , height: 50)
                .padding(.top,-10)
                .disabled(self.showWaitRequest)
            }
            else
            {
                HStack(spacing: 0) {
                    
                    Button{
                        
                        //backPress.value = value
                        backPress.value1 = value1
                        
//                        var count = 0
//                        Detail.wireless24G.forEach { item in
//                            wirelessValue24G[count] = item.value ?? ""
//                            count += 1
//                        }
//
//                        count = 0
//                        Detail.wireless5G.forEach { item in
//                            wirelessValue5G[count] = item.value ?? ""
//                            count += 1
//                        }
//
//                        count = 0
//                        Detail.wireless6G.forEach { item in
//                            wirelessValue6G[count] = item.value ?? ""
//                            count += 1
//                        }
                    
                        
                        if self.funcPressed == 0
                        {
                            if Detail.system[1].value != backPress.value1
                            {
                                self.showWaitRequest = true
                                self.blurAmount = 2
                                self.sysReloadTimeCout = false
                                self.sysReloadTime = 0
                                Detail.getAdministration(productName:backPress.value1)
                               // self.sysReloadflag = true
                               // self.sysReloadTime = 0.005
                                //self.timer = Timer.publish(every: 1.0, on: .main, in: .common).autoconnect()
                                
                            }
                            else
                            {
                                self.editFlag = false
                                
                                return
                            }
                            
                        }
                        else if self.funcPressed == 1
                        {
                            self.showWaitRequest = true
                            self.blurAmount = 2
                            self.sysReloadTimeCout = false
                            self.sysReloadTime = 0
                            if let idx = self.LANIPTypeArray.firstIndex(where: { $0 == self.LANIPType }) {
                                Detail.postLANPortBody.ipv4?.ipv4_type = idx
                                
                                if idx == 0
                                {
                                let result = calculatePressed(subnetMask: Detail.LANIPAddress[1].value!, IPAddress: Detail.LANIPAddress[0].value!)
                                
                                let s = IPToInt(ip: result[0])
                                let e = IPToInt(ip: result[1])
                                let g = IPToInt(ip: Detail.LANIPAddress[2].value!)
                                let us = IPToInt(ip: Detail.LANDHCPServer[0].value!)
                                let ue = IPToInt(ip: Detail.LANDHCPServer[1].value!)
                                 
                                if g < s ||  g > e
                                {
                                    //Gateway
                                    
                                    self.saveErrFlag = true
                                    self.saveErr = "Default Gateway"
                                }
                                if us < s ||  us > e
                                {
                                    //Start IP
                                    self.saveErrFlag = true
                                    if self.saveErr != ""
                                    {
                                    self.saveErr = self.saveErr + " , Starting IP Address"
                                    }
                                    else
                                    {
                                        self.saveErr = "Starting IP Address"
                                    }
                                }
                                if ue < s ||  ue > e
                                {
                                    //End IP
                                    self.saveErrFlag = true
                                    if self.saveErr != ""
                                    {
                                    self.saveErr = self.saveErr + " , Ending IP Address"
                                    }
                                    else
                                    {
                                        self.saveErr = "Ending IP Address"
                                    }
                                }
                                if us > ue
                                {
                                    self.saveErrFlag = true
                                    if self.saveErr != ""
                                    {
                                    self.saveErr = self.saveErr + "\n  Invalid Host Address Range"
                                    }
                                    else
                                    {
                                        self.saveErr = "Invalid Host Address Range"
                                    }
                                }
                                if self.saveErr != ""
                                {
                                    self.showWaitRequest = false
                                    self.blurAmount = 0
                                    self.sysReloadTimeCout = false
                                    self.sysReloadTime = 0
                                    return
                                }
                                }
                            }
                            
                            
//                            if let idx = self.LANIPTypeArray.firstIndex(where: { $0 == self.LANIPType }) {
//                                Detail.postLANPortBody.ipv4?.ipv4_type = idx
//                            }
                            Detail.postLANPortBody.ipv4?.ipv4_address = Detail.LANIPAddress[0].value
                            Detail.postLANPortBody.ipv4?.ipv4_subnet_mask = Detail.LANIPAddress[1].value
                            if let idx = self.FromDHCPArray.firstIndex(where: { $0 == self.GateWayFromDHCP }) {
                                Detail.postLANPortBody.ipv4?.ipv4_default_gateway_type = idx
                                
                                if Detail.postLANPortBody.ipv4?.ipv4_type == 0
                                {
                                    Detail.postLANPortBody.ipv4?.ipv4_default_gateway_type = 0
                                }
                            }
                           
                            Detail.postLANPortBody.ipv4?.ipv4_default_gateway = Detail.LANIPAddress[2].value
                            if let idx = self.FromDHCPArray.firstIndex(where: { $0 == self.PriAddrFromDHCP }) {
                                Detail.postLANPortBody.ipv4?.ipv4_dns_primary_type = idx
                                
                                if Detail.postLANPortBody.ipv4?.ipv4_type == 0
                                {
                                    Detail.postLANPortBody.ipv4?.ipv4_dns_primary_type = 0
                                }
                            }
                            Detail.postLANPortBody.ipv4?.ipv4_dns_primary = Detail.LANDNSServers[0].value
                            if let idx = self.FromDHCPArray.firstIndex(where: { $0 == self.SecAddrFromDHCP }) {
                                Detail.postLANPortBody.ipv4?.ipv4_dns_secondary_type = idx
                                if Detail.postLANPortBody.ipv4?.ipv4_type == 0
                                {
                                    Detail.postLANPortBody.ipv4?.ipv4_dns_secondary_type = 0
                                }
                            }
                            Detail.postLANPortBody.ipv4?.ipv4_dns_secondary = Detail.LANDNSServers[1].value
                            Detail.postLANPortBody.ipv4?.dhcp_enabled = (self.LANIPTypeTaggle  == true ? 1:0)
                            Detail.postLANPortBody.ipv4?.dhcp_start_addr = Detail.LANDHCPServer[0].value
                            Detail.postLANPortBody.ipv4?.dhcp_end_addr = Detail.LANDHCPServer[1].value
                            Detail.postLANPortBody.ipv4?.dhcp_domain_name = Detail.LANDHCPServer[2].value
                            if let idx = webLeaseTimeArray.firstIndex(where: { $0 == Detail.LANDHCPServer[3].value }) {
                                Detail.postLANPortBody.ipv4?.dhcp_lease_time = idx
                            }
                            else
                            {
                                Detail.postLANPortBody.ipv4?.dhcp_lease_time = webLeaseTimeArray.count - 1
                            }
                           
                            Detail.postLANPortBody.ipv4?.dhcp_dns_primary = Detail.LANDHCPServer[4].value
                            Detail.postLANPortBody.ipv4?.dhcp_dns_secondary = Detail.LANDHCPServer[5].value
                            
                            Detail.postLANPort()
                        }
                        else if self.funcPressed == 2
                        {
                            self.showWaitRequest = true
                            self.blurAmount = 2
                            self.sysReloadTimeCout = false
                            self.sysReloadTime = 0
                            
                            Detail.post24GBandsBody.radio_enable = (Wireless24G ? 1:0)
                            Detail.post24GBandsBody.enabled_ssidNum = Int(WiFiBands24G)!
                            for i in 0..<Int(WiFiBands24G)!
                            {
                                Detail.post24GBandsBody.ssidNames![i].ssidname = Detail.wireless24G[i*4+0].value
                                Detail.post24GSSIDSecurity[i].auth_detail?.psk_value = Detail.wireless24G[i*4+2].value
                                Detail.post24GBandsBody.ssidNames![i].vlan_id = Int(Detail.wireless24G[i*4+3].value!)!
                                
                                if Detail.wireless24G[i*4+1].value == "None"
                                {
                                    Detail.post24GSSIDSecurity[i].auth_method = 0
                                }
                                else if Detail.wireless6G[i*4+1].value == "WEP"
                                {
                                    Detail.post6GSSIDSecurity[i].auth_method = 1
                                }
                                else if Detail.wireless24G[i*4+1].value == "WPA/WPA2 Mixed Mode-AES"
                                {
                                    Detail.post24GSSIDSecurity[i].auth_method = 2
                                    Detail.post24GSSIDSecurity[i].auth_detail?.wpa_type = 8
                                    Detail.post24GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                }
                                else if Detail.wireless24G[i*4+1].value == "WPA2-AES"
                                {
                                    Detail.post24GSSIDSecurity[i].auth_method = 2
                                    Detail.post24GSSIDSecurity[i].auth_detail?.wpa_type = 5
                                    Detail.post24GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                }
                                else if Detail.wireless24G[i*4+1].value == "WPA2/WPA3 Mixed Mode-AES"
                                {
                                    Detail.post24GSSIDSecurity[i].auth_method = 2
                                    Detail.post24GSSIDSecurity[i].auth_detail?.wpa_type = 11
                                    Detail.post24GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                }
                                else if Detail.wireless24G[i*4+1].value == "WPA3-AES"
                                {
                                    Detail.post24GSSIDSecurity[i].auth_method = 2
                                    Detail.post24GSSIDSecurity[i].auth_detail?.wpa_type = 10
                                    Detail.post24GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                }
                                else if Detail.wireless24G[i*4+1].value == "WPA/WPA2 Mixed Mode-EAP-AES"
                                {
                                    Detail.post24GSSIDSecurity[i].auth_method = 3
                                    Detail.post24GSSIDSecurity[i].auth_detail?.wpa_type = 8
                                    Detail.post24GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                }
                                else if Detail.wireless24G[i*4+1].value == "WPA2-EAP-AES"
                                {
                                    Detail.post24GSSIDSecurity[i].auth_method = 3
                                    Detail.post24GSSIDSecurity[i].auth_detail?.wpa_type = 5
                                    Detail.post24GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                }
                                else if Detail.wireless24G[i*4+1].value == "WPA3-EAP-AES"
                                {
                                    Detail.post24GSSIDSecurity[i].auth_method = 3
                                    Detail.post24GSSIDSecurity[i].auth_detail?.wpa_type = 10
                                    Detail.post24GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                }
                                else if Detail.wireless24G[i*4+1].value == "OWE"
                                {
                                    Detail.post24GSSIDSecurity[i].auth_method = 5
                                }
                            }
//
//                            Detail.post24GBands()
//                            NSLog("post24GBands")
//                            for i in 0..<Int(WiFiBands24G)!
//                            {
//                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.001) {
//                                    Detail.post24GSecurity(ssid_index:"\(i)")
//                                    NSLog("post24GSecurity:\(i)")
//                                }
//                            }
                            
                            Detail.post5GBandsBody.radio_enable = (Wireless5G ? 1:0)
                            Detail.post5GBandsBody.enabled_ssidNum = Int(WiFiBands5G)!
                            for i in 0..<Int(WiFiBands5G)!
                            {
                                NSLog((Detail.post5GBandsBody.ssidNames?[i].ssidname)!)
                                NSLog(Detail.wireless5G[i*4+0].value!)
                                Detail.post5GBandsBody.ssidNames?[i].ssidname = Detail.wireless5G[i*4+0].value
                                Detail.post5GSSIDSecurity[i].auth_detail?.psk_value = Detail.wireless5G[i*4+2].value
                                Detail.post5GBandsBody.ssidNames![i].vlan_id = Int(Detail.wireless5G[i*4+3].value!)!
                                
                                if Detail.wireless5G[i*4+1].value == "None"
                                {
                                    Detail.post5GSSIDSecurity[i].auth_method = 0
                                }
                                else if Detail.wireless5G[i*4+1].value == "WEP"
                                {
                                    Detail.post5GSSIDSecurity[i].auth_method = 1
                                }
                                else if Detail.wireless5G[i*4+1].value == "WPA/WPA2 Mixed Mode-AES"
                                {
                                    Detail.post5GSSIDSecurity[i].auth_method = 2
                                    Detail.post5GSSIDSecurity[i].auth_detail?.wpa_type = 8
                                    Detail.post5GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                }
                                else if Detail.wireless5G[i*4+1].value == "WPA2-AES"
                                {
                                    Detail.post5GSSIDSecurity[i].auth_method = 2
                                    Detail.post5GSSIDSecurity[i].auth_detail?.wpa_type = 5
                                    Detail.post5GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                }
                                else if Detail.wireless5G[i*4+1].value == "WPA2/WPA3 Mixed Mode-AES"
                                {
                                    Detail.post5GSSIDSecurity[i].auth_method = 2
                                    Detail.post5GSSIDSecurity[i].auth_detail?.wpa_type = 11
                                    Detail.post5GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                }
                                else if Detail.wireless5G[i*4+1].value == "WPA3-AES"
                                {
                                    Detail.post5GSSIDSecurity[i].auth_method = 2
                                    Detail.post5GSSIDSecurity[i].auth_detail?.wpa_type = 10
                                    Detail.post5GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                }
                                else if Detail.wireless5G[i*4+1].value == "WPA/WPA2 Mixed Mode-EAP-AES"
                                {
                                    Detail.post5GSSIDSecurity[i].auth_method = 3
                                    Detail.post5GSSIDSecurity[i].auth_detail?.wpa_type = 8
                                    Detail.post5GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                }
                                else if Detail.wireless5G[i*4+1].value == "WPA2-EAP-AES"
                                {
                                    Detail.post5GSSIDSecurity[i].auth_method = 3
                                    Detail.post5GSSIDSecurity[i].auth_detail?.wpa_type = 5
                                    Detail.post5GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                }
                                else if Detail.wireless5G[i*4+1].value == "WPA3-EAP-AES"
                                {
                                    Detail.post5GSSIDSecurity[i].auth_method = 3
                                    Detail.post5GSSIDSecurity[i].auth_detail?.wpa_type = 10
                                    Detail.post5GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                }
                                else if Detail.wireless5G[i*4+1].value == "OWE"
                                {
                                    Detail.post5GSSIDSecurity[i].auth_method = 5
                                }
                               
                            }
                            
//                            Detail.post5GBands()
//                            NSLog("post5GBands")
//                            for i in 0..<Int(WiFiBands5G)!
//                            {
//                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.001) {
//                                    Detail.post5GSecurity(ssid_index:"\(i)")
//                                    NSLog("post5GSecurity:\(i)")
//                                }
//                            }
                            if support6G.contains(detail.model!)
                            {
                            Detail.post6GBandsBody.radio_enable = (Wireless6G ? 1:0)
                            Detail.post6GBandsBody.enabled_ssidNum = Int(WiFiBands6G)!
                            
                                for i in 0..<Int(WiFiBands6G)!
                                {
                                    NSLog((Detail.post6GBandsBody.ssidNames?[i].ssidname)!)
                                    NSLog(Detail.wireless6G[i*4+0].value!)
                                    Detail.post6GBandsBody.ssidNames?[i].ssidname = Detail.wireless6G[i*4+0].value
                                    Detail.post6GSSIDSecurity[i].auth_detail?.psk_value = Detail.wireless6G[i*4+2].value
                                    Detail.post6GBandsBody.ssidNames![i].vlan_id = Int(Detail.wireless6G[i*4+3].value!)!
                                    
                                    if Detail.wireless6G[i*4+1].value == "None"
                                    {
                                        Detail.post6GSSIDSecurity[i].auth_method = 0
                                    }
                                    else if Detail.wireless6G[i*4+1].value == "WEP"
                                    {
                                        Detail.post6GSSIDSecurity[i].auth_method = 1
                                    }
                                    else if Detail.wireless6G[i*4+1].value == "WPA/WPA2 Mixed Mode-AES"
                                    {
                                        Detail.post6GSSIDSecurity[i].auth_method = 2
                                        Detail.post6GSSIDSecurity[i].auth_detail?.wpa_type = 8
                                        Detail.post6GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                    }
                                    else if Detail.wireless6G[i*4+1].value == "WPA2-AES"
                                    {
                                        Detail.post6GSSIDSecurity[i].auth_method = 2
                                        Detail.post6GSSIDSecurity[i].auth_detail?.wpa_type = 5
                                        Detail.post6GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                    }
                                    else if Detail.wireless5G[i*4+1].value == "WPA2/WPA3 Mixed Mode-AES"
                                    {
                                        Detail.post6GSSIDSecurity[i].auth_method = 2
                                        Detail.post6GSSIDSecurity[i].auth_detail?.wpa_type = 11
                                        Detail.post6GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                    }
                                    else if Detail.wireless6G[i*4+1].value == "WPA3-AES"
                                    {
                                        Detail.post6GSSIDSecurity[i].auth_method = 2
                                        Detail.post6GSSIDSecurity[i].auth_detail?.wpa_type = 10
                                        Detail.post6GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                    }
                                    else if Detail.wireless6G[i*4+1].value == "WPA/WPA2 Mixed Mode-EAP-AES"
                                    {
                                        Detail.post6GSSIDSecurity[i].auth_method = 3
                                        Detail.post6GSSIDSecurity[i].auth_detail?.wpa_type = 8
                                        Detail.post6GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                    }
                                    else if Detail.wireless6G[i*4+1].value == "WPA2-EAP-AES"
                                    {
                                        Detail.post6GSSIDSecurity[i].auth_method = 3
                                        Detail.post6GSSIDSecurity[i].auth_detail?.wpa_type = 5
                                        Detail.post6GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                    }
                                    else if Detail.wireless6G[i*4+1].value == "WPA3-EAP-AES"
                                    {
                                        Detail.post6GSSIDSecurity[i].auth_method = 3
                                        Detail.post6GSSIDSecurity[i].auth_detail?.wpa_type = 10
                                        Detail.post6GSSIDSecurity[i].auth_detail?.encrypt_type = 3
                                    }
                                    else if Detail.wireless6G[i*4+1].value == "OWE"
                                    {
                                        Detail.post6GSSIDSecurity[i].auth_method = 5
                                    }
                                   
                                }
                                
                                
//                            Detail.post6GBands()
//                            NSLog("post6GBands")
//                            for i in 0..<Int(WiFiBands6G)!
//                            {
//                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.001) {
//                                    Detail.post6GSecurity(ssid_index:"\(i)")
//                                    NSLog("post6GSecurity:\(i)")
//                                }
//                            }
                                
                                
                            }
                            if support6G.contains(detail.model!)
                            {
                                self.enableSSID = 3 + Int(WiFiBands24G)! + Int(WiFiBands5G)! //+ Int(WiFiBands6G)!
                            }
                            else
                            {
                                self.enableSSID = 2 + Int(WiFiBands24G)! + Int(WiFiBands5G)!
                            }
                            NSLog("POST WIFI Start : \(enableSSID)")
                            Detail.postSSIDs(model: detail.model!)
                          //  self.sysReloadflag = true
                          //  self.sysReloadTime = 0.001
                            self.wifiApply = true
                           // self.timer = Timer.publish(every: 1.0, on: .main, in: .common).autoconnect()
                        }
                        else if self.funcPressed == 4
                        {
                            self.showWaitRequest = true
                            self.blurAmount = 2
                            self.sysReloadTimeCout = false
                            self.sysReloadTime = 0
                            Detail.postDateAndTimeBody.local_time_year = Int(Detail.LocalTime[0].value!)
                            Detail.postDateAndTimeBody.local_time_month = Detail.month.firstIndex(where: {$0 == Detail.LocalTime[1].value})! + 1
                            Detail.postDateAndTimeBody.local_time_day = Int(Detail.LocalTime[2].value!)
                            Detail.postDateAndTimeBody.local_time_hour = Int(Detail.LocalTime[3].value!)
                            Detail.postDateAndTimeBody.local_time_minute = Int(Detail.LocalTime[4].value!)
                            Detail.postDateAndTimeBody.local_time_seconde = Int(Detail.LocalTime[5].value!)
                            Detail.postDateAndTimeBody.time_zone = TimeZoneList.firstIndex(where: {$0 == self.timeZone})
                            Detail.postDateAndTimeBody.ntp_enabled = (self.onNTPServer == true ? 1:0)
                            Detail.postDateAndTimeBody.ntp_server_name = self.FromNTPServer
                            Detail.postDateAndTimeBody.ntp_update_interval = Int(Detail.NTPTimeServer[2].value!)
                            Detail.postDateAndTimeBody.auto_daylight_save = (self.DaylightSaving == true ? 1:0)
                            Detail.getDateAndTimeResult = nil
                            Detail.postDateAndTime()
//                            self.sysReloadflag = true
//                            self.sysReloadTime = 0.005
//                            self.timer = Timer.publish(every: 1.0, on: .main, in: .common).autoconnect()
                            refreshDateandTime = true
                        }

                    } label: {
                        ZStack{
                            RoundedRectangle(cornerRadius: 0)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                        HStack {
                            Image("save")
                            
                            Text("Save")
                               // .frame(minWidth: 0, maxWidth: 70, minHeight: 0, maxHeight: 40,alignment: .leading)
                                .font(.InterRegular18)
                                .foregroundColor(.white)
                                .background(Color.c0x005BC8)
                         }
                        .frame(alignment:.center)
                        }
                    }.frame(width: width/3 , height: 50)
                        .background(Color.c0x005BC8)
                        .buttonStyle(PlainButtonStyle())
                        .onReceive(Just(sysReload)) { Result in
                            NSLog("sysReload \(Result) \(Detail.postsysReloadResult)")
                            if Detail.postsysReloadResult?.error_code == 0
                            {
                                NSLog("sysReload : \(Detail.postsysReloadResult?.data?.reload_time)")
                                let delay = Double((Detail.postsysReloadResult?.data?.reload_time)!)!
                                if sysReloadTimeCout == false
                                {
                                    self.sysReloadTime = Int(delay)
                                    sysReloadTimeCout = true
                                }
                                DispatchQueue.main.asyncAfter(deadline: .now() + delay ) {
                                   if self.save == false && Detail.postsysReloadResult != nil {
                                            self.save = true
                                            NSLog("sysReload finish")
                                            sysReload = 0
                                            self.editFlag = false
                                            self.blurAmount = 0.0
                                            self.showWaitRadio = false
                                            self.showWaitRequest = false
                                            Detail.postsysReloadResult = nil
                                    }
                                    else if self.save == false && self.funcPressed == 4 {
                                        self.save = true
                                        NSLog("sysReload finish")
                                        sysReload = 0
                                        self.editFlag = false
                                        self.blurAmount = 0.0
                                        self.showWaitRadio = false
                                        self.showWaitRequest = false
                                    }
                                }
                                DispatchQueue.main.asyncAfter(deadline: .now() + delay - 3.0 ) {
                                    if self.funcPressed == 4 && refreshDateandTime == true
                                    {
                                        NSLog("sysReload refresh")
                                        Detail.getDateAndTime()
                                        refreshDateandTime = false
                                    }
                                    if self.funcPressed == 0 && Detail.system[1].value != backPress.value1
                                    {
                                         Detail.system[1].value = backPress.value1
                                    }
//                                    else if self.funcPressed == 1 && refreshDateandTime == true
//                                    {
//                                        NSLog("sysReload refresh")
//                                        Detail.getDateAndTime()
//                                        refreshDateandTime = false
//                                    }
                                }
                              }
                              else
                              {
                                  DispatchQueue.main.asyncAfter(deadline: .now() + 1.0 ) {
                                      sysReload += 1
                                  }
                                  
                                  if funcPressed == 2 && self.wifiApply && Detail.postfWiFiResult == self.enableSSID
                                  {
                                      NSLog("POST WIFI FINISH : \(Detail.postfWiFiResult) \(self.enableSSID)")
                                      Detail.postsysReload()
                                      self.blurAmount = 2.0
                                      wifiApply = false
                                      self.showWaitRadio = true
                                      
                                  }
                              }
                           }
                    
                    Button{
                        
                    } label: {
                        ZStack{
                            RoundedRectangle(cornerRadius: 0)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                            HStack {
                        Image("revReboot")
                        
                        Text("Reboot")
                           // .frame(minWidth: 0, maxWidth: 70, minHeight: 0, maxHeight: 40,alignment: .leading)
                            .font(.InterRegular18)
                            .foregroundColor(Color.c0x242527_30)
                            }
                            .frame(alignment:.center)
                        }
                    }.frame(width: width/3 , height: 50)
                        .buttonStyle(PlainButtonStyle())
   
                    if self.disableRadio
                    {
                        Button{
                          //  showRadioAlert = true
                           // blurAmount = 2
                        } label: {
                            ZStack{
                                RoundedRectangle(cornerRadius: 0)
                                        .stroke(Color.c0x696A6B, lineWidth: 1)
                                HStack {
                            Image("revDisable")
                            
                            Text("Enable")
                             //   .frame(minWidth: 0, maxWidth: 70, minHeight: 0, maxHeight: 40,alignment: .leading)
                                .font(.InterRegular18)
                                .foregroundColor(Color.c0x242527_30)
                                }
                                .frame(alignment:.center)
                            }
                        }.frame(width: width/3 , height: 50)
                            .buttonStyle(PlainButtonStyle())
                    }
                    else
                    {
                        Button{
                           // showRadioAlert = true
                        } label: {
                            ZStack{
                                RoundedRectangle(cornerRadius: 0)
                                        .stroke(Color.c0x696A6B, lineWidth: 1)
                                HStack {
                            Image("reradioEnable")
                            
                            Text("Disable")
                              //  .frame(minWidth: 0, maxWidth: 70, minHeight: 0, maxHeight: 40,alignment: .leading)
                                .font(.InterRegular18)
                                .foregroundColor(Color.c0x242527_30)
                                }
                                .frame(alignment:.center)
                            }
                        }.frame(width: width/3 , height: 50)
                            .buttonStyle(PlainButtonStyle())
                    }
                   
                }//HStack
                .frame(width: width , height: 50)
                .padding(.top,-10)
                .disabled(self.showWaitRequest)
            }
        }
    }
    @ViewBuilder
    func showFunc(width:CGFloat)->some View {
        
        ScrollView(.horizontal) {
           // HStack{
            VStack {
            HStack(spacing:0) {
                Button(action: {
                   // withAnimation(.easeInOut(duration: 0.1)) {
                        if !self.editFlag && !self.showWaitRequest{
                            self.diableFlag = false
                            Detail.getSystem()
                            if  self.funcPressed != 0
                            {
                                self.funcPressed = 0
                            }
                            self.showWaitRequest = true
                            self.blurAmount = 2
                         
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                self.blurAmount = 0.0
                                self.showWaitRequest = false
                            }
                            
                        }
                        lastHoveredId = ""
                   // }
                }, label: {
                    
                    if self.funcPressed == 0 {
                        VStack(spacing:2) {
                            Text("System")
                                .frame(height: 35)
                                .font(.InterSemiBold14)
                                .foregroundColor(Color.c0x242527)
                                .overlay(
                                    Rectangle().frame(height: 2).offset(y: -6).foregroundColor(Color.c0x005BC8)
                                              , alignment: .bottom)

                                
                        }
                        
                    }
                    else {
                        VStack(spacing:2) {
                        Text("System")
                            .frame(height: 35)
                            .font(.InterRegular14)
                            .foregroundColor(editFlag ? Color.c0x242527_30:Color.c0x242527)
                            .overlay(
                                Rectangle().frame(height: 2).offset(y: 4).foregroundColor(Color.clear)
                                          , alignment: .bottom)
                            
                        }
                        
                    }
                })
                
                //.padding(.leading,10)
                .onReceive(Detail.$response) { result in
                    if result == 500
                    {
                        showErr = true
                        showDialog(tag: "", width: width, height: width * 2, x: width / 2 , y: width, Content: "Invalid Username/Password")
                    }
                    else if result != 0 && result != 200
                    {
                        showErr = true
                        showDialog(tag: "", width: width, height: width * 2, x: width / 2 , y: width, Content: "The request timed out")
                    }
                }
                .padding(.leading,20)
                .accessibilityIdentifier("system")
                
               // .background(Color.red)
                
                Button(action: {
                   // withAnimation(.easeInOut(duration: 1.0)) {
                        if !self.editFlag && !self.showWaitRequest{
                            self.diableFlag = false
                            Detail.getLANPort()
                           
                            if  self.funcPressed != 1
                            {
                                self.funcPressed = 1
                            }
                            
                            self.showWaitRequest = true
                            self.blurAmount = 2
                           // self.sysReloadflag = false
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                self.blurAmount = 0.0
                                self.showWaitRequest = false
                            }
                        }
                        
                        lastHoveredId = ""
                   // }
                }, label: {
                   
                    if self.funcPressed == 1 {
                        VStack(spacing:2) {
                           Text("LAN Port")
                                .frame(height: 35)
                                .font(.InterSemiBold14)
                                .foregroundColor(Color.c0x242527)
                                .overlay(
                                    Rectangle().frame(height: 2).offset(y: -6).foregroundColor(Color.c0x005BC8)
                                              , alignment: .bottom)

                                
                        }
                        
                    }
                    else {
                        VStack(spacing:2) {
                        Text("LAN Port")
                            .frame(height: 35)
                            .font(.InterRegular14)
                            .foregroundColor(editFlag ? Color.c0x242527_30:Color.c0x242527)
                            .overlay(
                                Rectangle().frame(height: 2).offset(y: 4).foregroundColor(Color.clear)
                                          , alignment: .bottom)
                            
                        }
                        
                    }
                        
                })
                    .padding(.leading,20)
                 .accessibilityIdentifier("lanport")
                 
                 //.background(Color.green)
        
        //.frame(width : 160,height: 50,alignment: .center)
                
                    Button(action: {
                       // withAnimation(.easeInOut(duration: 1.0)) {
                            if !self.editFlag && !self.showWaitRequest{
                                self.diableFlag = false
                                Detail.getSSIDs(model:detail.model!)
                               
                                if  self.funcPressed != 2
                                {
                                    self.funcPressed = 2
                                }
                               
                                self.showWaitRequest = true
                                self.blurAmount = 2
                               // self.sysReloadflag = false
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    self.blurAmount = 0.0
                                    self.showWaitRequest = false
                                }
                            }
                            
                            lastHoveredId = ""
                       // }
                    }, label: {
                       
                        if self.funcPressed == 2 {
                            VStack(spacing:2) {
                               Text("Wireless Bands")
                                    .frame(height: 35)
                                    .font(.InterSemiBold14)
                                    .foregroundColor(Color.c0x242527)
                                    .lineLimit(1)
                                    .overlay(
                                        Rectangle().frame(height: 2).offset(y: -6).foregroundColor(Color.c0x005BC8)
                                                  , alignment: .bottom)

                                    
                            }
                            
                        }
                        else {
                            VStack(spacing:2) {
                            Text("Wireless Bands")
                                .frame(height: 35)
                                .font(.InterRegular14)
                                .foregroundColor(editFlag ? Color.c0x242527_30:Color.c0x242527)
                                .lineLimit(1)
                                .overlay(
                                    Rectangle().frame(height: 2).offset(y: 4).foregroundColor(Color.clear)
                                              , alignment: .bottom)
                                
                            }
                            
                        }
                            
                    })
                    .padding(.leading,20)
                    .accessibility(addTraits: .isButton)
                    .accessibilityIdentifier("Funcbands")

            Button(action: {
              //  withAnimation(.easeInOut(duration: 1.0)) {
                    if !self.editFlag && !self.showWaitRequest{
                        self.diableFlag = true
                        self.blurAmount = 2
                        Detail.getClientsCount = 0
                        Detail.wirelessClients.removeAll(keepingCapacity: true)
                        Detail.mobilekick.removeAll(keepingCapacity: true)
                        Detail.mobileIPSSID.removeAll(keepingCapacity: true)
                        DispatchQueue.main.async {
                        Detail.getWirelessClients(GHz: "2")
                        Detail.getWirelessClients(GHz: "5")
                        Detail.getWirelessClients(GHz: "6")
                        }
                        if self.funcPressed != 3
                        {
                            self.funcPressed = 3
                        }
                        self.showwirelessClients = true
                        
                       
                    }
                    lastHoveredId = ""
             //   }
            }, label: {
                if self.funcPressed == 3 {
                    VStack(spacing:2) {
                       Text("Wireless Clients")
                            .frame(height: 35)
                            .font(.InterSemiBold14)
                            .foregroundColor(Color.c0x242527)
                            .lineLimit(1)
                            .overlay(
                                Rectangle().frame(height: 2).offset(y: -6).foregroundColor(Color.c0x005BC8)
                                          , alignment: .bottom)

                    }
                    
                }
                else {
                    VStack(spacing:2) {
                    Text("Wireless Clients")
                        .frame(height: 35)
                        .font(.InterRegular14)
                        .foregroundColor(editFlag ? Color.c0x242527_30:Color.c0x242527)
                        .lineLimit(1)
                        .overlay(
                            Rectangle().frame(height: 2).offset(y: 4).foregroundColor(Color.clear)
                                      , alignment: .bottom)
                  
                    }
                   
                }
                    
            })
                    .padding(.leading,20)
                    .accessibility(addTraits: .isButton)
             .accessibilityIdentifier("Funcclients")
            
            
            Button(action: {
               // withAnimation(.easeInOut(duration: 1.0)) {
                if !self.editFlag && !self.showWaitRequest {
                    self.diableFlag = false
                    Detail.getDateAndTime()
                    if  self.funcPressed != 4
                    {
                        self.funcPressed = 4
                    }
                 
                    self.requestDate = true
                    self.blurAmount = 2

                }
                lastHoveredId = ""
              //  }
            }, label: {
                if self.funcPressed == 4 {
                    VStack(spacing:2) {
                       Text("Date And Time")
                            .frame(height: 35)
                            .font(.InterSemiBold14)
                            .foregroundColor(Color.c0x242527)
                            .lineLimit(1)
                            .overlay(
                                Rectangle().frame(height: 2).offset(y: -6).foregroundColor(Color.c0x005BC8)
                                          , alignment: .bottom)

                    }
                    
                }
                else {
                    VStack(spacing:2) {
                    Text("Date And Time")
                        .frame(height: 35)
                        .font(.InterRegular14)
                        .foregroundColor(editFlag ? Color.c0x242527_30:Color.c0x242527)
                        .lineLimit(1)
                        .overlay(
                            Rectangle().frame(height: 2).offset(y: 4).foregroundColor(Color.clear)
                                      , alignment: .bottom)
                  
                    }
                    
                }
                    
            })
                    .padding(.leading,20)
             .accessibilityIdentifier("dateandtime")
            
            Button(action: {
                if  !self.editFlag && !self.showWaitRequest{
                    self.diableFlag = true
                    if  self.funcPressed != 5
                    {
                        self.funcPressed = 5
                    }
                }
                lastHoveredId = ""
            }, label: {
                if self.funcPressed == 5 {
                    VStack(spacing:2) {
                       Text("Ping Test")
                            .frame(height: 35)
                            .font(.InterSemiBold14)
                            .foregroundColor(Color.c0x242527)
                            .lineLimit(1)
                            .overlay(
                                Rectangle().frame(height: 2).offset(y: -6).foregroundColor(Color.c0x005BC8)
                                          , alignment: .bottom)
                           
                    }
                    
                }
                else {
                    VStack(spacing:2) {
                    Text("Ping Test")
                        .frame(height: 35)
                        .font(.InterRegular14)
                        .foregroundColor(editFlag ? Color.c0x242527_30:Color.c0x242527)
                        .lineLimit(1)
                        .overlay(
                            Rectangle().frame(height: 2).offset(y: 4).foregroundColor(Color.clear)
                                      , alignment: .bottom)
                    
                    }
                   
                }
                    
            })
                    .padding(.leading,20)
             .accessibilityIdentifier("pingtest")
            
            Button(action: {
                if !self.editFlag && !self.showWaitRequest{
                    self.diableFlag = true
                    if  self.funcPressed != 6
                    {
                        self.funcPressed = 6
                    }
                }
                lastHoveredId = ""
            }, label: {
                if self.funcPressed == 6 {
                    VStack(spacing:2) {
                       Text("Traceroute Test")
                            .frame(height: 35)
                            .font(.InterSemiBold14)
                            .foregroundColor(Color.c0x242527)
                            .lineLimit(1)
                            .overlay(
                                Rectangle().frame(height: 2).offset(y: -6).foregroundColor(Color.c0x005BC8)
                                          , alignment: .bottom)

                    }
                    
                }
                else {
                    VStack(spacing:2) {
                    Text("Traceroute Test")
                        .frame(height: 35)
                        .font(.InterRegular14)
                        .foregroundColor(editFlag ? Color.c0x242527_30:Color.c0x242527)
                        .lineLimit(1)
                        .overlay(
                            Rectangle().frame(height: 2).offset(y: 4).foregroundColor(Color.clear)
                                      , alignment: .bottom)
                   
                    }
                    
                }
                    
            })
                    .padding(.leading,20)
                    .frame(width:150)
             .accessibilityIdentifier("traceroutetest")
            
        }
        .foregroundColor(Color.black.opacity(0.7))
        
        .frame(height: 35)
        //.background(Color.red)
        //.frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .center)
            //    }//HStack
            }.padding(.top,5) //VStack
                .frame(height: 48)
             //   .background(Color.blue)
            
        }
 //       .padding([.top,.bottom],1)
//        .frame(height:50)
        .opacity(self.APnotFound ? 0.3:1.0)
        .disabled(self.APnotFound)
        .accessibilityIdentifier("Main")
        
        
    }
    @ViewBuilder
    func System(width:CGFloat)->some View {
        if funcPressed == 0 {
       // if editFlag {
            Text("System Information")
                    .frame(height: 30)
                    .font(.InterRegular18)
                    .foregroundColor(Color.c0x242527)
                    .padding(.leading,20)
                    
            
            GridStack(rows: 13, columns: 1) { row, col in
                if row == 0 {
                    Text("\(Detail.system[row].name!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-50, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .topLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                   
                    Text("\(Detail.system[row].value!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-50, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .topRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                }
                else if row == 1
                {
                    Text("\(Detail.system[row].name!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-50, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .bottomLeft)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                    ZStack{
                       // Text("\(Detail.system[row].value!)")
                        MarqueeSystemText(Detail: Detail, width: width/2-50)
                        .font(.InterMedium12)
                        .frame(width: width/2-50, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .bottomRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                       if self.editFlag {
                            TextField("\(Detail.system[row].value!)",text:$value1, onEditingChanged: { (editingChanged) in
                                if editingChanged {
                                    
                                    self.lastHoveredId = Detail.lasttag[row]
                                }
                            })
                            .frame(width: width/2-50, height: 35, alignment:.leading)
                            .background(Color.white)
                            .font(.InterMedium12)
                            .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[row], width: width/2-50, height: 35))
                            .onAppear {
                                value1 = "\(Detail.system[row].value!)"
                            }
                            .accessibilityIdentifier("prodname")
//                            .onReceive(Just(Detail.system[row].value)) { _ in
//                                let isAlphanumerics = Detail.system[row].value!.rangeOfCharacter(from: nonAlphanumericCharacters) == nil
//                                if isAlphanumerics
//                                {
//                                    self.SSIDAlert = false
//                                }
//                                else
//                                {
//                                    self.SSIDAlert = true
//                                }
//                            }
                        }
                    }//ZStack
                }
                else
                {
                    Text("\(Detail.system[row].name!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-50, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .bottomLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                   if row == 3
                   {
//                       if #available(iOS 15.0, *) {
//                           TimelineView(.periodic(from: .now, by: 1.0)) { timeline in
//
//                               Text(timeline.date,formatter: stackDateFormat)
//                                   .font(.InterMedium12)
//                                   .frame(width: width/2-50, height: 40, alignment:.leading)
//                                   .padding(.horizontal, 15)
//                                   .foregroundColor(Color.c0x242527)
//                                   .overlay(
//                                    RoundedCorner(radius: 3,corners: .bottomRight)
//                                        .stroke(Color.c0x696A6B, lineWidth: 1)
//                                   )
//                           }
//                       } else {
                           // Fallback on earlier versions
                          
                           Text("\(Detail.system[3].value!)")
                               .font(.InterMedium12)
                               .frame(width: width/2-50, height: 40, alignment:.leading)
                               .padding(.horizontal, 15)
                               .foregroundColor(Color.c0x242527)
                               .overlay(
                                RoundedCorner(radius: 3,corners: .bottomRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                               .onReceive(Just(Detail.system[3].value)) { _ in
                                   if upadteSystime == false
                                   {
                                       upadteSystime = true
                                       DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                           Detail.system[3].value = systemTimeString(inputDate: Detail.system[3].value!)
                                           upadteSystime = false
                                       }
                                   }
                               }
                       //}

                   }
                   else if row == 2
                   {
                       
                       Text("\(Detail.system[2].value!)")
                           .font(.InterMedium12)
                           .frame(width: width/2-50, height: 40, alignment:.leading)
                           .padding(.horizontal, 15)
                           .foregroundColor(Color.c0x242527)
                           .overlay(
                               RoundedCorner(radius: 3,corners: .bottomRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                            )
                           .onReceive(Just(Detail.system[2].value)) { result in
                                if updateUptime == false {
                                   updateUptime = true
                                   DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                      Detail.system[2].value = uptimeString(inputDate: Detail.system[2].value!)
                                      updateUptime = false
                                   }
                               }
                           }
                   }
                   else if row == 9
                   {
                       MarqueeSystemDNSText(Detail: Detail, width: width/2-50)
                           .font(.InterMedium12)
                           .frame(width: width/2-50, height: 40, alignment:.leading)
                           .padding(.horizontal, 15)
                           .foregroundColor(Color.c0x242527)
                           .overlay(
                               RoundedCorner(radius: 3,corners: .bottomRight)
                                   .stroke(Color.c0x696A6B, lineWidth: 1)
                            )
                   }
                    else if row == 11
                    {
                        MarqueeSystemIPV6Text(Detail: Detail, width: width/2-50)
                            .font(.InterMedium12)
                            .frame(width: width/2-50, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                    }
                    else if row == 12
                    {
                        MarqueeSystemIPV6LinkText(Detail: Detail, width: width/2-50)
                            .font(.InterMedium12)
                            .frame(width: width/2-50, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                    }
                   else {
                       
                    Text("\(Detail.system[row].value!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-50, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .bottomRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                    }
                }//else 
             }
             .padding(.leading,20)
             .padding(.top,5)
        }

    }
    @ViewBuilder
    func LAN(width:CGFloat)->some View {
        if funcPressed == 1 {
        Text("LAN-side IP Address")
                .frame(height: 30)
                .font(.InterRegular18)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
        
        HStack {
            Text("IP Address Assignment")
                .frame(width:width*2/3,height: 30,alignment: .leading)
                .font(.InterRegular14)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
         
          //  GeometryReader { geometry in
            Button(action: {
                self.showLANIPType = true
               // posBandsTaggle = geometry.frame(in: .global).origin
            }) {
                HStack(spacing:15) {
                    Text(self.LANIPType)
                        .frame(width:width*1/5, height: 25, alignment: .trailing)
                        .padding(.leading,5)
                        .padding(.trailing ,10)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .gesture(drag)
                        .background(Color.white)
                    if editFlag {
                        
                        Image("down")
                            .resizable()
                            .frame(width: 10, height: 6)
                            .rotationEffect(showLANIPType ?  .degrees(180):.degrees(0))
                            .disabled(true)
                            .background(Color.clear)
                            .padding(.trailing ,10)
                        
                    }
                }
                .gesture(drag)
            
            }
            .onTapGesture {
                self.showLANIPType = true
            }
            .frame(width:width*1/3, height: 40, alignment: .trailing)
            .disabled(!self.editFlag)
            .accessibilityIdentifier("laniptype")
           // }
            
        }
        .padding(.top,5)
        .onReceive(Detail.$getLANResult) { result in
            NSLog("getLANResult : \(result)")
            if result != nil
            { //FromDHCPArray = ["User-Defined","From DHCP"]
                self.LANIPType =  self.LANIPTypeArray[(result?.data?.ipv4?.ipv4_type)!]
                self.GateWayFromDHCP = self.FromDHCPArray[(result?.data?.ipv4?.ipv4_default_gateway_type)!]
                self.PriAddrFromDHCP = self.FromDHCPArray[(result?.data?.ipv4?.ipv4_dns_primary_type)!]
                self.SecAddrFromDHCP = self.FromDHCPArray[(result?.data?.ipv4?.ipv4_dns_secondary_type)!]
                self.LANIPTypeTaggle = (result?.data?.ipv4?.dhcp_enabled == 1 ? true:false)
            }
        }
        
        if self.editFlag {
            GridStack(rows: Detail.LANIPAddress.count, columns: 1) { row, col in
                if row == 0
                {
                    if self.LANIPType == "DHCP"
                    {
                        Text("\(Detail.LANIPAddress[row].name!)")
                              .font(.InterMedium12)
                              .frame(width: width/2-30, height: 40, alignment:.leading)
                              .padding(.horizontal, 15)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                     

                     
                      Text("\(Detail.LANIPAddress[row].value!)")
                          .font(.InterMedium12)
                          .frame(width: width/2-30, height: 40, alignment:.leading)
                          .padding(.horizontal, 15)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius: 3,corners: .topRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                    }
                    else
                    {
                        Text("\(Detail.LANIPAddress[row].name!)")
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                                  .font(.InterMedium12)
                                  .foregroundColor(Color.c0x242527)
                                  .overlay(
                                      RoundedCorner(radius: 3,corners: .topLeft)
                                        .stroke(Color.c0x696A6B, lineWidth: 1)
                                   )
                        
                        ZStack{
                            TextField("\(Detail.LANIPAddress[row].value!)",text:$Detail.LANIPAddress[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                if editingChanged {
                                    self.lastHoveredId = Detail.lasttag[row]
                                  
                                }
                                else
                                {
                                    let result = calculatePressed(subnetMask: Detail.LANIPAddress[1].value!, IPAddress: Detail.LANIPAddress[0].value!)
                                    Detail.LANDHCPServer[0].value = result[0]
                                    Detail.LANDHCPServer[1].value = result[1]
                                    
                                }
                            })
                                .frame(width: width/2-50, height: 35, alignment:.leading)
                                .background(Color.white)
                                .font(.InterMedium12)
                                .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[row], width: width/2-50, height: 35))
                                .keyboardType(.decimalPad)
                                .accessibilityIdentifier("ipaddress")
                               
                                
                        }.frame(width: width/2, height: 40, alignment:.center)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .topRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                    }
                }
                else if row == Detail.LANIPAddress.count-1
                {
                    if self.LANIPType == "DHCP"
                    {
                        Text("\(Detail.LANIPAddress[row].name!)")
                            .frame(width: width/2-30, height: 80, alignment:.leading)
                            .padding(.horizontal, 15)
                                  .font(.InterMedium12)
                                  .foregroundColor(Color.c0x242527)
                                  .overlay(
                                      RoundedCorner(radius: 3,corners: .bottomLeft)
                                     .stroke(Color.c0x696A6B, lineWidth: 1)
                                   )
                       
                            ZStack(alignment:.top){
                                VStack(spacing:0) {
                                HStack(spacing:0) {
                                    Text(self.GateWayFromDHCP)
                                        .frame(width:width*1/3 + 20, height: 35, alignment: .leading)
                                        .padding(.leading,0)
                                        .font(.InterMedium12)
                                        .foregroundColor(Color.c0x242527)
                                        .gesture(drag)

                                    Image("down")
                                        .resizable()
                                        .frame(width: 10, height: 6)
                                        .rotationEffect(GateWayFromDHCPFlag ?  .degrees(180):.degrees(0))
                                        .disabled(true)
                                        .background(Color.clear)
                                        .padding(.trailing ,0)
                                }
                                .gesture(drag)
                                .onTapGesture {
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                    self.GateWayFromDHCPFlag = true
                                    }
                                }
                                .accessibility(addTraits: .isButton)
                                .accessibilityIdentifier("GateWayflag")
                                if self.GateWayFromDHCP == self.FromDHCPArray[0]{
                                    TextField("\(Detail.LANIPAddress[row].value!)",text:$Detail.LANIPAddress[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                        if editingChanged {
                                            
                                            self.lastHoveredId = Detail.lasttag[row]
                                        }
                                    })
                                        .frame(width: width/2-50, height: 35, alignment:.leading)
                                        .background(Color.white)
                                        .font(.InterMedium12)
                                        .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[row], width: width/2-50, height: 35))
                                        .keyboardType(.decimalPad)
                                }
                                else
                                {
                                    Text("\(Detail.LANIPAddress[row].value!)")
                                        .frame(width: width/2-50, height: 35, alignment:.leading)
                                        .background(Color.white)
                                        .foregroundColor(.black)
                                        .font(.InterMedium12)
                                        .padding(.leading,-20)
                                }
                                
                           }
                                
                        }.frame(width: width/2, height: 80, alignment:.center)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                    }
                    else
                    {
                        if self.LANIPType == "DHCP"
                        {
                            Text("\(Detail.LANIPAddress[row].name!)")
                                  .font(.InterMedium12)
                                  .frame(width: width/2-30, height: 40, alignment:.leading)
                                  .padding(.horizontal, 15)
                                  .foregroundColor(Color.c0x242527)
                                  .overlay(
                                      RoundedCorner(radius: 0,corners: .bottomLeft)
                                        .stroke(Color.c0x696A6B, lineWidth: 1)
                                   )
                         

                         
                          Text("\(Detail.LANIPAddress[row].value!)")
                              .font(.InterMedium12)
                              .frame(width: width/2-30, height: 40, alignment:.leading)
                              .padding(.horizontal, 15)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 0,corners: .bottomRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                        }
                        else
                        {
                            Text("\(Detail.LANIPAddress[row].name!)")
                                .frame(width: width/2-30, height: 40, alignment:.leading)
                                .padding(.horizontal, 15)
                                      .font(.InterMedium12)
                                      .foregroundColor(Color.c0x242527)
                                      .overlay(
                                          RoundedCorner(radius: 3,corners: .bottomLeft)
                                            .stroke(Color.c0x696A6B, lineWidth: 1)
                                       )
                            
                            ZStack{
                                TextField("\(Detail.LANIPAddress[row].value!)",text:$Detail.LANIPAddress[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                    if editingChanged {
                                        
                                        self.lastHoveredId = Detail.lasttag[row]
                                      
                                    }
                                })
                                    .frame(width: width/2-50, height: 35, alignment:.leading)
                                    .background(Color.white)
                                    .font(.InterMedium12)
                                    .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[row], width: width/2-50, height: 35))
                                    .keyboardType(.decimalPad)
                                    .accessibilityIdentifier("GateWayTypetext")
                                
                            }.frame(width: width/2, height: 40, alignment:.center)
                                .overlay(
                                    RoundedCorner(radius: 3,corners: .bottomRight)
                                        .stroke(Color.c0x696A6B, lineWidth: 1)
                                 )
                        }
                        
                    }
                   
                }
                else
                {
                    if self.LANIPType == "DHCP"
                    {
                        Text("\(Detail.LANIPAddress[row].name!)")
                              .font(.InterMedium12)
                              .frame(width: width/2-30, height: 40, alignment:.leading)
                              .padding(.horizontal, 15)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 0,corners: .bottomLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                     

                     
                      Text("\(Detail.LANIPAddress[row].value!)")
                          .font(.InterMedium12)
                          .frame(width: width/2-30, height: 40, alignment:.leading)
                          .padding(.horizontal, 15)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius: 0,corners: .bottomRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                    }
                    else
                    {
                        Text("\(Detail.LANIPAddress[row].name!)")
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                                  .font(.InterMedium12)
                                  .foregroundColor(Color.c0x242527)
                                  .overlay(
                                      RoundedCorner(radius: 0,corners: .bottomRight)
                                        .stroke(Color.c0x696A6B, lineWidth: 1)
                                   )
                        
                        ZStack{
                            TextField("\(Detail.LANIPAddress[row].value!)",text:$Detail.LANIPAddress[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                if editingChanged {
                                    
                                    self.lastHoveredId = Detail.lasttag[row]
                                    
                                }
                                else
                                {
                                    let result = calculatePressed(subnetMask: Detail.LANIPAddress[1].value!, IPAddress: Detail.LANIPAddress[0].value!)
                                    Detail.LANDHCPServer[0].value = result[0]
                                    Detail.LANDHCPServer[1].value = result[1]
                                   
                                }
                            })
                                .frame(width: width/2-50, height: 35, alignment:.leading)
                                .background(Color.white)
                                .font(.InterMedium12)
                                .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[row], width: width/2-50, height: 35))
                                .keyboardType(.decimalPad)
                                .accessibilityIdentifier("SubnetTypetext")
                                
                        }.frame(width: width/2, height: 40, alignment:.center)
                            .overlay(
                                RoundedCorner(radius: 0,corners: .bottomRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                    }
                    
                }
                            
             }
            .frame(width:width,alignment: .center)
            .padding(.leading,20)
            .padding(.top,10)
            
           
        }
        else
        {
            GridStack(rows: Detail.LANIPAddress.count, columns: 1) { row, col in
                if row == 0
                {
                    Text("\(Detail.LANIPAddress[row].name!)")
                          .font(.InterMedium12)
                          .frame(width: width/2-30, height: 40, alignment:.leading)
                          .padding(.horizontal, 15)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius: 3,corners: .topLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                 

                 
                  Text("\(Detail.LANIPAddress[row].value!)")
                      .font(.InterMedium12)
                      .frame(width: width/2-30, height: 40, alignment:.leading)
                      .padding(.horizontal, 15)
                      .foregroundColor(Color.c0x242527)
                      .overlay(
                          RoundedCorner(radius: 3,corners: .topRight)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                       )
                }
                else if row == Detail.LANIPAddress.count-1
                {
                    Text("\(Detail.LANIPAddress[row].name!)")
                          .font(.InterMedium12)
                          .frame(width: width/2-30, height: 40, alignment:.leading)
                          .padding(.horizontal, 15)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius: 3,corners: .bottomLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                 

                 
                  Text("\(Detail.LANIPAddress[row].value!)")
                      .font(.InterMedium12)
                      .frame(width: width/2-30, height: 40, alignment:.leading)
                      .padding(.horizontal, 15)
                      .foregroundColor(.black)
                      .overlay(
                          RoundedCorner(radius: 3,corners: .bottomRight)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                       )
                }
                else
                {
                    Text("\(Detail.LANIPAddress[row].name!)")
                          .font(.InterMedium12)
                          .frame(width: width/2-30, height: 40, alignment:.leading)
                          .padding(.horizontal, 15)
                          .foregroundColor(.black)
                          .overlay(
                              RoundedCorner(radius: 0,corners: .bottomLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                 

                 
                  Text("\(Detail.LANIPAddress[row].value!)")
                      .font(.InterMedium12)
                      .frame(width: width/2-30, height: 40, alignment:.leading)
                      .padding(.horizontal, 15)
                      .foregroundColor(.black)
                      .overlay(
                          RoundedCorner(radius: 0,corners: .bottomRight)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                       )
                }
                  
                   
                    }
            .frame(width:width,alignment: .center)
            .padding(.leading,20)
                .padding(.top,10)
        }
        
        Text("LAN-side DNS Servers")
                .frame(height: 30)
                .font(.InterRegular14)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
                .padding(.top,10)
        
        if self.editFlag {
            GridStack(rows: Detail.LANDNSServers.count, columns: 1) { row, col in
                
                if row == 0
                {
                    
                    if self.LANIPType == "DHCP"
                    {
                        Text("\(Detail.LANDNSServers[row].name!)")
                            .frame(width: width/2-30, height: 80, alignment:.leading)
                            .padding(.horizontal, 15)
                                  .font(.InterMedium12)
                                  .foregroundColor(Color.c0x242527)
                                  .overlay(
                                      RoundedCorner(radius: 3,corners: .topLeft)
                                        .stroke(Color.c0x696A6B, lineWidth: 1)
                                   )
                        
                        ZStack(alignment:.top){
                            VStack(spacing:0) {
                            HStack(spacing:0) {
                                Text(self.PriAddrFromDHCP)
                                    .frame(width:width*1/3 + 20, height: 35, alignment: .leading)
                                    .padding(.leading,0)
                                    .font(.InterMedium12)
                                    .foregroundColor(Color.c0x242527)
                                    .gesture(drag)

                                Image("down")
                                    .resizable()
                                    .frame(width: 10, height: 6)
                                    .rotationEffect(PriAddrFromDHCPFlag ?  .degrees(180):.degrees(0))
                                    .disabled(true)
                                    .background(Color.clear)
                                    .padding(.trailing ,0)
                            }
                            .gesture(drag)
                            .onTapGesture {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                self.PriAddrFromDHCPFlag = true
                                }
                            }
                            .accessibility(addTraits: .isButton)
                            .accessibilityIdentifier("PriAddrflag")
                                
                                if self.PriAddrFromDHCP == self.FromDHCPArray[0]{
                                    TextField("\(Detail.LANDNSServers[row].value!)",text:$Detail.LANDNSServers[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                        if editingChanged {
                                            
                                            self.lastHoveredId = Detail.lasttag[row+overText[1]]
                                        }
                                    })
                                        .frame(width: width/2-50, height: 35, alignment:.leading)
                                        .background(Color.white)
                                        .font(.InterMedium12)
                                        .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[row+overText[1]], width: width/2-50, height: 35))
                                        .keyboardType(.decimalPad)
                                        .accessibilityIdentifier("PriAddTypeTextDHCP")
                                }
                                else
                                {
                                    Text("\(Detail.LANDNSServers[row].value!)")
                                        .frame(width: width/2-50, height: 35, alignment:.leading)
                                        .background(Color.white)
                                        .foregroundColor(Color.c0x242527)
                                        .font(.InterMedium12)
                                        .padding(.leading,-20)
                                }
                            
                            
                            }
                                
                        }.frame(width: width/2, height: 80, alignment:.center)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                    }
                    else
                    {
                        Text("\(Detail.LANDNSServers[row].name!)")
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                                  .font(.InterMedium12)
                                  .foregroundColor(Color.c0x242527)
                                  .overlay(
                                      RoundedCorner(radius: 3,corners: .topLeft)
                                        .stroke(Color.c0x696A6B, lineWidth: 1)
                                   )
                        
                        ZStack{
                            TextField("\(Detail.LANDNSServers[row].value!)",text:$Detail.LANDNSServers[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                if editingChanged {
                                    
                                    self.lastHoveredId = Detail.lasttag[row+overText[1]]
                                }
                            })
                                .frame(width: width/2-50, height: 35, alignment:.leading)
                                .background(Color.white)
                                .font(.InterMedium12)
                                .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[row+overText[1]], width: width/2-50, height: 35))
                                .keyboardType(.decimalPad)
                                .accessibilityIdentifier("PriAddTypeText")
                            
                        }.frame(width: width/2, height: 40, alignment:.center)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .topRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                    }
                    
                    
                }
                else if row == Detail.LANDNSServers.count-1
                {
                    if self.LANIPType == "DHCP"
                    {
                        Text("\(Detail.LANDNSServers[row].name!)")
                            .frame(width: width/2-30, height: 80, alignment:.leading)
                            .padding(.horizontal, 15)
                                  .font(.InterMedium12)
                                  .foregroundColor(Color.c0x242527)
                                  .overlay(
                                      RoundedCorner(radius:3 ,corners: .bottomLeft)
                                        .stroke(Color.c0x696A6B, lineWidth: 1)
                                   )
                        
                        ZStack(alignment:.top){
                            VStack(spacing:0) {
                            HStack(spacing:0) {
                                Text(self.SecAddrFromDHCP)
                                    .frame(width:width*1/3 + 20, height: 35, alignment: .leading)
                                    .padding(.leading,0)
                                    .font(.InterMedium12)
                                    .foregroundColor(Color.c0x242527)
                                    .gesture(drag)

                                Image("down")
                                    .resizable()
                                    .frame(width: 10, height: 6)
                                    .rotationEffect(SecAddrFromDHCPFlag ?  .degrees(180):.degrees(0))
                                    .disabled(true)
                                    .background(Color.clear)
                                    .padding(.trailing ,0)
                            }
                            .gesture(drag)
                            .onTapGesture {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                self.SecAddrFromDHCPFlag = true
                                }
                            }
                            .accessibility(addTraits: .isButton)
                            .accessibilityIdentifier("SecAddrflag")
                           
                              
                            if self.SecAddrFromDHCP == self.FromDHCPArray[0]{
                                TextField("\(Detail.LANDNSServers[row].value!)",text:$Detail.LANDNSServers[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                    if editingChanged {
                                        
                                        self.lastHoveredId = Detail.lasttag[row+overText[1]]
                                    }
                                })
                                    .frame(width: width/2-50, height: 35, alignment:.leading)
                                    .background(Color.white)
                                    .font(.InterMedium12)
                                    .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[row+overText[1]], width: width/2-50, height: 35))
                                    .keyboardType(.decimalPad)
                                    .accessibilityIdentifier("SecAddTypeTextDHCP")
                            }
                            else
                            {
                                Text("\(Detail.LANDNSServers[row].value!)")
                                    .frame(width: width/2-50, height: 35, alignment:.leading)
                                    .background(Color.white)
                                    .foregroundColor(Color.c0x242527)
                                    .font(.InterMedium12)
                                    .padding(.leading,-20)
                            }
                                
                            }
                                
                        }.frame(width: width/2, height: 80, alignment:.center)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                    }
                    else
                    {
                    Text("\(Detail.LANDNSServers[row].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius:3 ,corners: .bottomLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    
                    ZStack{
                        TextField("\(Detail.LANDNSServers[row].value!)",text:$Detail.LANDNSServers[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                            if editingChanged {
                                
                                self.lastHoveredId = Detail.lasttag[row+overText[1]]
                            }
                        })
                            .frame(width: width/2-50, height: 35, alignment:.leading)
                            .background(Color.white)
                            .font(.InterMedium12)
                            .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[row+overText[1]], width: width/2-50, height: 35))
                            .keyboardType(.decimalPad)
                            .accessibilityIdentifier("SecAddTypeText")
                        
                    }.frame(width: width/2, height: 40, alignment:.center)
                        .overlay(
                            RoundedCorner(radius:3 ,corners: .bottomRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                    }
                }
                else
                {
                    Text("\(Detail.LANDNSServers[row].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius:0 ,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    
                    ZStack{
                        TextField("\(Detail.LANDNSServers[row].value!)",text:$Detail.LANDNSServers[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                            if editingChanged {
                                
                                self.lastHoveredId = Detail.lasttag[row+overText[1]]
                            }
                        })
                            .frame(width: width/2-50, height: 35, alignment:.leading)
                            .background(Color.white)
                            .font(.InterMedium12)
                            .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[row+overText[1]], width: width/2-50, height: 35))
                            .keyboardType(.decimalPad)
                    }.frame(width: width/2, height: 40, alignment:.center)
                        .overlay(
                            RoundedCorner(radius:0 ,corners: .topLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                    
                }
                
                
                            
             }
            .frame(width:width,alignment: .center)
            .padding(.leading,20)
            .padding(.top,10)
            
           
        }
        else
        {
            GridStack(rows: Detail.LANDNSServers.count, columns: 1) { row, col in
                
                if row == 0
                {
                    Text("\(Detail.LANDNSServers[row].name!)")
                           .font(.InterMedium12)
                           .frame(width: width/2-30, height: 40, alignment:.leading)
                           .padding(.horizontal, 15)
                           .foregroundColor(Color.c0x242527)
                           .overlay(
                               RoundedCorner(radius:3 ,corners: .topLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                            )
                  

                  
                   Text("\(Detail.LANDNSServers[row].value!)")
                       .font(.InterMedium12)
                       .frame(width: width/2-30, height: 40, alignment:.leading)
                       .padding(.horizontal, 15)
                       .foregroundColor(Color.c0x242527)
                       .overlay(
                           RoundedCorner(radius:3 ,corners: .topRight)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                        )
                }
                else if row == Detail.LANDNSServers.count-1
                {
                    Text("\(Detail.LANDNSServers[row].name!)")
                           .font(.InterMedium12)
                           .frame(width: width/2-30, height: 40, alignment:.leading)
                           .padding(.horizontal, 15)
                           .foregroundColor(Color.c0x242527)
                           .overlay(
                               RoundedCorner(radius:3 ,corners: .bottomLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                            )
                  

                  
                   Text("\(Detail.LANDNSServers[row].value!)")
                       .font(.InterMedium12)
                       .frame(width: width/2-30, height: 40, alignment:.leading)
                       .padding(.horizontal, 15)
                       .foregroundColor(Color.c0x242527)
                       .overlay(
                           RoundedCorner(radius:3 ,corners: .bottomRight)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                        )
                }
                else
                {
                    Text("\(Detail.LANDNSServers[row].name!)")
                           .font(.InterMedium12)
                           .frame(width: width/2-30, height: 40, alignment:.leading)
                           .padding(.horizontal, 15)
                           .foregroundColor(Color.c0x242527)
                           .overlay(
                               RoundedCorner(radius:0 ,corners: .bottomRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                            )
                  

                  
                   Text("\(Detail.LANDNSServers[row].value!)")
                       .font(.InterMedium12)
                       .frame(width: width/2-30, height: 40, alignment:.leading)
                       .padding(.horizontal, 15)
                       .foregroundColor(Color.c0x242527)
                       .overlay(
                           RoundedCorner(radius:0 ,corners: .bottomRight)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                        )
                }
                
                
                
                   
             }
            .frame(width:width,alignment: .center)
            .padding(.leading,20)
                .padding(.top,10)
        }
        
        if self.LANIPType != "DHCP" {
        
        Text("DHCP server")
                .frame(height: 30)
                .font(.InterRegular14)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
                .padding(.top,10)
            
        Toggle(isOn: $LANIPTypeTaggle)
        {
            Text("DHCP server")
        }
        .toggleStyle(customDefault1ToggleStyle(edit: $editFlag))
        .frame(width:width,alignment: .center)
        .padding(.leading,20)
        .onTapGesture {
            backPress.LANType[0] = self.LANIPTypeTaggle
        }
        .disabled(!editFlag)
        .padding(.top,5)
        .accessibilityIdentifier("DHCPserver")
        
        if self.editFlag {
            GridStack(rows: Detail.LANDHCPServer.count, columns: 1) { row, col in
                
                if row == 0
                {
                    Text("\(Detail.LANDHCPServer[row].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius:3 ,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    
                    ZStack{
                      
                            TextField("\(Detail.LANDHCPServer[row].value!)",text:$Detail.LANDHCPServer[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                if editingChanged {
                                    
                                    self.lastHoveredId = Detail.lasttag[row+overText[2]]
                                }
                            })
                                    .frame(width: width/2-50, height: 35, alignment:.leading)
                                    .background(Color.white)
                                    .font(.InterMedium12)
                                    .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[row+overText[2]], width: width/2-50, height: 35))
                                    .keyboardType(.decimalPad)
                                    .accessibilityIdentifier("startIP")
                        
                    }.frame(width: width/2, height: 40, alignment:.center)
                        .overlay(
                            RoundedCorner(radius:3 ,corners: .topRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                }
                else if row == Detail.LANDHCPServer.count-1
                {
                    Text("\(Detail.LANDHCPServer[row].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius:3 ,corners: .bottomLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    
                    ZStack{
                     
                            TextField("\(Detail.LANDHCPServer[row].value!)",text:$Detail.LANDHCPServer[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                if editingChanged {
                                    
                                    self.lastHoveredId = Detail.lasttag[row+overText[2]]
                                }
                            })
                                    .frame(width: width/2-50, height: 35, alignment:.leading)
                                    .background(Color.white)
                                    .font(.InterMedium12)
                                    .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[row+overText[2]], width: width/2-50, height: 35))
                                    .keyboardType(.decimalPad)
                                    .accessibilityIdentifier("secIP")
                            
                    }.frame(width: width/2, height: 40, alignment:.center)
                        .overlay(
                            RoundedCorner(radius:3 ,corners: .bottomRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                }
                else
                {
                    Text("\(Detail.LANDHCPServer[row].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius:0 ,corners: .topRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    
                    VStack{
                      //  GeometryReader { geometry in
                        if Detail.LANDHCPServer[row].name == "Lease Time" {
                            HStack(spacing:0){
                            Text("\(Detail.LANDHCPServer[row].value!)")
                                    .frame(width: width/2-50, height: 35, alignment:.leading)
                                    .background(Color.white)
                                    .padding(.leading, 15)
                                    .font(.InterMedium12)
                                    .foregroundColor(Color.c0x242527)
                                    .gesture(drag)
                                
                                Image("down")
                                    .resizable()
                                    .frame(width: 10, height: 6)
                                    .rotationEffect(showLeaseTimeFlag ?  .degrees(180):.degrees(0))
                                    .disabled(true)
                                    .background(Color.clear)
                                    .padding(.trailing ,20)
                            }
                            .gesture(drag)
                            .onTapGesture {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                self.showLeaseTimeFlag = true
                                  //  posBandsTaggle = geometry.frame(in: .global).origin
                                }
                               
                            }
                            .accessibility(addTraits: .isButton)
                            .accessibilityIdentifier("DHCPleasetime")
                        }
                        else
                        {
                            TextField("\(Detail.LANDHCPServer[row].value!)",text:$Detail.LANDHCPServer[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                if editingChanged {
                                    
                                    self.lastHoveredId = Detail.lasttag[row+overText[2]]
                                }
                            })
                                    .frame(width: width/2-50, height: 35, alignment:.leading)
                                    .background(Color.white)
                                    .font(.InterMedium12)
                                    .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[row+overText[2]], width: width/2-50, height: 35))
                                    .keyboardType(Detail.LANDHCPServer[row].name == "Domain Name" ? .default:.decimalPad)
                                    .accessibilityIdentifier("LANDHCPServer\(row)")
                        }
 
                            
                    }.frame(width: width/2, height: 40, alignment:.center)
                        .overlay(
                            RoundedCorner(radius:0 ,corners: .topRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                }
                
                
            
                            
             }
            .frame(width:width,alignment: .center)
            .padding(.leading,20)
            .padding(.top,10)
        }
        else
        {
            GridStack(rows: Detail.LANDHCPServer.count, columns: 1) { row, col in
                if row == 0
                {
                    Text("\(Detail.LANDHCPServer[row].name!)")
                          .font(.InterMedium12)
                          .frame(width: width/2-30, height: 40, alignment:.leading)
                          .padding(.horizontal, 15)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius:3 ,corners: .topLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                 
                  Text("\(Detail.LANDHCPServer[row].value!)")
                      .font(.InterMedium12)
                      .frame(width: width/2-30, height: 40, alignment:.leading)
                      .padding(.horizontal, 15)
                      .foregroundColor(Color.c0x242527)
                      .overlay(
                          RoundedCorner(radius:3 ,corners: .topRight)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                       )
                }
                else if row == Detail.LANDHCPServer.count-1
                {
                    Text("\(Detail.LANDHCPServer[row].name!)")
                          .font(.InterMedium12)
                          .frame(width: width/2-30, height: 40, alignment:.leading)
                          .padding(.horizontal, 15)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius:3 ,corners: .bottomLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                 

                 
                  Text("\(Detail.LANDHCPServer[row].value!)")
                      .font(.InterMedium12)
                      .frame(width: width/2-30, height: 40, alignment:.leading)
                      .padding(.horizontal, 15)
                      .foregroundColor(Color.c0x242527)
                      .overlay(
                          RoundedCorner(radius:3 ,corners: .bottomRight)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                       )
                }
                else
                {
                    Text("\(Detail.LANDHCPServer[row].name!)")
                          .font(.InterMedium12)
                          .frame(width: width/2-30, height: 40, alignment:.leading)
                          .padding(.horizontal, 15)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius:0 ,corners: .topRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                 

                 
                  Text("\(Detail.LANDHCPServer[row].value!)")
                      .font(.InterMedium12)
                      .frame(width: width/2-30, height: 40, alignment:.leading)
                      .padding(.horizontal, 15)
                      .foregroundColor(Color.c0x242527)
                      .overlay(
                          RoundedCorner(radius:0 ,corners: .topRight)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                       )
                }
                   
            }
            .frame(width:width,alignment: .center)
            .padding(.leading,20)
                .padding(.top,10)
        }
        }
        }
    }
    @ViewBuilder
    func WirelessBands(width:CGFloat)->some View { //420E 840E support 6G
        if funcPressed == 2 {
        Text("Wireless Bands")
                .frame(height: 30)
                .font(.InterRegular18)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
         
        Toggle(isOn: $Wireless24G)
        {
            Text("2.4Ghz")
        }
        .toggleStyle(customWiBandsToggleStyle(edit: $editFlag))
        .frame(width:width,alignment: .center)
        .padding(.leading,20)
        .padding(.top,5)
        .onTapGesture {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            backPress.wirelessType[0] = self.Wireless24G
            }
        }
        .disabled(!editFlag)
        .gesture(drag)
        .accessibilityIdentifier("Bands2G")
        
        HStack {
            Text("Enable SSID number")
                .frame(width:width*2/3,height: 25,alignment: .leading)
                .font(.InterRegular14)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
         
            GeometryReader { geometry in
            Button(action: {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.showBandsTaggle24G.toggle()
               // posBandsTaggle = geometry.frame(in: .global).origin
                }
            }) {
                    
                HStack(spacing:0) {
                    Text(self.WiFiBands24G)
                        .frame(width:width*1/10, height: 25, alignment: .trailing)
                        .padding(.trailing ,10)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .gesture(drag)
                    if editFlag {

                        Image("down")
                            .resizable()
                            .frame(width: 10, height: 6)
                            .rotationEffect(showBandsTaggle24G ?  .degrees(180):.degrees(0))
                            .disabled(true)
                            .background(Color.clear)
                            .padding(.trailing ,10)
                    }
                }
                .gesture(drag)
               
            }
            .gesture(drag)
            .frame(width:width*1/3, height: 25, alignment: .trailing)
            .disabled(!self.editFlag)
            }
            .gesture(drag)
            .onReceive(Detail.$get24GBandsResult) { result in
                if result != nil && result != Detail.get24GBandsResult
                {
                    NSLog("get24GSSIDSecurity")
                    self.WiFiBands24G = (result?.data?.enabled_ssidNum)!.description
                }
                
            }
            .accessibility(addTraits: .isButton)
            .accessibilityIdentifier("SSID2GNUM")
        }
        .padding(.top,5)
        .gesture(drag)
        .onReceive(Detail.$get24GSSIDSecurityResult) { result in
//            NSLog("get24GSSIDSecurity : \(result)")
//            if result != nil
//            {
//                if result?.data?.auth_detail?.wpa_type?.description != nil
//                {
//                    NSLog("get24GSSIDSecurity re:\((result?.data?.ssid_index?.description)!) \((result?.data?.auth_detail?.wpa_type?.description)!)")
//                    NSLog("get24GSSIDSecurity re: \((result?.data?.ssid_index?.description)!)    \(Detail.wireless24G[1].value)")
//                }
//            }
            
        }
        //wirelessDetail(detail: Detail.wireless24G,bands:Int(self.WiFiBands24G)!,width: width,flag: self.Wireless24G,overText:overText[0])
        LazyVStack{
        wirelessDetail(detail: Detail.wireless24G,bands:Int(self.WiFiBands24G)!,width: width,flag: true,overText:overText[0])
        }
                
        Toggle(isOn: $Wireless5G)
        {
            Text("5Ghz")
        }
        .toggleStyle(customWiBandsToggleStyle(edit: $editFlag))
        .frame(width:width,alignment: .center)
        .padding(.leading,20)
        .padding(.top,15)
        .onTapGesture {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            backPress.wirelessType[1] = self.Wireless5G
            }
        }
        .disabled(!editFlag)
        .gesture(drag)
        .accessibilityIdentifier("Bands5G")
        
        HStack {
            Text("Enable SSID number")
                .frame(width:width*2/3,height: 25,alignment: .leading)
                .font(.InterRegular14)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
            
            GeometryReader { geometry in
            Button(action: {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.showBandsTaggle5G.toggle()
               // posBandsTaggle = geometry.frame(in: .global).origin
                }
            }) {
                    
                HStack(spacing:5) {
                    Text(self.WiFiBands5G)
                        .frame(width:width*1/10, height: 25, alignment: .trailing)
                        .padding(.trailing ,10)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .gesture(drag)
                    if editFlag {
                        Image("down")
                            .resizable()
                            .frame(width: 10, height: 6)
                            .rotationEffect(showBandsTaggle5G ?  .degrees(180):.degrees(0))
                            .disabled(true)
                            .background(Color.clear)
                            .padding(.trailing ,10)
                    }
                }
                .gesture(drag)
               
            }
            .gesture(drag)
            .frame(width:width*1/3, height: 25, alignment: .trailing)
            .disabled(!self.editFlag)
          }
            .gesture(drag)
            .onReceive(Detail.$get5GBandsResult) { result in
                if result != nil && result != Detail.get5GBandsResult
                {
                    NSLog("get5GSSIDSecurity")
                    self.WiFiBands5G = (result?.data?.enabled_ssidNum)!.description
                }
                
            }
            .accessibility(addTraits: .isButton)
            .accessibilityIdentifier("SSID5GNUM")
        }
        .padding(.top,5)
        .gesture(drag)
        .onReceive(Detail.$get5GSSIDSecurityResult) { result in
            
//            if result != nil && result != Detail.get5GBandsResult
//            {
//                NSLog("get5GSSIDSecurity")
//                self.WiFiBands5G = (result?.data?.enabled_ssidNum)!.description
////                for i in 0..<(result?.data?.enabled_ssidNum)! {
////                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.02) {
////                    Detail.get5GSSIDSecurity(ssid_index:"\(i)")
////                    }
////                }
//            }
            
        }
      //  wirelessDetail5G(detail: Detail.wireless5G,bands:Int(self.WiFiBands5G)!,width: width,flag: self.Wireless5G,overText:overText[1])
        LazyVStack{
        wirelessDetail5G(detail: Detail.wireless5G,bands:Int(self.WiFiBands5G)!,width: width,flag: true,overText:overText[1])
        }
        if support6G.contains(detail.model!)
        {
        Toggle(isOn: $Wireless6G)
        {
            Text("6Ghz")
        }
        .toggleStyle(customWiBandsToggleStyle(edit: $editFlag))
        .frame(width:width,alignment: .center)
        .padding(.leading,20)
        .padding(.top,15)
        .onTapGesture {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            backPress.wirelessType[2] = self.Wireless6G
            }
        }
        .disabled(!self.editFlag)
        .gesture(drag)
        .accessibilityIdentifier("Bands6G")
        
        HStack {
            Text("Enable SSID number")
                .frame(width:width*2/3,height: 25,alignment: .leading)
                .font(.InterRegular14)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
            GeometryReader {geometry in
            Button(action: {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.showBandsTaggle6G.toggle()
                //posBandsTaggle = geometry.frame(in: .global).origin
                }
            }) {
                    
                HStack(spacing:5) {
                    Text(self.WiFiBands6G)
                        .frame(width:width*1/10, height: 25, alignment: .trailing)
                        .padding(.trailing ,10)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .gesture(drag)
                    if editFlag {
                        Image("down")
                            .resizable()
                            .frame(width: 10, height: 6)
                            .rotationEffect(showBandsTaggle6G ?  .degrees(180):.degrees(0))
                            .disabled(true)
                            .background(Color.clear)
                            .padding(.trailing ,10)
                    }
                }
                .gesture(drag)
               
            }
            .gesture(drag)
            .frame(width:width*1/3, height: 25, alignment: .trailing)
            .disabled(!editFlag)
            }
            .gesture(drag)
            .onReceive(Detail.$get6GBandsResult) { result in
                if result != nil && result != Detail.get6GBandsResult
                {
                    NSLog("get6GSSIDSecurity")
                    self.WiFiBands6G = (result?.data?.enabled_ssidNum)!.description
                }
                
            }
            .accessibility(addTraits: .isButton)
            .accessibilityIdentifier("SSID6GNUM")
        }
        .padding(.top,5)
        .gesture(drag)
        .onReceive(Detail.$get5GSSIDSecurityResult) { result in
//            if result != nil && result != Detail.get6GBandsResult
//            {
//                NSLog("get6GSSIDSecurity")
//                self.WiFiBands6G = (result?.data?.enabled_ssidNum)!.description
////                for i in 0..<(result?.data?.enabled_ssidNum)! {
////                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.02) {
////                    Detail.get6GSSIDSecurity(ssid_index:"\(i)")
////                    }
////                }
//            }
            
        }
      //  wirelessDetail6G(detail: Detail.wireless6G,bands:Int(self.WiFiBands6G)!,width: width,flag: self.Wireless6G,overText:overText[2])
        LazyVStack{
        wirelessDetail6G(detail: Detail.wireless6G,bands:Int(self.WiFiBands6G)!,width: width,flag: true,overText:overText[2])
        }
        }
        }
    }
    @ViewBuilder
    func edit24GAuth(width:CGFloat,lable:Int,i:Int)->some View{
        HStack(spacing:0){
        Text(Detail.wireless24G[lable].value!)
            .frame(width: width/2-50, height: 35, alignment:.leading)
            .background(i%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
            .padding(.leading, 15)
            .font(.InterMedium12)
            .foregroundColor(Color.c0x242527)
            .clipped()
            .gesture(drag)
            
            Image("down")
                .resizable()
                .frame(width: 10, height: 6)
                .rotationEffect(showAuthType24GFlag ?  .degrees(180):.degrees(0))
                .disabled(true)
                .background(Color.clear)
                .padding(.trailing ,20)
        }
        .gesture(drag)
        .frame(width:width/2,height:40)
        .overlay(
            RoundedCorner(radius: 0,corners: .topLeft)
                .stroke(Color.c0x696A6B, lineWidth: 1)
         )
        .onTapGesture {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.showAuthType24GFlag = true
                self.AuthROW = lable
                if let idx = AuthArray.firstIndex(where: { $0 == Detail.wireless24G[lable].value! }) {
                    AuthArraySelected = idx
                }
                else
                {
                    AuthArraySelected = 0
                }
            }

        }
        .accessibility(addTraits: .isButton)
        .accessibilityIdentifier("SSID2GAuth\(lable)")
    }
    @ViewBuilder
    func edit24G(width:CGFloat,lable:Int,overText:Int,i:Int)->some View {
        HStack(spacing:0) {
                TextField("\(Detail.wireless24G[lable].value!)",text:$Detail.wireless24G[lable].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                    if editingChanged {
                        self.lastHoveredId = Detail.lasttag[lable + overText]
                    }
                   })
                    .frame(width: width/2-30, height: 35, alignment:.leading)
                    .background(i%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
                    .font(.InterMedium12)
                    .textFieldStyle(MyWiBandsTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[lable + overText], width: width/2-30, height: 35))
                    .keyboardType(.default)
                    .accessibilityIdentifier("psk2G\(lable)")
                    .onReceive(Just(Detail.wireless24G[lable].value)) { _ in
                        if self.lastHoveredId == Detail.lasttag[lable + overText]{
                        if Detail.wireless24G[lable].name == "Pre-shared Key"
                        {
                            if Detail.wireless24G[lable - 1].value == "None" || Detail.wireless24G[lable - 1].value == "WEP" || Detail.wireless24G[lable - 1].value == "OWE"
                            {
                                return
                            }
                            else{
                            if Detail.wireless24G[lable].value!.count >= WPAMin && Detail.wireless24G[lable].value!.count <= WPAMax
                            {
                                let isAlphanumerics = Detail.wireless24G[lable].value!.rangeOfCharacter(from: nonAlphanumericCharacters) == nil
                                if isAlphanumerics
                                {
                                    self.pskAlert = false
                                }
                                else
                                {
                                    self.pskAlert = true
                                }
                            }
                            else
                            {
                                self.pskAlert = true
                            }
                            }
                        }
                        else if  Detail.wireless24G[lable].name == "SSID"
                        {
                            if Detail.wireless24G[lable].value!.count >= SSIDMin && Detail.wireless24G[lable].value!.count <= SSIDMax
                            {
                                self.SSIDAlert = false
                            }
                            else
                            {
                                self.SSIDAlert = true
                            }
                        }
                        }
                    }
        }
        .frame(width:(CGFloat)(width/2),height:40)
        .overlay(
            RoundedCorner(radius: 0,corners: .topLeft)
                .stroke(Color.c0x696A6B, lineWidth: 1)
         )
   
    }
    @ViewBuilder
    func wirelessDetail(detail:[WirelessBody],bands:Int,width:CGFloat,flag:Bool,overText:Int)->some View{
        
        ForEach(0..<bands, id:\.self) { i in
                  
            GridStack(rows: 4, columns: 1) { row, col in
                if editFlag {
                    if row == 0 {
                        Text("\(detail[row+i*4].name!)\(i+1)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    else
                    {
                        Text("\(detail[row+i*4].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    
                    if detail[row+i*4].name == "Authentication Type" {
                        let lable = row + i * 4
                        edit24GAuth(width: width, lable: lable,i:i)
                    }
                    else if detail[row+i*4].name == "Pre-shared Key" && ( Detail.wireless24G[row+i*4 - 1].value == "None" || Detail.wireless24G[row+i*4 - 1].value == "WEP" || Detail.wireless24G[row+i*4 - 1].value == "OWE" )
                    {
                        Text("")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                              .clipped()
                       
                    }
                    else
                    {
                        let lable = row + i * 4
                        edit24G(width: width, lable: lable,overText: overText,i:i)
                    }
                }
                else
                {
                    if row == 0 {
                        Text("\(detail[row+i*4].name!)\(i+1)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    else
                    {
                        Text("\(detail[row+i*4].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    
                    if detail[row+i*4].name == "Authentication Type" || detail[row+i*4].name == "SSID"  {
                        
                        //MarqueeWiBands24GText(text: Detail.wireless24G[row+i*4].value!)
                       // Text("\(Detail.wireless24G[row+i*4].value!)")
                        MarqueeWiBands24GText(Detail: Detail, index: row+i*4, width:width/2-30)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .background(i%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
                            .padding(.horizontal, 15)
                            .font(.InterMedium12)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 0,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)

                         )
//                            .onReceive(Detail.$wireless24G) { re in
//                                NSLog("get24GSSIDSecurityResult ..re: \(Detail.wireless24G[1].value!)")
//                            }
                       
//                        Marquee(i:row+i*4)
                    }
                    else{
                    Text("\(Detail.wireless24G[row+i*4].value!)")
                    .frame(width: width/2-30, height: 40, alignment:.leading)
                    .padding(.horizontal, 15)
                          .font(.InterMedium12)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius: 3,corners: .topLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                          .clipped()
                    }
                }
            }
            .font(.InterMedium12)
            .background(i%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
            .foregroundColor(Color.c0x242527)
            .cornerRadius(3)
            .padding(.top,12)
        }
    }
    @ViewBuilder
    func edit5GAuth(width:CGFloat,lable:Int,i:Int)->some View{
        HStack(spacing:0){
        Text(Detail.wireless5G[lable].value!)
            .frame(width: width/2-50, height: 35, alignment:.leading)
            .background(i%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
            .padding(.leading, 15)
            .font(.InterMedium12)
            .foregroundColor(Color.c0x242527)
            .clipped()
            .gesture(drag)
            
            Image("down")
                .resizable()
                .frame(width: 10, height: 6)
                .rotationEffect(showAuthType5GFlag ?  .degrees(180):.degrees(0))
                .disabled(true)
                .background(Color.clear)
                .padding(.trailing ,20)
        }
        .gesture(drag)
        .frame(width:width/2,height:40)
        .overlay(
            RoundedCorner(radius: 0,corners: .topLeft)
                .stroke(Color.c0x696A6B, lineWidth: 1)
         )
        .onTapGesture {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.showAuthType5GFlag = true
                self.AuthROW = lable
               // print("tap:\(self.AuthROW)")
                if let idx = AuthArray.firstIndex(where: { $0 == Detail.wireless5G[lable].value! }) {
                    AuthArraySelected = idx
                }
                else
                {
                    AuthArraySelected = 0
                }
            }

        }
        .accessibility(addTraits: .isButton)
        .accessibilityIdentifier("SSID5GAuth\(lable)")
    }
    @ViewBuilder
    func edit5G(width:CGFloat,lable:Int,overText:Int,i:Int)->some View {
        HStack(spacing:0) {
           
                TextField("\(Detail.wireless5G[lable].value!)",text:$Detail.wireless5G[lable].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                    if editingChanged {
                        
                        self.lastHoveredId = Detail.lasttag[lable + overText]
                       
                    }
                   })
                .frame(width: CGFloat(width/2)-30, height: 35, alignment:.leading)
                    .background(i%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
                    .font(.InterMedium12)
                    .textFieldStyle(MyWiBandsTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[lable + overText], width: width/2-30, height: 35))
                    .keyboardType(.default)
                    .accessibilityIdentifier("psk5G\(lable)")
                    .onReceive(Just(Detail.wireless5G[lable].value)) { _ in
                        if self.lastHoveredId == Detail.lasttag[lable + overText]{
                        if Detail.wireless5G[lable].name == "Pre-shared Key"
                        {
                            if Detail.wireless5G[lable - 1].value == "None" || Detail.wireless5G[lable - 1].value == "WEP" || Detail.wireless5G[lable - 1].value == "OWE"
                            {
                                return
                            }
                            if Detail.wireless5G[lable].value!.count >= WPAMin && Detail.wireless5G[lable].value!.count <= WPAMax
                            {
                                let isAlphanumerics = Detail.wireless5G[lable].value!.rangeOfCharacter(from: nonAlphanumericCharacters) == nil
                                if isAlphanumerics
                                {
                                    self.pskAlert = false
                                }
                                else
                                {
                                    self.pskAlert = true
                                }
                            }
                            else
                            {
                                self.pskAlert = true
                            }
                        }
                        else if  Detail.wireless5G[lable].name == "SSID"
                        {
                            if Detail.wireless5G[lable].value!.count >= SSIDMin && Detail.wireless5G[lable].value!.count <= SSIDMax
                            {
                                self.SSIDAlert = false
                            }
                            else
                            {
                                self.SSIDAlert = true
                            }
                        }
                        }
                    }
           
        }
        .frame(width:(CGFloat)(width/2),height:40)
        .overlay(
            RoundedCorner(radius: 0,corners: .topLeft)
                .stroke(Color.c0x696A6B, lineWidth: 1)
         )
    }
    @ViewBuilder
    func wirelessDetail5G(detail:[WirelessBody],bands:Int,width:CGFloat,flag:Bool,overText:Int)->some View{
        
        ForEach(0..<bands, id:\.self) { i in
        
            GridStack(rows: 4, columns: 1) { row, col in
                if editFlag {
                    if row == 0 {
                        Text("\(detail[row+i*4].name!)\(i+1)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    else
                    {
                        Text("\(detail[row+i*4].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                           
                    
                    if detail[row+i*4].name == "Authentication Type" {
                        let lable = row+i*4
                        edit5GAuth(width: width, lable: lable,i:i)
                    }
                    else if Detail.wireless5G[row+i*4].name == "Pre-shared Key" && ( Detail.wireless5G[row+i*4 - 1].value == "None" || Detail.wireless5G[row+i*4 - 1].value == "WEP" || Detail.wireless5G[row+i*4 - 1].value == "OWE" )
                    {
                       
                        Text("")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                              .clipped()
                       
                    }
                    else
                    {
                        let lable = row + i * 4
                        edit5G(width:width,lable:lable,overText:overText,i:i)
                    }
                }
                else
                {
                    if row == 0 {
                        Text("\(detail[row+i*4].name!)\(i+1)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    else
                    {
                        Text("\(detail[row+i*4].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    
                    if detail[row+i*4].name == "Authentication Type" || detail[row+i*4].name == "SSID" {
                       
                        MarqueeWiBands5GText(Detail: Detail, index: row+i*4, width:width/2-30)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .background(i%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
                            .padding(.horizontal, 15)
                            .font(.InterMedium12)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 0,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                                   
                         )
                        
                    }
                    else{
                    Text("\(Detail.wireless5G[row+i*4].value!)")
                    .frame(width: width/2-30, height: 40, alignment:.leading)
                    .padding(.horizontal, 15)
                          .font(.InterMedium12)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius: 3,corners: .topLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                    }
                }
            }
            .font(.InterMedium12)
            .foregroundColor(Color.c0x242527)
            .background(i%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
            .cornerRadius(3)
            .padding(.top,12)

        }
    
    }
    @ViewBuilder
    func edit6GAuth(width:CGFloat,lable:Int,i:Int)->some View {
        HStack(spacing:0){
        Text(Detail.wireless6G[lable].value!)
            .frame(width: width/2-50, height: 35, alignment:.leading)
            .background(i%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
            .padding(.leading, 15)
            .font(.InterMedium12)
            .foregroundColor(Color.c0x242527)
            .clipped()
            .gesture(drag)

            Image("down")
                .resizable()
                .frame(width: 10, height: 6)
                .rotationEffect(showAuthType6GFlag ?  .degrees(180):.degrees(0))
                .disabled(true)
                .background(Color.clear)
                .padding(.trailing ,20)
        }
        .gesture(drag)
        .frame(width:width/2,height:40)
        .overlay(
            RoundedCorner(radius: 0,corners: .topLeft)
           .stroke(Color.c0x696A6B, lineWidth: 1)
         )
        .onTapGesture {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.showAuthType6GFlag = true
                self.AuthROW = lable
               // print("tap:\(self.AuthROW)")
                if let idx = Auth6GArray.firstIndex(where: { $0 == Detail.wireless6G[lable].value! }) {
                    AuthArraySelected = idx
                }
                else
                {
                    AuthArraySelected = 0
                }
            }

        }
        .accessibility(addTraits: .isButton)
        .accessibilityIdentifier("SSID6GAuth\(lable)")
    }
    @ViewBuilder
    func edit6G(width:CGFloat,lable:Int,overText:Int,i:Int)->some View {
        HStack(spacing:0) {
                TextField("\(Detail.wireless6G[lable].value!)",text:$Detail.wireless6G[lable].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                    if editingChanged {
                        
                        self.lastHoveredId = Detail.lasttag[lable + overText]
                    }
                   })
                    .frame(width: width/2-30, height: 35, alignment:.leading)
                    .background(i%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
                    .font(.InterMedium12)
                    .textFieldStyle(MyWiBandsTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[lable + overText], width: width/2-30, height: 35))
                    .keyboardType(.default)
                    .accessibilityIdentifier("psk6G\(lable)")
                    .onReceive(Just(Detail.wireless6G[lable].value)) { _ in
                        if self.lastHoveredId == Detail.lasttag[lable + overText]{
                        if Detail.wireless6G[lable].name == "Pre-shared Key"
                        {
                            if Detail.wireless6G[lable - 1].value == "None" || Detail.wireless6G[lable - 1].value == "WEP" || Detail.wireless6G[lable - 1].value == "OWE"
                            {
                                return
                            }
                            if Detail.wireless6G[lable].value!.count >= WPAMin && Detail.wireless6G[lable].value!.count <= WPAMax
                            {
                                let isAlphanumerics = Detail.wireless6G[lable].value!.rangeOfCharacter(from: nonAlphanumericCharacters) == nil
                                if isAlphanumerics
                                {
                                    self.pskAlert = false
                                }
                                else
                                {
                                    self.pskAlert = true
                                }
                            }
                            else
                            {
                                self.pskAlert = true
                            }
                        }
                        else if  Detail.wireless6G[lable].name == "SSID"
                        {
                            if Detail.wireless6G[lable].value!.count >= SSIDMin && Detail.wireless6G[lable].value!.count <= SSIDMax
                            {

                                self.SSIDAlert = false
                            }
                            else
                            {
                                self.SSIDAlert = true
                            }
                        }
                        }
                    }
            
        }
        .frame(width:(CGFloat)(width/2),height:40)
        .overlay(
            RoundedCorner(radius: 0,corners: .topLeft)
           .stroke(Color.c0x696A6B, lineWidth: 1)
         )
    }
    @ViewBuilder
    func wirelessDetail6G(detail:[WirelessBody],bands:Int,width:CGFloat,flag:Bool,overText:Int)->some View {
        ForEach(0..<bands, id:\.self) { i in
           
            GridStack(rows: 4, columns: 1) { row, col in
                if editFlag {
                    if row == 0 {
                        Text("\(detail[row+i*4].name!)\(i+1)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                 .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    else
                    {
                        Text("\(detail[row+i*4].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                 .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    
                    if detail[row+i*4].name == "Authentication Type" {
                        let lable = row + i * 4
                        edit6GAuth(width: width, lable: lable,i:i)
                    }
                    else if detail[row+i*4].name == "Pre-shared Key" && ( Detail.wireless6G[row+i*4 - 1].value == "None" || Detail.wireless6G[row+i*4 - 1].value == "WEP" || Detail.wireless6G[row+i*4 - 1].value == "OWE" )
                       
                    {
                        Text("")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                              .clipped()
                        
                    }
                    else
                    {
                        let lable = row + i * 4
                        edit6G(width: width, lable: lable, overText: overText,i:i)
                    }
                }
                else
                {
                    if row == 0 {
                        Text("\(detail[row+i*4].name!)\(i+1)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                 .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    else
                    {
                        Text("\(detail[row+i*4].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                 .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    
                    if detail[row+i*4].name == "Authentication Type" || detail[row+i*4].name == "SSID" {
                        
                        MarqueeWiBands6GText(Detail: Detail, index: row+i*4, width:width/2-30)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .background(i%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
                            .padding(.horizontal, 15)
                            .font(.InterMedium12)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 0,corners: .topLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                                   
                         )
                       
                    }
                    else{
                    Text("\(Detail.wireless6G[row+i*4].value!)")
                    .frame(width: width/2-30, height: 40, alignment:.leading)
                    .padding(.horizontal, 15)
                          .font(.InterMedium12)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius: 3,corners: .topLeft)
                             .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                    }
                }
            }
            .font(.InterMedium12)
            .foregroundColor(Color.c0x242527)
            .background(i%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
            .cornerRadius(3)
            .padding(.top,12)

        }
   
    }
    @ViewBuilder
    func WirelessClients(width:CGFloat)->some View {
        if funcPressed == 3 {
        HStack {
            Text("Wireless Clients")
                .frame(width:width/2,height: 30,alignment: .leading)
                .font(.InterRegular18)
                .foregroundColor(Color.c0x242527)
                    .padding(.leading,20)
            Button(action: {
                
                self.blurAmount = 2
                
                Detail.getClientsCount = 0
                Detail.wirelessClients.removeAll(keepingCapacity: true)
                Detail.mobilekick.removeAll(keepingCapacity: true)
                Detail.mobileIPSSID.removeAll(keepingCapacity: true)
                DispatchQueue.main.async {
                Detail.getWirelessClients(GHz: "2")
                Detail.getWirelessClients(GHz: "5")
                Detail.getWirelessClients(GHz: "6")
                }
                var currentNetworkInfo: [String: Any] = [:]
                    getNetworkInfo { (wifiInfo) in

                      currentNetworkInfo = wifiInfo
                        NSLog("currentNetworkInfo : \(currentNetworkInfo["SSID"])")
                        
                        var arr  = self.getIPAddress()
                        NSLog("arr : \(arr)")
                        
                        Detail.CurrentIP = arr
                        Detail.CurrentSSID = "\(currentNetworkInfo["SSID"])"
               }
                
                self.showwirelessClients = true
                
//                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//                    self.blurAmount = 0.0
//                    self.showWaitRequest = false
//                }
                
            }) {
                Image("spinner9")
                    .resizable()
                    .frame(width:25,height: 25)
                    .padding(.trailing,10)
                
            }
            .frame(width:width/2,height: 30,alignment: .trailing)
            .padding(.trailing,40)
            .onReceive(Detail.$getClientsCount) { count in
                if support6G.contains(detail.model!)
                {
                    if count == 3
                    {
                        
                        self.showwirelessClients = false
                        self.blurAmount = 0
                        
                    }
                }
                else
                {
                    if count == 2
                    {
                        
                        self.showwirelessClients = false
                        self.blurAmount = 0
                       
                    }
                }
                
            }
            .accessibility(addTraits: .isButton)
            .accessibilityIdentifier("connectclients")
            
        }
        
       
         //   ZStack(){
                ScrollView(.vertical) {
                    VStack(alignment:.leading) {

                        ForEach(0..<Detail.wirelessClients.count/10, id: \.self) { i in
                               
                            HStack(spacing:5) {
                                    VStack(spacing:0){
                                        showWirelessClientsList(i:i,width: width-40)
                                    }
                                    .frame(width:width,height: 120)
                                    .overlay(
                                        RoundedCorner(radius: 3,corners: .allCorners)
                                            .stroke(Color.c0x696A6B, lineWidth: 1)
                                     )
                                    .accessibility(addTraits: .isButton)
                                    .accessibilityIdentifier("client")
                                    
                                    Button(action: {
                                        
                                        Detail.deleteClients(i: i, radio: Detail.mobilekick[i].radio!, ssid_index: Detail.mobilekick[i].ssid_index!, client_mac: Detail.mobilekick[i].client_mac!)
                                        self.offsets.remove(at: i)
                                        
                                    }) {
                                        Image("cross")
                                            .resizable()
                                            .frame(width:25,height: 25)
                                            .opacity(Detail.mobilekick[i].current == true ? 0.3:1.0)
                                    }
                                    .frame(width:50,height:50)
                                    .accessibilityIdentifier("deleteclient")
                                    
                                }
                                .padding(.top,5)
                                .padding(.trailing, -63)
                                .padding(.bottom,2)
                                .offset(x: offsets[i].width)
                                .disabled(Detail.mobilekick[i].current == true ? true:false)
                                .gesture(
                                    DragGesture( coordinateSpace: .global)
                                        .onChanged { gesture in
                                            withAnimation(.spring()) {
                                                self.offsets[i] = gesture.translation
                                                if offsets[i].width > 50 {
                                                    self.offsets[i] = .zero
                                                }
                                            }
                                            NSLog("x : \(gesture.location.x) , y : \(gesture.location.y)")
                                        }
                                        .onEnded { _ in
                                            withAnimation(.spring()) {
//                                                if self.offsets[i].width < -100 {
//                                                  //  Detail.deleteClients(i: i)
//                                                  //  self.offsets.remove(at: i)
//                                                }
//                                                else
                                                    if self.offsets[i].width < -50 {
                                                    self.offsets[i].width = -50
                                                }
                                            }
                                        }
                                )
//                                .accessibility(addTraits: .isButton)
//                                .accessibilityIdentifier("client")
                               // .disabled(!self.editFlag)
                         } // foreach
                    }//vstack
                    .frame(width:width+20,alignment: .center)
                    .padding(.leading,25)
                    .padding(.top,5)
                    
                }//scroll
                .frame(width:width,alignment: .center)
        }
    }
   @ViewBuilder
    func showWirelessClientsList(i:Int,width:CGFloat)->some View
    {
        ForEach(0..<5,id:\.self) { index in
            if index <= 1 {
                HStack{
                    Text(Detail.wirelessClients[i*10+index*2].value!)
                        .font(.InterMedium12)
                     .multilineTextAlignment(.leading)
                     .foregroundColor(.c0x242527)
                    .frame(width:width/2,height: 25,alignment: .leading)

                    Text(Detail.wirelessClients[i*10+index*2+1].value!)
                        .font(.InterMedium12)
                    .multilineTextAlignment(.trailing)
                    .foregroundColor(.c0x242527)
                    .frame(width:width/2,height: 25,alignment: .trailing)
                }
                .frame(height:25)
                .padding(.top,-5)
            }
            else
            {
                HStack{
                    Text("\(Detail.wirelessClients[i*10+index*2].name!): \(Detail.wirelessClients[i*10+index*2].value!)")
                        .font(.InterMedium12)
                     .multilineTextAlignment(.leading)
                    .foregroundColor(.c0x242527)
                    .frame(width:width/2+50,height: 25,alignment: .leading)

                    Text("\(Detail.wirelessClients[i*10+index*2+1].name!): \(Detail.wirelessClients[i*10+index*2+1].value!)")
                        .font(.InterMedium12)
                    .multilineTextAlignment(.trailing)
                    .foregroundColor(.c0x242527)
                    .frame(width:width/2-50,height: 25,alignment: .trailing)
                }
                .frame(height:25)
                .padding(.top,-5)
            }
        }
    }
    
    @ViewBuilder
    func DateAndTime(width:CGFloat)->some View {
        if funcPressed == 4 {
        Text("Date And Time")
                .frame(height: 30)
                .font(.InterRegular18)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
        
        VStack{
            
                Text("Local Time")
                .frame(width:width,height: 30,alignment: .leading)
                    .font(.InterRegular14)
                    .foregroundColor(Color.c0x242527)
                    .padding(.top,12)
            
            if !self.editFlag || self.onNTPServer {
            GridStack(rows: 6, columns: 1) { row, col in
                if row == 0
                {
                    Text("\(Detail.LocalTime[row].name!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .topLeft)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                   
                    Text("\(Detail.LocalTime[row].value!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .topRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                }
                else if row == 5
                {
                    Text("\(Detail.LocalTime[row].name!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .bottomLeft)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                   
                    Text("\(Detail.LocalTime[row].value!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .bottomRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                }
                else
                {
                    Text("\(Detail.LocalTime[row].name!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 0,corners: .topLeft)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                   
                    Text("\(Detail.LocalTime[row].value!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 0,corners: .topRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                    }
                }
                .frame(width: width)
                .padding(.top,12)
                .background(Color.clear)
            }
            else if editFlag && !self.onNTPServer
            {
                GridStack(rows: 6, columns: 1) { row, col in
                    if row == 0
                    {
                        Text("\(Detail.LocalTime[row].name!)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .topLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                        HStack(spacing:0){
                        Text("\(Detail.LocalTime[row].value!)")
                                .frame(width: width/2-50, height: 35, alignment:.leading)
                                .background(Color.white)
                                .padding(.leading, 15)
                                .font(.InterMedium12)
                                .foregroundColor(Color.c0x242527)
                                .gesture(drag)
                        
                            Image("down")
                                .resizable()
                                .frame(width: 10, height: 6)
                                .rotationEffect(showLocalTimeYear ?  .degrees(180):.degrees(0))
                                .disabled(true)
                                .background(Color.clear)
                                .padding(.trailing ,20)
                        }
                        .gesture(drag)
                        .frame(width: width/2,height:40)
                        .onTapGesture {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            self.showLocalTimeYear = true
                            }

                        }
                        .overlay(
                            RoundedCorner(radius: 3,corners: .topRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                        .accessibility(addTraits: .isButton)
                        .accessibilityIdentifier("LocalTime0")
                    }
                    else if row == 5
                    {
                        Text("\(Detail.LocalTime[row].name!)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                        
                        HStack(spacing:0){
                        Text("\(Detail.LocalTime[row].value!)")
                                .frame(width: width/2-50, height: 35, alignment:.leading)
                                .background(Color.white)
                                .padding(.leading, 15)
                                .font(.InterMedium12)
                                .foregroundColor(Color.c0x242527)
                                .gesture(drag)
                        
                            Image("down")
                                .resizable()
                                .frame(width: 10, height: 6)
                                .rotationEffect(showLocalTimeSeconds ?  .degrees(180):.degrees(0))
                                .disabled(true)
                                .background(Color.clear)
                                .padding(.trailing ,20)
                        }
                        .gesture(drag)
                        .frame(width: width/2,height:40)
                        .onTapGesture {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            self.showLocalTimeSeconds = true
                            }
                        }
                        .overlay(
                            RoundedCorner(radius: 3,corners: .bottomRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                        .accessibility(addTraits: .isButton)
                        .accessibilityIdentifier("LocalTime5")
                    }
                    else
                    {
                        Text("\(Detail.LocalTime[row].name!)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 0,corners: .topLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                       
                        
                        
                        HStack(spacing:0){
                        Text("\(Detail.LocalTime[row].value!)")
                                .frame(width: width/2-50, height: 35, alignment:.leading)
                                .background(Color.white)
                                .padding(.leading, 15)
                                .font(.InterMedium12)
                                .foregroundColor(Color.c0x242527)
                                .gesture(drag)

                            Image("down")
                                .resizable()
                                .frame(width: 10, height: 6)
                                .rotationEffect(showLocalTimeMonth ?  .degrees(180):.degrees(0))
                                .disabled(true)
                                .background(Color.clear)
                                .padding(.trailing ,20)
                        }
                        .gesture(drag)
                        .frame(width: width/2,height:40)
                        .onTapGesture {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                              if row == 1
                              {
                                  self.showLocalTimeMonth = true
                              }
                              else if row == 2
                              {
                                  self.showLocalTimeDay = true
                              }
                              else if row == 3
                              {
                                  self.showLocalTimeHours = true
                              }
                              else if row == 4
                              {
                                  self.showLocalTimeMinutes = true
                              }
                            }
                        }
                        .overlay(
                            RoundedCorner(radius: 0,corners: .bottomRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                        .accessibility(addTraits: .isButton)
                        .accessibilityIdentifier("LocalTime\(row)")
                   }
              } //Gra..
              .frame(width: width)
              .padding(.top,12)
            } // else
            
            Button(action: {
               
            }) {
                
                if !editFlag || self.onNTPServer {
                    HStack(spacing:0) {
                        Text("Acquire Current Time From Your Phone")
                            .frame( height: 30, alignment: .center)
                            .padding(.bottom,5)
                            .font(.InterMedium14)
                            .foregroundColor(Color(hex:0xD6D8DA))
                            .background(
                                RoundedRectangle(cornerRadius: 5, style: .continuous)
                                    .fill(Color(hex:0xFAFAFB))
                                    .padding(.bottom,5)
                                    .padding(.leading,-5)
                                    .padding(.trailing,-3)
                                   
                            )
                    }
                    .padding(.trailing,5)
                    .background(
                        RoundedRectangle(cornerRadius: 5, style: .continuous)
                            .stroke(Color(hex:0xD6D8DA), lineWidth: 1)
                            .padding(.bottom,5)
                            .padding(.leading,-6)
                            
                    )
                        
                }
                else
                {
                    HStack(spacing:0) {
                        Text("Acquire Current Time From Your Phone")
                            .frame( height: 30, alignment: .center)
                            .padding(.bottom,5)
                            .font(.InterMedium14)
                            .foregroundColor(Color.c0x242527)
                            .background(
                                RoundedRectangle(cornerRadius: 5, style: .continuous)
                                    .fill(Color(hex:0xFFFFFF))
                                    .padding(.bottom,5)
                                    .padding(.leading,-5)
                                    .padding(.trailing,-3)
                            )
                    }
                    .padding(.trailing,5)
                    .background(
                        RoundedRectangle(cornerRadius: 5, style: .continuous)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                            .padding(.bottom,5)
                            .padding(.leading,-6)
                            
                    )
                    .onTapGesture {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        let formattedDate = dateFormatter.string(from: Date())
                        print(formattedDate)
                        let newinput = formattedDate.split(separator: " ")
                        let yymmdd = String(newinput[0]).split(separator: "-")
                        let uptime = String(newinput[1]).regex(pattern: "\\d{2}")
                        Detail.LocalTime[0].value = String(yymmdd[0])
                        Detail.LocalTime[1].value = Detail.month[Int(yymmdd[1])!-1]
                        Detail.LocalTime[2].value = String(yymmdd[2])
                        Detail.LocalTime[3].value = String(uptime[0])
                        Detail.LocalTime[4].value = String(uptime[1])
                        Detail.LocalTime[5].value = String(uptime[2])
                    }
                    .accessibility(addTraits: .isButton)
                    .accessibilityIdentifier("TimeFromYourPhone")
                    
                }
               
            }
            .frame( width:width,height: 30, alignment: .trailing)
            .padding(.top,5)
            
        } //VStack
        .frame(width: width)
        .padding(.leading,20)
        .padding(.top,5)
        .background(Color.clear)
  
        VStack{
          
            Text("NTP Time Server")
                    .frame(width:width,height: 30,alignment: .leading)
                    .font(.InterRegular14)
                    .foregroundColor(Color.c0x242527)
                    .padding(.top,12)
            
            Toggle(isOn: $onNTPServer)
            {
                Text("NTP")
            }
            .toggleStyle(customNTP1ToggleStyle(edit: $editFlag))
            .frame(width:width,alignment: .center)
            .padding(.top,5)
            .onTapGesture {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
               // backPress.wirelessType[2] = Wireless6G
                self.onNTPServer.toggle()
                if self.onNTPServer
                {
                    self.FromNTPServerlag = true
                }
                else
                {
                    self.FromNTPServerlag = false
                }
                }
            }
            .disabled(!self.editFlag)
            .accessibilityIdentifier("ToggleNTPServer")
            .background(Color.clear)
//            .accessibility(addTraits: .isButton)
//            .accessibilityIdentifier("ToggleNTPServer")
            
                
            if self.editFlag {
                GridStack(rows: 3, columns: 1) { row, col in
                    if row == 0
                    {
                        Text("\(Detail.NTPTimeServer[row].name!)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .topLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                        ZStack{
                            HStack(spacing:0) {
                                if DaylightSaving {
                                Text("Enable")
                                    .frame(width:width/2-50, height: 35, alignment: .leading)
                                    .padding(.leading,15)
                                    .font(.InterMedium12)
                                    .foregroundColor(Color.c0x242527)
                                    .gesture(drag)
                                    //.foregroundColor(Color(hex: 0x818384))
                                }
                                else
                                {
                                    Text("Disable")
                                        .frame(width:width/2-50, height: 35, alignment: .leading)
                                        .padding(.leading,15)
                                        .font(.InterMedium12)
                                        .foregroundColor(Color.c0x242527)
                                        .gesture(drag)
                                       // .foregroundColor(Color(hex: 0x818384))
                                }

                                Image("down")
                                    .resizable()
                                    .frame(width: 10, height: 6)
                                    .rotationEffect(showNTPServerType ?  .degrees(180):.degrees(0))
                                    .disabled(true)
                                    .background(Color.clear)
                                    .padding(.trailing ,20)
                            }
                            .gesture(drag)
                            //.padding(.horizontal , 10)
                            .onTapGesture {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                    self.DaylightSaving.toggle()
                                    NSLog("\(self.DaylightSaving == true ? 1:0)" )
                                }
                            }
                            .accessibility(addTraits: .isButton)
                            .accessibilityIdentifier("ToggleDaylightSaving")
                        }
                        .frame(width: width/2, height: 40, alignment:.leading)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .topRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                        
                    }
                    else if row == 2
                    {
                        Text("\(Detail.NTPTimeServer[row].name!)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                        HStack(spacing:0){
                        TextField("\(Detail.NTPTimeServer[row].value!)",text: $NTPinterval, onEditingChanged: { (editingChanged) in
                            
                            if editingChanged {
                               
                                self.lastHoveredId = Detail.lasttag[2]
                            }
                             
                        })
                            .font(.InterMedium12)
                            .frame(width: 30, height: 35, alignment:.leading)
                            .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[2], width: 30, height: 35))
                            .padding(.leading,-20)
                            .accessibilityIdentifier("NTPTimeServerHours")
                            
                            Text("(Hours)")
                                .frame(width: width/4-30,alignment:.leading)
                                .font(.InterMedium12)
                                .foregroundColor(Color.c0x242527)
                                .padding(.leading,20)
                                
                        }
                        .frame(width: width/2, height: 40, alignment:.center)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .bottomRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                        //.disabled(!self.onNTPServer)
                    }
                    else
                    {
                        Text("\(Detail.NTPTimeServer[row].name!)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 80, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 0,corners: .topLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
      
                        ZStack(alignment:.top){
                            VStack(spacing:0) {
                            HStack(spacing:0) {
                                Text(NTPServerType)
                                    .frame(width:width/2-50, height: 35, alignment: .leading)
                                    .padding(.leading,15)
                                    .font(.InterMedium12)
                                   // .foregroundColor(Color(hex: 0x818384))
                                    .foregroundColor(Color.c0x242527)
                                    .gesture(drag)

                                Image("down")
                                    .resizable()
                                    .frame(width: 10, height: 6)
                                    .rotationEffect(showNTPServerType ?  .degrees(180):.degrees(0))
                                    .disabled(true)
                                    .background(Color.clear)
                                    .padding(.trailing ,20)
                            }
                            .gesture(drag)
                            .onTapGesture {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                self.showNTPServerType = true
                                }
                               
                            }
                            .accessibility(addTraits: .isButton)
                            .accessibilityIdentifier("NTPServerType")
                                
                                TextField(FromNTPServer,text:$FromNTPServer, onEditingChanged: { (editingChanged) in
                                
                                if editingChanged {

                                    self.lastHoveredId = Detail.lasttag[3]
                                   
                                }
                                
                                })

                                .frame(width: width/2-50, height: 35, alignment:.leading)
                                .background(Color.white)
                                .font(.InterMedium12)
                                .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[3], width: width/2-50, height: 35))
                                .padding(.horizontal,15)
                                .disabled(self.FromNTPServerlag)
                                .accessibilityIdentifier("FromNTPServerName")
                                
                            }
                                
                        }.frame(width: width/2, height: 80, alignment:.center)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomRight)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                           // .disabled(!self.onNTPServer)
                            
                    }
                }
                .frame(width: width,alignment: .center)
                
            }
            else
            {
                GridStack(rows: 3, columns: 1) { row, col in
                    if row == 0
                    {
                        Text("\(Detail.NTPTimeServer[row].name!)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .topLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                        if DaylightSaving {
                            Text("Enable")
                                .font(.InterMedium12)
                                .frame(width: width/2-30, height: 40, alignment:.leading)
                                .padding(.horizontal, 15)
                                .foregroundColor(Color.c0x242527)
                                .overlay(
                                    RoundedCorner(radius: 3,corners: .topRight)
                                   .stroke(Color.c0x696A6B, lineWidth: 1)
                            )
                        }
                        else
                        {
                            Text("Disable")
                                .font(.InterMedium12)
                                .frame(width: width/2-30, height: 40, alignment:.leading)
                                .padding(.horizontal, 15)
                                .foregroundColor(Color.c0x242527)
                                .overlay(
                                    RoundedCorner(radius: 3,corners: .topRight)
                                   .stroke(Color.c0x696A6B, lineWidth: 1)
                            )
                        }
                    }
                    else if row == 2
                    {
                        Text("\(Detail.NTPTimeServer[row].name!)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                        Text("\(Detail.NTPTimeServer[row].value!) (Hours)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomRight)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                    }
                    else
                    {
                        Text("\(Detail.NTPTimeServer[row].name!)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 80, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 0,corners: .topLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                        ZStack(alignment:.top){
                            VStack(spacing:0) {
                                Text(self.NTPServerType)
                                    .font(.InterMedium12)
                                    .frame(width: width/2-30, height: 40, alignment:.leading)
                                    .padding(.horizontal, 15)
                                    .foregroundColor(Color.c0x242527)
                                    
                                Text(self.FromNTPServer)
                                    .font(.InterMedium12)
                                    .frame(width: width/2-30, height: 40, alignment:.leading)
                                    .padding(.horizontal, 15)
                                    .foregroundColor(Color.c0x242527)
                            }
                        }
                        .frame(width: width/2, height: 80, alignment:.center)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .bottomRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )

                    }//else
                }
                .frame(width: width,alignment: .center)
                .disabled(true)
                
            }
            
            
        }
        .frame(width: width)
        .padding(.leading,20)
        .padding(.top,5)
   
        VStack{
            
                Text("Time Zone")
                    .frame(width:width,height: 30,alignment: .leading)
                    .font(.InterRegular14)
                    .foregroundColor(Color.c0x242527)
                    .padding(.top,12)
                
            HStack(spacing:0) {
               // Text(self.timeZone)
                if editFlag {
                    Text(self.timeZone)
                        .frame(width:width-20, height: 40, alignment: .leading)
                        .padding(.bottom,5)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                }
                else {
                    if Detail.TimeZone != ""
                    {
                MarqueeTimeZoneText(Detail: Detail, width: width-20)
                    .frame(width:width-20, height: 40, alignment: .leading)
                    .padding(.bottom,5)
                    .font(.InterRegular14)
                    .foregroundColor(Color.c0x242527)
                    .gesture(drag)
                    }
                }
                if editFlag {
                
                Image("down")
                    .resizable()
                    .frame(width: 10, height: 6)
                    .rotationEffect(showTimeZone ?  .degrees(180):.degrees(0))
                    .disabled(true)
                    .background(Color.clear)
                    .padding(.trailing ,0)
                }
            }
            .gesture(drag)
            .onTapGesture {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.showTimeZone = true
                }
            }
            .accessibility(addTraits: .isButton)
            .accessibilityIdentifier("Opentimezone")
        }
        .frame(width: width)
        .padding(.leading,20)
        .padding(.top,5)
        .disabled(!self.editFlag)
        .onReceive(Detail.$getDateAndTimeResult) { result in
            if result != nil && self.requestDate == true
            {
                if result?.error_code == 0 {
                NSLog("\(result)")
                self.requestDate = false
                self.blurAmount = 0
                
                self.onNTPServer = (result?.data?.ntp_enabled == 0 ? false:true)
                self.FromNTPServer = result?.data?.ntp_server_name ?? "0.sophos.pool.ntp.org"
                self.DaylightSaving = (((result?.data?.auto_daylight_save) == 1) ? true:false)
                self.timeZone = TimeZoneList[(result?.data?.time_zone)!]
                
                if self.FromNTPServer == "0.sophos.pool.ntp.org"
                {
                    self.NTPServerType = "Global"
                }
                else if self.FromNTPServer == "0.africa.pool.ntp.org"
                {
                    self.NTPServerType = "Africa"
                }
                else if self.FromNTPServer == "0.asia.pool.ntp.org"
                {
                    self.NTPServerType = "Asia"
                }
                else if self.FromNTPServer == "0.europe.pool.ntp.org"
                {
                    self.NTPServerType = "Europe"
                }
                else if self.FromNTPServer == "0.north-america.pool.ntp.org"
                {
                    self.NTPServerType = "North America"
                }
                else if self.FromNTPServer == "0.oceania.pool.ntp.org"
                {
                    self.NTPServerType = "Oceania"
                }
                else if self.FromNTPServer == "0.south-america.pool.ntp.org"
                {
                    self.NTPServerType = "South America"
                }
                else
                {
                    self.NTPServerType = "User-Defined"
                    self.FromNTPServerlag = true
                }
                }
                else
                {
                    self.requestDate = false
                    self.blurAmount = 0
                    self.showErr = true
                    self.blurAmount = 2
                }
            }
            
        }
       
            
        }
    }
    @ViewBuilder
    func PingTest(width:CGFloat)->some View {
        Text("Ping Test")
                .frame(height: 30)
                .font(.InterRegular18)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
                
        
        VStack{
            ZStack(alignment:.top){
                Text(self.pingIPplaceholder)
                    .foregroundColor(Color(hex:0x888888))
                    .font(.InterRegular14)
                    .frame(width: width,height: 40,alignment:.leading)
                    .padding(.leading,30)
                    .padding(.top,5)
                    
            HStack(spacing:0){
                TextField("",text:$pingIP, onEditingChanged: { (editingChanged) in
                    if editingChanged {
                        self.lastHoveredId = Detail.lasttag[1]
                        self.pingIPplaceholder = ""
                    }
                })
                    .accessibilityIdentifier("pingIP")
                    .frame(height: 40,alignment:.trailing)
                    .font(.InterRegular14)
                    .textFieldStyle(MyTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[1], width: width-50, height: 40))
                   // .keyboardType(.decimalPad)
                    .onChange(of: pingIP) {
                       if $0 == ""
                        {
                           self.pingIPplaceholder = "Destination Address"
                       }
                        else
                        {
                            self.pingIPplaceholder = ""
                        }
                    }
                    
           
            }
            .frame(width: width)
            .padding(.leading,20)
            .padding(.top,5)
            }
            
            
            Button(action: {
               // if self.pingIP.isIpAddress() {
                  // Detail.startPinging(ip: pingIP,times: 10)
                
                Detail.pingTest(address: pingIP,isIPv4: self.pingIP.isIPv4(),isIPv6: self.pingIP.isIPv6())
                
                    
                  //  self.progressValue = 0.0
                    self.showWaitPingRequest = true
                    self.blurAmount = 2
                   // self.sysReloadflag = true
                    //self.sysReloadTime = 0.002
              //  }
                
               
            }) {
                    
                HStack(spacing:0) {
                    Text("Execute")
                        .frame( height: 40, alignment: .center)
                        .padding(.bottom,2)
                        
                        .font(.InterMedium14)
                        .foregroundColor(self.pingIP.isIpAddress() ? Color.c0x242527:Color(hex:0xD6D8DA))
                        .background(
                            RoundedRectangle(cornerRadius: 5, style: .continuous)
                                .fill(self.pingIP.isIpAddress() ? Color(hex:0xFFFFFF):Color(hex:0xFAFAFB))
                                .padding(.bottom,3)
                                .padding([.leading,.trailing],-9)
                                .padding(.top,3)
                                //.stroke(focused ? Color.gray : Color.gray, lineWidth: 1)
                                
                        )
                    
                }
                .background(
                    RoundedRectangle(cornerRadius: 3, style: .continuous)
                        .stroke(self.pingIP.isIpAddress() ? Color.c0x696A6B:Color(hex:0xD6D8DA), lineWidth: 1)
                        .padding(.bottom,2)
                        .padding([.leading,.trailing],-10)
                        .padding(.top,2)
                        
                )
               
            }
            .disabled(!self.pingIP.isIpAddress())
            .frame( width:width,height: 40, alignment: .trailing)
            .padding(.top,5)
            .accessibilityIdentifier("pingIPExecute")
            
            Text("Result")
                .frame(width:width,height: 30,alignment: .leading)
                .font(.InterMedium14)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
                .padding(.top,-8)
            
            VStack{
                ScrollView(.vertical, showsIndicators: false){
                        ScrollViewReader { pageScroller in
                        VStack{
                            Text(self.pingMessage)
                            .font(.InterMedium12)
                            .foregroundColor(Color.c0x242527)
                            .padding(.leading,5)
                            .id(0)
      
                        }
                        .onReceive(Detail.$pingMessage){ result in
//                            self.pingMessage = result + "\n" + self.pingMessage
//                            withAnimation {
//                                pageScroller.scrollTo(0, anchor: .bottom)
//                            }
                            self.pingMessage = result
                          //  self.progressValue = 1.0
                            self.showWaitPingRequest = false
                            blurAmount = 0.0
                           // self.sysReloadflag = false
                        }
                        .frame(maxWidth: .infinity , alignment: .leading)
                        }
                    }
                
            }
            .frame(width:width,height:250,alignment: .leading) //(width/3+width/2)
            .padding(.leading,20)
            .background(
                RoundedRectangle(cornerRadius: 5, style: .continuous)
                    .stroke(Color.c0x696A6B, lineWidth: 1)
                    .padding(.bottom,5)
                    .padding(.leading,20)
                    .padding(.top,-8)
            )
            
        }
        .padding(.leading,-5)
        //.disabled(!save)
        
    }
    @ViewBuilder
    func TracerouteTest(width:CGFloat)->some View {
        Text("Traceroute Test")
                .frame(height: 30)
                .font(.InterRegular18)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
        
        VStack{
            ZStack(alignment:.top){
                Text(self.tracerouteIPplaceholder)
                    .foregroundColor(Color.c0x696A6B)
                    .font(.InterRegular14)
                    .frame(width: width,height: 40,alignment:.leading)
                    .padding(.leading,30)
                    .padding(.top,5)
                    
            HStack(spacing:0){
                TextField("",text:$tracerouteIP, onEditingChanged: { (editingChanged) in
                    if editingChanged {
                        
                        self.lastHoveredId = Detail.lasttag[1]
                        self.tracerouteIPplaceholder = ""
                    }
                })
                    
                    .accessibilityIdentifier("tracerouteIP")
                    .frame(height: 40,alignment:.trailing)
                    .font(.InterRegular14)
                    .textFieldStyle(MyTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: Detail.lasttag[1], width: width-50, height: 40))
                    //.keyboardType(.decimalPad)
                    .onChange(of: tracerouteIP) {
                       if $0 == ""
                        {
                           self.tracerouteIPplaceholder = "Destination Address"
                       }
                        else
                        {
                            self.tracerouteIPplaceholder = ""
                        }
                    }
            }
            .frame(width: width)
            .padding(.leading,20)
            .padding(.top,5)
        }
            
            Button(action: {
              //  if tracerouteIP.isIpAddress() {
                    Detail.tracerouteTest(address: tracerouteIP)
                    //Detail.startPinging(ip: tracerouteIP,times: 10)
                    self.showWaitPingRequest = true
                    self.blurAmount = 2
              //  }
                
            }) {
                    
                HStack(spacing:0) {
                    Text("Execute")
                        .frame( height: 40, alignment: .center)
                        .padding(.bottom,2)
                        .font(.InterMedium14)
                        .foregroundColor(self.tracerouteIP.isIpAddress() ? Color.c0x242527:Color(hex:0xD6D8DA))
                        .background(
                            RoundedRectangle(cornerRadius: 3, style: .continuous)
                                .fill(self.tracerouteIP.isIpAddress() ? Color(hex:0xFFFFFF):Color(hex:0xFAFAFB))
                                .padding(.bottom,3)
                                .padding([.leading,.trailing],-9)
                                .padding(.top,3)
                                //.stroke(focused ? Color.gray : Color.gray, lineWidth: 1)
                                
                        )
                    
                }
                .background(
                    RoundedRectangle(cornerRadius: 3, style: .continuous)
                        .stroke(self.tracerouteIP.isIpAddress() ? Color.c0x696A6B:Color(hex:0xD6D8DA), lineWidth: 1)
                        .padding(.bottom,2)
                        .padding([.leading,.trailing],-10)
                        .padding(.top,2)
                    )
               
            }
            .frame( width:width,height: 40, alignment: .trailing)
            .padding(.top,5)
            .accessibilityIdentifier("tracerouteIPExecute")
            
            Text("Result")
                .frame(width:width,height: 30,alignment: .leading)
                .font(.InterMedium14)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
                .padding(.top,-8)
            
            VStack{
                ScrollView(.vertical, showsIndicators: false){
                        ScrollViewReader { pageScroller in
                        VStack{
                            Text(self.tracerouteMessage)
                            .font(.InterMedium12)
                            .foregroundColor(Color.c0x242527)
                            .padding(.leading,5)
                            .id(0)
      
                        }
                        .onReceive(Detail.$tracerouteMessage){ result in
                            self.tracerouteMessage = result //+ "\n" + self.tracerouteMessage
                            withAnimation {
                                pageScroller.scrollTo(0, anchor: .bottom)
                            }
                            self.showWaitPingRequest = false
                            blurAmount = 0.0
                        }
                        .frame(maxWidth: .infinity , alignment: .leading)
                        }
                    }
                
            }
            .frame(width:width,height:250,alignment: .leading) //(width/3+width/2)
            .padding(.leading,20)
            .background(
                RoundedRectangle(cornerRadius: 3, style: .continuous)
                    .stroke(Color.c0x696A6B, lineWidth: 1)
                    .padding(.bottom,5)
                    .padding(.leading,20)
                    .padding(.top,-8)
            )
            
        }
        .padding(.leading,-5)
    }
   
    @ViewBuilder
    func showsave(width:CGFloat)->some View {
        if self.save {
            ZStack {
                VStack(spacing:0){
            Text("Changes have been saved successfully")
                    .frame(width:width,height: 30)
                    .font(.InterRegular18)
                    .foregroundColor(Color.white)
                    .background(Color.c0x00851D)
                    .shadow(radius: 10)
                
            }
            }.popup(isPresented: $save, with: $save) { item in
               
            }
            .frame(width:width,height: 35)
            .position(x: width/2, y: 110)
            .onAppear {
                //Detail.system[0].value = value
               // Detail.system[1].value = value1
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.save = false
                }
            }
        }
        if self.saveErrFlag {
            ZStack {
                VStack(spacing:0){
            Text("  Invalid IP Address : \(saveErr)")
                    .multilineTextAlignment(.leading)
                    .lineLimit(2)
                    .frame(width:width,height: 40,alignment: .leading)
                    .font(.InterRegular16)
                    .foregroundColor(Color.white)
                    .background(Color.c0xDA3E00)
                    .shadow(radius: 10)
            }
            }.popup(isPresented: $saveErrFlag, with: $saveErrFlag) { item in
               
            }
            .frame(width:width,height: 40)
            .position(x: width/2, y: 110)
            .onAppear {
                //Detail.system[0].value = value
               // Detail.system[1].value = value1
                DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                    self.saveErrFlag = false
                    self.saveErr = ""
                }
            }
        }
    }
    @ViewBuilder
    func showDialog(tag:String,width:CGFloat,height:CGFloat,x:CGFloat,y:CGFloat,Content:String) -> some View {
        if self.showAlert {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                
                VStack() {
                    GeometryReader { geometry in
                    Text(Content)
                        .multilineTextAlignment(.center)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .position(x: geometry.size.width/2, y: geometry.size.height/3)
                    
                    HStack()  {
                        Button{
                            backPress.RebootFlag = true
                            backPress.SSDPDiscvoer.removeAll(keepingCapacity: true)
                            Detail.mobileIPSSID.removeAll(keepingCapacity: true)
                            Detail.getClientsCount = 0
                            Detail.getWirelessClients(GHz: "2")
                            Detail.getWirelessClients(GHz: "5")
                            Detail.getWirelessClients(GHz: "6")
                            self.check = true
                        } label: {
                            Text("Yes")
                                .frame(minWidth: 0, maxWidth: 100, minHeight: 0, maxHeight: 40)
                                .font(.InterMedium14)
                                .foregroundColor(.white)
                                .background(Color.c0x005BC8)
                                .cornerRadius(5)
                        }
                        .accessibilityIdentifier("rebootok")
                        .frame(width: 100, height: 40)
                        .onReceive(Detail.$getClientsCount) { Results in
                                if self.check {
                                    NSLog("$getClientsCount : \(Results)")
                                    
                                    var currentNetworkInfo: [String: Any] = [:]
                                    getNetworkInfo { (wifiInfo) in

                                      currentNetworkInfo = wifiInfo
                                        NSLog("currentNetworkInfo : \(currentNetworkInfo["SSID"])")
                                        
                                        var arr  = self.getIPAddress()
                                        NSLog("arr : \(arr)")
                                        
                                        let a = Detail.mobileIPSSID.filter { $0.value == arr ||  $0.value == "\(currentNetworkInfo["SSID"])"}
                                        NSLog("a : \(a)")
                                        
                                        if support6G.contains(detail.model!)
                                        {
                                            if Results == 3 //3 //6g API failed
                                            {
                                                if a.isEmpty
                                                {
                                                    NSLog("putReboot isEmpty")
                                                    Detail.putReboot()
                                                    self.showAlert = false
                                                    self.showWait = true
                                                    Detail.mobileIPSSID.removeAll(keepingCapacity: true)
                                                    self.check = false
                                                }
                                                else
                                                {
                                                    NSLog("putReboot")
                                                    self.showCurrentAPReboot = true
                                                    Detail.mobileIPSSID.removeAll(keepingCapacity: true)
                                                    self.check = false
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if Results == 2
                                            {
                                                if a.isEmpty
                                                {
                                                    Detail.putReboot()
                                                    self.showAlert = false
                                                    self.showWait = true
                                                    Detail.mobileIPSSID.removeAll(keepingCapacity: true)
                                                    self.check = false
                                                }
                                                else
                                                {
                                                    self.showCurrentAPReboot = true
                                                    Detail.mobileIPSSID.removeAll(keepingCapacity: true)
                                                    self.check = false
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                
//                                Detail.putReboot()
//                                self.showAlert = false
//                                self.showWait = true
                            }
                        
                        Spacer()
                        Button{
                            self.showAlert = false
                            self.blurAmount = 0
                        } label: {
                            ZStack{
                            RoundedRectangle(cornerRadius: 5)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                            Text("No")
                                .font(.InterMedium14)
                                .foregroundColor(Color.c0x242527)
                            }.frame(width: 100, height: 40)
                        }
                      }//HStack
                    .position(x: geometry.size.width/2, y: geometry.size.height - 35)
                    } // GeometryReader
                }
                .frame(width:width-200,height: 200,alignment: .center)
                .background(Color.white)
                
                
            }.popup(isPresented: $showAlert, with: $showAlert) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width-150,height: 200,alignment: .center)
            .position(x: x, y: y)
        }
        
        if self.showErr {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                
                VStack() {
                    GeometryReader { geometry in
                    Text("Could not connect to the server.")
                        .multilineTextAlignment(.center)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .position(x: geometry.size.width/2, y: geometry.size.height/3)
                    
                    HStack()  {
                        Button{
                            
                            self.showErr = false
                        } label: {
                            Text("Yes")
                                .frame(minWidth: 0, maxWidth: 100, minHeight: 0, maxHeight: 40)
                                .font(.InterMedium14)
                                .foregroundColor(.white)
                                .background(Color.c0x005BC8)
                                .cornerRadius(5)
                        }.frame(width: 100, height: 40)
                        
                        Spacer()
                        Button{
                            self.showErr = false
                        } label: {
                            ZStack{
                            RoundedRectangle(cornerRadius: 5)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                            Text("No")
                                .font(.InterMedium14)
                                .foregroundColor(Color.c0x242527)
                            }.frame(width: 100, height: 40)
                        }
                      }//HStack
                    .position(x: geometry.size.width/2, y: geometry.size.height - 35)
                    } // GeometryReader
                }
                .frame(width:width-200,height: 200,alignment: .center)
                .background(Color.white)
                
                
            }.popup(isPresented: $showErr, with: $showErr) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width-150,height: 200,alignment: .center)
            .position(x: x, y: y)
        }
        if self.showCurrentAPReboot {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                
                VStack() {
                    GeometryReader { geometry in
                    Text("Rebooting this AP will temporarily turn off any active networks that the AP is providing and disconnect wireless clients that are connected to these networks")
                        .multilineTextAlignment(.center)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .position(x: geometry.size.width/2, y: geometry.size.height/3)
                    
                    HStack()  {
                        Button{
                            Detail.putReboot()
                            self.showAlert = false
                            self.showWait = true
                            self.showCurrentAPReboot = false
                            self.sysReloadTime = 120
                            self.sysReloadTimeFlag = false
                        } label: {
                            Text("Yes")
                                .frame(minWidth: 0, maxWidth: 100, minHeight: 0, maxHeight: 40)
                                .font(.InterMedium14)
                                .foregroundColor(.white)
                                .background(Color.c0x005BC8)
                                .cornerRadius(5)
                        }.frame(width: 100, height: 40)
                       .accessibilityIdentifier("rebootokok")
//
//                        Spacer()
//                        Button{
//                            self.showErr = false
//                        } label: {
//                            ZStack{
//                            RoundedRectangle(cornerRadius: 5)
//                                    .stroke(Color.c0x696A6B, lineWidth: 1)
//                            Text("No")
//                                .font(.InterMedium14)
//                                .foregroundColor(Color.c0x242527)
//                            }.frame(width: 100, height: 40)
//                        }
                      }//HStack
                    .position(x: geometry.size.width/2, y: geometry.size.height - 35)
                    } // GeometryReader
                }
                .frame(width:width-200,height: 200,alignment: .center)
                .background(Color.white)
                
                
            }.popup(isPresented: $showCurrentAPReboot, with: $showCurrentAPReboot) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width-150,height: 200,alignment: .center)
            .position(x: x, y: y)
        }
    }
    @ViewBuilder
    func showWait(tag:String,width:CGFloat,height:CGFloat,x:CGFloat,y:CGFloat,Content:String) -> some View {
        if self.showWait {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                
                VStack() {
                    GeometryReader { geometry in
                    Text(Content)
                        .multilineTextAlignment(.center)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .position(x: geometry.size.width/2, y: geometry.size.height/3)
                        
//                        ProgressBar(progress: $progressValue)
//                            .frame(width: 50.0, height: 50.0)
//                            .position(x: geometry.size.width/2, y: geometry.size.height/1.5)
                        ProgressView()
                            .progressViewStyle(CircularProgressViewStyle(tint: Color.c0x242527))
                            .scaleEffect(x: 1, y: 1, anchor: .center)
                            .position(x: geometry.size.width/2, y: geometry.size.height/1.5)
                            .onReceive(timer) { i in
                                
                                //NSLog("progressValue : \(progressValue)")
                                if progressValue < 1.0  && progressValue >= 0.0 {
                                    if progressValue >= 0.89
                                    {
                                        progressValue = 1.0
                                        self.timer = Timer.publish(every: 3.0, on: .main, in: .common).autoconnect()
                                    }
                                    else
                                    {
                                        progressValue += 0.0015
                                    }
                                }
                                if progressValue == 1.0 {
                                    
                                   // DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                                       NSLog("discoverService : \(i)")
                                       
                                      // client.discoverService(forDuration: 3.0, searchTarget: "www.sophos.com:device:WLANAccessPointDevice:1", port: 1900)
                                        backPress.RebootFlag = false
                                       if backPress.SSDPDiscvoer.isEmpty
                                       {
                                           timeoutCount += 1
                                           
                                       }
                                       if timeoutCount == 22 //22
                                       {
                                           self.timer.upstream.connect().cancel()
                                           
                                           progressValue = 0.0
                                           self.showWait = false
                                           blurAmount = 0
                                           self.RebootErr = true
                                           self.APnotFound = true
                                       }
                                    
                                       for respone in backPress.SSDPDiscvoer
                                         {
                                             timeoutCount = 0
                                             let result = SSDPService(host: respone.key, response: respone.value)
                                             if result.Mac != nil
                                             {
                                                 if result.Mac == detail.mac {
                                                 let ssdp = SSDP(LOCATION: result.location, SERVER: result.server, ST: result.searchTarget, USN: result.uniqueServiceName, Vendor: result.Vendor, Mac: result.Mac, Model: result.Model, Name: result.Name, OperatorMode: result.OperatorMode, Identity: result.Identity, FirmwareVersion: result.FirmwareVersion, CPUUtilization: result.CPUUtilization, ConnectedUsers:result.ConnectedUsers, SNR: result.SNR, MemoryUtilization: result.MemoryUtilization, WiFiRadio: result.WiFiRadio, live: "", SOPHOS_INFO: result.SOPHOS_INFO)
                                                 
                                                 self.timer.upstream.connect().cancel()
                                                 
                                                 progressValue = 0.0
                                                 self.showWait = false
                                                 blurAmount = 0
                                                 self.RebootSuccess = true
                                                 }
                                             }
                                        }
                                   // }
                                    
                                }
                            }//onreciver
                        Text("\(self.sysReloadTime) seconds left")
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .multilineTextAlignment(.center)
                        .position(x: geometry.size.width/2, y: geometry.size.height/1.5 + 30)
                        .onReceive(Just(sysReloadTime)) { Result in
                            if self.sysReloadTimeFlag == false
                            {
                                NSLog("self.sysReloadTime Result: \(Result)")
                                self.sysReloadTimeFlag = true
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                    self.sysReloadTime -= 1
                                    self.sysReloadTimeFlag = false
                                    NSLog("self.sysReloadTime: \(self.sysReloadTime)")
                                }
                            }
                        }
                        
                    } // GeometryReader
                }
                .frame(width:width-200,height: 200,alignment: .center)
                .background(Color.white)
                
                
            }.popup(isPresented: $showWait, with: $showWait) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width-150,height: 200,alignment: .center)
            .position(x: x, y: y)
        }
    }
    func getNetworkInfo(compleationHandler: @escaping ([String: Any])->Void){

        
       var currentWirelessInfo: [String: Any] = [:]
        
        if #available(iOS 14.0, *) {
           
            NEHotspotNetwork.fetchCurrent { network in
                
                guard let network = network else {
                    compleationHandler([:])
                    return
                }
                
                let bssid = network.bssid
                let ssid = network.ssid

                //currentWirelessInfo = ["BSSID ": bssid, "SSID": ssid, "SSIDDATA": "<54656e64 615f3443 38354430>"]
                currentWirelessInfo = ["BSSID ": bssid, "SSID": ssid]
                compleationHandler(currentWirelessInfo)
            }
        }

    }
    func getIPAddress() -> String {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }

                guard let interface = ptr?.pointee else { return "" }
                let addrFamily = interface.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {

                    // wifi = ["en0"]
                    // wired = ["en2", "en3", "en4"]
                    // cellular = ["pdp_ip0","pdp_ip1","pdp_ip2","pdp_ip3"]

                    let name: String = String(cString: (interface.ifa_name))
                  //  if  name == "en0" || name == "en2" || name == "en3" || name == "en4" || name == "pdp_ip0" || name == "pdp_ip1" || name == "pdp_ip2" || name == "pdp_ip3" {
                    if  name == "en0" {
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface.ifa_addr, socklen_t((interface.ifa_addr.pointee.sa_len)), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address ?? ""
    }
    @ViewBuilder
    func showRebootSuccess(width:CGFloat)->some View {
        if self.RebootSuccess {
            ZStack {
                VStack(spacing:0){
            Text("This AP rebooted successfully")
                    .frame(width:width,height: 30)
                    .font(.InterRegular18)
                    .foregroundColor(Color.white)
                    .background(Color.c0x00851D)
                    .shadow(radius: 10)
                
            }
            }.popup(isPresented: $RebootSuccess, with: $RebootSuccess) { item in
               
            }
            .frame(width:width,height: 35)
            .position(x: width/2, y: 110)
            .onAppear {
             //   Detail.system[0].value = value
            //    Detail.system[1].value = value1
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.RebootSuccess = false
                }
            }
        }
        if self.RebootErr {
            ZStack {
                VStack(spacing:0){
            Text("This AP did not reboot succesfully, please try again")
                    .frame(width:width,height: 30)
                    .font(.InterRegular14)
                    .foregroundColor(Color.white)
                    .background(Color.c0xDA3E00)
                    .shadow(radius: 10)
                
            }
            }.popup(isPresented: $RebootErr, with: $RebootErr) { item in
               
            }
            .frame(width:width,height: 35)
            .position(x: width/2, y: 110)
            .onAppear {
             //   Detail.system[0].value = value
               // Detail.system[1].value = value1
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.RebootErr = false
                }
            }
        }
    }
    @ViewBuilder
    func showRadioDialog(tag:String,width:CGFloat,height:CGFloat,x:CGFloat,y:CGFloat,Content:String) -> some View {
        if self.showRadioAlert {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                
                VStack() {
                    GeometryReader { geometry in
                    Text(Content)
                        .multilineTextAlignment(.center)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .position(x: geometry.size.width/2, y: geometry.size.height/3)
                    
                    HStack()  {
                        Button{
                            let ssdp = SSDP(LOCATION: detail.location!, SERVER: "", ST: "", USN: "", Vendor: "", Mac: detail.mac!, Model:"", Name: detail.appname , OperatorMode: detail.central , Identity: "", FirmwareVersion: "", CPUUtilization: "", ConnectedUsers:"", SNR: "", MemoryUtilization: "", WiFiRadio: detail.wifiradio, live:"", SOPHOS_INFO: "")
                            let result = datas.saveToCurrentEable(detail.mac!, ssdp)
                            
                            if result
                            {
                                Detail.getOffWiFiResult = 0
                                Detail.OffWiFi(model: detail.model!)
                            }
                            self.check = true
                            
                        } label: {
                            Text("Yes")
                                .frame(minWidth: 0, maxWidth: 100, minHeight: 0, maxHeight: 40)
                                .font(.InterMedium14)
                                .foregroundColor(.white)
                                .background(Color.c0x005BC8)
                                .cornerRadius(5)
                        }
                        .accessibilityIdentifier("wifidisableok")
                        .frame(width: 100, height: 40)
                        .onReceive(Detail.$getOffWiFiResult) { result in
                                NSLog("OffWiFi Detail.getOffWiFiResult :\(result)")
                                var requests = 2
                                if support6G.contains(detail.model!)
                                {
                                    requests = 3
                                }
                                if result == requests && self.check == true
                                {
                                    self.showRadioAlert = false
                                    blurAmount = 0
//                                    self.disableRadio = true
//                                    self.Wireless24G = false
//                                    self.Wireless5G = false
//                                    self.Wireless6G = false
                                    
                                    Detail.getOffWiFiResult = 0
                                    self.check = false
                                    Detail.postsysReload()
                                  //  self.progressValue = 0.0
                                    self.showWIFIAlert = true
                                    self.blurAmount = 2
                                   // self.sysReloadflag = true
                                   // self.sysReloadTime = 0.002
                                    self.sysReloadTimeCout = false
                                    self.sysReloadTime = 0
                                }
                            }
                        
                        Spacer()
                        Button{
                            self.showRadioAlert = false
                            blurAmount = 0
                        } label: {
                            ZStack{
                            RoundedRectangle(cornerRadius: 5)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                            Text("No")
                                .font(.InterMedium14)
                                .foregroundColor(Color.c0x242527)
                            }.frame(width: 100, height: 40)
                        }
                      }//HStack
                    .position(x: geometry.size.width/2, y: geometry.size.height - 35)
                    } // GeometryReader
                }
                .frame(width:width-200,height: 200,alignment: .center)
                .background(Color.white)
                
                
            }.popup(isPresented: $showRadioAlert, with: $showRadioAlert) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width-150,height: 200,alignment: .center)
            .position(x: x, y: y)
        }
//        if self.showRadioONAlert {
//            ZStack(){
//
//                RoundedRectangle(cornerRadius: 5)
//                        .foregroundColor(.white)
//                        .shadow(radius: 10)
//
//                VStack() {
//                    GeometryReader { geometry in
//                    Text("You are connected to the AP you wish to disconnect \n\n  Do you wish to continue")
//                        .multilineTextAlignment(.center)
//                        .font(.InterRegular14)
//                        .foregroundColor(Color.c0x242527)
//                        .position(x: geometry.size.width/2, y: geometry.size.height/3)
//
//                    HStack()  {
//                        Button{
//                            let ssdp = datas.getCurrentEableMatch(detail.mac!)
//                            if ssdp.WiFiRadio != "" &&  ssdp.WiFiRadio != "0"
//                            {
//                                NSLog("OffWiFi :\(ssdp.WiFiRadio)")
//                                Detail.getOffWiFiResult = 0
//                                Detail.OnWiFi(wifiradoi:ssdp.WiFiRadio!,model: detail.model!)
//                                self.check = true
//
//                            }
//                            else
//                            {
//                                Detail.getOffWiFiResult = 0
//                                if support6G.contains(detail.model!){
//                                    Detail.OnWiFi(wifiradoi:"7",model: detail.model!)
//                                }
//                                else
//                                {
//                                    Detail.OnWiFi(wifiradoi:"3",model: detail.model!)
//                                }
//                                self.check = true
//                            }
//
//                        } label: {
//                            Text("Yes")
//                                .frame(minWidth: 0, maxWidth: 100, minHeight: 0, maxHeight: 40)
//                                .font(.InterMedium14)
//                                .foregroundColor(.white)
//                                .background(Color.c0x005BC8)
//                                .cornerRadius(5)
//                        }
//                        .frame(width: 100, height: 40)
//                        .onReceive(Detail.$getOffWiFiResult) { Result in
//                                NSLog("OffWiFi Detail.getOffWiFiResult :\(Result)")
//                                var requests = 2
//                                if support6G.contains(detail.model!)
//                                {
//                                    requests = 3
//                                }
//                                if Result == requests && self.check == true
//                                {
//                                    self.showRadioONAlert = false
//                                    blurAmount = 0
////                                    self.disableRadio = false
////                                    self.Wireless24G = true
////                                    self.Wireless5G = true
////                                    self.Wireless6G = true
//
//                                    Detail.getOffWiFiResult = 0
//                                    self.check = false
//
//                                    Detail.postsysReload()
//
//                                    self.showWIFIAlert = true
//                                    self.blurAmount = 2
//                                  //  self.sysReloadflag = true
//                                  //  self.sysReloadTime = 0.002
//                                    self.sysReloadTimeCout = false
//                                    self.sysReloadTime = 0
//                                }
//                            }
//                        .accessibilityIdentifier("wifienableok")
//
//                        Spacer()
//                        Button{
//                            self.showRadioONAlert = false
//                            blurAmount = 0
//                        } label: {
//                            ZStack{
//                            RoundedRectangle(cornerRadius: 5)
//                                    .stroke(Color.c0x696A6B, lineWidth: 1)
//                            Text("No")
//                                .font(.InterMedium14)
//                                .foregroundColor(Color.c0x242527)
//                            }.frame(width: 100, height: 40)
//                        }
//                      }//HStack
//                    .position(x: geometry.size.width/2, y: geometry.size.height - 35)
//                    } // GeometryReader
//                }
//                .frame(width:width-200,height: 200,alignment: .center)
//                .background(Color.white)
//
//
//            }.popup(isPresented: $showRadioONAlert, with: $showRadioONAlert) { item in
//
//            }
//            .background(
//                RoundedRectangle(cornerRadius: 5)
//                        .stroke(Color.c0x696A6B, lineWidth: 1)
//            )
//            .frame(width:width-150,height: 200,alignment: .center)
//            .position(x: x, y: y)
//        }
    }
    @ViewBuilder
    func showList(width:CGFloat,height:CGFloat)->some View {
        if self.showLANIPType {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                        
                ScrollView(.vertical) {
                    VStack(alignment:.leading) {
                      
                            ForEach(LANIPTypeArray, id: \.self) { i in
                                Spacer()
                                Button {
                                    self.LANIPType = i
                                    self.showLANIPType = false
                                    blurAmount = 0
                                } label: {
                                    Text(i)
                                        .frame(width: 100,height:40,alignment:.leading)
                                        .font(.InterRegular14)
                                        .foregroundColor(Color.c0x242527)
                                        .multilineTextAlignment(.leading)
                                }
                                .frame(width: 100,height:40,alignment:.leading)
                                .padding(.leading,10)
                                .accessibilityIdentifier("laniptypeone\(i)")
                                Divider()
                            }
                    }
                    .frame(width: 100,alignment: .center)
                    .background(Color.white)
                }
                .frame(width:width/4+20,alignment: .center)
                .onAppear{
                    blurAmount = 2
                    
                }
                
            }.popup(isPresented: $showLANIPType, with: $showLANIPType) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width/3,height: 120,alignment: .center)
            .position(x:width/2,y:height/2)
           
        }
        else if self.showLeaseTimeFlag {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                        
                ScrollView(.vertical) {
                    VStack(alignment:.leading) {
                      
                            ForEach(LeaseTimeArray, id: \.self) { i in
                                Spacer()
                                Button {
                                    self.showLeaseTimeFlag = false
                                    self.blurAmount = 0
                                    Detail.LANDHCPServer[3].value = i
                                } label: {
                                    Text(i)
                                        .frame(width: 100,height:40,alignment:.leading)
                                        .font(.InterRegular14)
                                        .foregroundColor(Color.c0x242527)
                                        .multilineTextAlignment(.leading)
                                    
                                }
                                .frame(width: 100,height:40,alignment:.leading)
                                .padding(.leading,10)
                                .accessibility(addTraits: .isButton)
                                .accessibilityIdentifier("leasetime0\(i)")
                                Divider()
                            }
                       
                    }
                    .frame(width: 100,alignment: .center)
                    .background(Color.white)
                }
                .frame(width:width/4+20,alignment: .center)
                .onAppear{
                    self.blurAmount = 2
                }
                         
            }.popup(isPresented: $showLeaseTimeFlag, with: $showLeaseTimeFlag) { item in
                
            }
            .frame(width:width/3,height: 200,alignment: .center)
            .position(x:width/2,y:height/2)
            
        }
        else if self.GateWayFromDHCPFlag {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                        
                ScrollView(.vertical) {
                    VStack(alignment:.leading) {
                      
                        ForEach(self.FromDHCPArray, id: \.self) { i in
                                Spacer()
                                Button {
                                    self.GateWayFromDHCP = i
                                    self.GateWayFromDHCPFlag = false
                                    self.blurAmount = 0
                                } label: {
                                    Text(i)
                                        .frame(width: 100,height:40,alignment:.leading)
                                        .font(.InterRegular14)
                                        .foregroundColor(Color.c0x242527)
                                        .multilineTextAlignment(.leading)
                                }
                                .frame(width: 100,height:40,alignment:.leading)
                                .padding(.leading,10)
                                .accessibility(addTraits: .isButton)
                                .accessibilityIdentifier("GateWayflag\(i)")
                                Divider()
                            }
                    }
                    .frame(width: 100,alignment: .center)
                    .background(Color.white)
                }
                .frame(width:width/4+20,alignment: .center)
                .onAppear{
                    self.blurAmount = 2
                }
                
            }.popup(isPresented: $GateWayFromDHCPFlag, with: $GateWayFromDHCPFlag) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width/3,height: 120,alignment: .center)
            .position(x:width/2,y:height/2)
            
        }
        else if self.PriAddrFromDHCPFlag {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                        
                ScrollView(.vertical) {
                    VStack(alignment:.leading) {
                      
                        ForEach(self.FromDHCPArray, id: \.self) { i in
                               
                                Button {
                                    self.PriAddrFromDHCP = i
                                    self.PriAddrFromDHCPFlag = false
                                    self.blurAmount = 0
                                } label: {
                                    Text(i)
                                        .frame(width: 100,height:40,alignment:.leading)
                                        .font(.InterRegular14)
                                        .foregroundColor(Color.c0x242527)
                                        .multilineTextAlignment(.leading)
                                }
                                .frame(width: 100,height:40,alignment:.leading)
                                .padding(.leading,10)
                                .accessibility(addTraits: .isButton)
                                .accessibilityIdentifier("PriAddrflag\(i)")
                                Divider()
                            }
                    }
                    .frame(width: 100,alignment: .center)
                    .background(Color.white)
                }
                .frame(width:width/4+20,alignment: .center)
                .onAppear{
                    self.blurAmount = 2
                }
                
            }.popup(isPresented: $PriAddrFromDHCPFlag, with: $PriAddrFromDHCPFlag) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width/3,height: 120,alignment: .center)
            .position(x:width/2,y:height/2)
            
        }
        else if self.SecAddrFromDHCPFlag {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                        
                ScrollView(.vertical) {
                    VStack(alignment:.leading) {
                      
                        ForEach(self.FromDHCPArray, id: \.self) { i in
                               
                                Button {
                                    self.SecAddrFromDHCP = i
                                    self.SecAddrFromDHCPFlag = false
                                    self.blurAmount = 0
                                } label: {
                                    Text(i)
                                        .frame(width: 100,height:40,alignment:.leading)
                                        .font(.InterRegular14)
                                        .foregroundColor(Color.c0x242527)
                                        .multilineTextAlignment(.leading)
                                }
                                .frame(width: 100,height:40,alignment:.leading)
                                .padding(.leading,10)
                                .accessibility(addTraits: .isButton)
                                .accessibilityIdentifier("SecAddrflag\(i)")
                                Divider()
                            }
                    }
                    .frame(width: 100,alignment: .center)
                    .background(Color.white)
                }
                .frame(width:width/4+20,alignment: .center)
                .onAppear{
                    self.blurAmount = 2
                }
                
            }.popup(isPresented: $SecAddrFromDHCPFlag, with: $SecAddrFromDHCPFlag) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width/3,height: 120,alignment: .center)
            .position(x:width/2,y:height/2)
            
        }
        else if self.showBandsTaggle24G || self.showBandsTaggle5G || self.showBandsTaggle6G {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                        
                ScrollView(.vertical) {
                    VStack() {
                      
                            ForEach(SSIDArray, id: \.self) { i in
                                Spacer()
                                Button {
                                    if self.showBandsTaggle24G
                                    {
                                        self.WiFiBands24G = i
//                                        Detail.wireless24G = []
//                                        var num = 0
//                                        while(num<Int(i)!)
//                                        {
//                                            Detail.updateWireless24G()
//                                            if (wirelessValue24G.count/4-1) < num {
//                                                wirelessValue24G.insert("Sophos AP6 420E CCDD10_2_\(num+1)", at: 4*num)
//                                                wirelessValue24G.insert("WPA3-AES", at: 4*num+1)
//                                                wirelessValue24G.insert("Edimax1234", at: 4*num+2)
//                                                wirelessValue24G.insert("1", at: 4*num+3)
//                                            }
//                                            num += 1
//                                        }
//
//                                        var count = 0
//                                        Detail.wireless24G.forEach { item in
//                                            Detail.wireless24G[count].value = wirelessValue24G[count]
//                                            count += 1
//                                        }
                                        
                                        self.showBandsTaggle24G.toggle()
                                    }
                                    else if self.showBandsTaggle5G
                                    {
                                        self.WiFiBands5G = i
                                       // Detail.wireless5G = []
//                                        var num = 0
//                                        while(num<Int(i)!)
//                                        {
//                                            Detail.updateWireless5G()
//                                            if (wirelessValue5G.count/4-1) < num {
//                                                wirelessValue5G.insert("Sophos AP6 420E CCDD10_5_\(num+1)", at: 4*num)
//                                                wirelessValue5G.insert("WPA3-AES", at: 4*num+1)
//                                                wirelessValue5G.insert("Edimax1234", at: 4*num+2)
//                                                wirelessValue5G.insert("1", at: 4*num+3)
//                                            }
//                                            num += 1
//                                        }
//
//                                        var count = 0
//                                        Detail.wireless5G.forEach { item in
//                                            if count >= Int(i)!
//                                            {
//                                                Detail.wireless5G[count].value = wirelessValue5G[count]
//
//                                            }
//                                            count += 1
//                                        }
                                        
                                        self.showBandsTaggle5G.toggle()
                                    }
                                    else if self.showBandsTaggle6G
                                    {
                                        self.WiFiBands6G = i
//                                        Detail.wireless6G = []
//                                        var num = 0
//                                        while(num<Int(i)!)
//                                        {
//                                            Detail.updateWireless6G()
//                                            if (wirelessValue6G.count/4-1) < num {
//                                                wirelessValue6G.insert("Sophos AP6 420E CCDD10_6_\(num+1)", at: 4*num)
//                                                wirelessValue6G.insert("WPA3-AES", at: 4*num+1)
//                                                wirelessValue6G.insert("Edimax1234", at: 4*num+2)
//                                                wirelessValue6G.insert("1", at: 4*num+3)
//                                            }
//                                            num += 1
//                                        }
//
//                                        var count = 0
//                                        Detail.wireless6G.forEach { item in
//                                            Detail.wireless6G[count].value = wirelessValue6G[count]
//                                            count += 1
//                                        }
                                        
                                        self.showBandsTaggle6G.toggle()
                                    }
                                    self.blurAmount = 0
                                } label: {
                                    Text(i)
                                        .frame(width: 100,height:40,alignment:.center)
                                        .font(.InterRegular14)
                                        .foregroundColor(Color.c0x242527)
                                    
                                }
                                .frame(width: 100,height:40,alignment:.center)
                                .accessibility(addTraits: .isButton)
                                .accessibilityIdentifier("SSID2GNUM\(i)")
                                Divider()
                            }
                       
                    }
                    .frame(width: 100,alignment: .center)
                    .background(Color.white)
                }
                .frame(width:width/4+20,alignment: .center)
                .onAppear{
                    self.blurAmount = 2
                }
                
            }.popup(isPresented: $showBandsTaggle24G, with: $showBandsTaggle24G) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width/3,height: 200,alignment: .center)
            .position(x:width/2,y:height/2)
            
            }
        else if self.showAuthType24GFlag || showAuthType5GFlag {
            //print("showAuthTypeFlag tapped")
            ZStack(){
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                        
                ScrollView(.vertical) {
                    VStack(alignment:.leading) {
                        
                        Text("Choose your Wireless Authentication method")
                            .font(.InterRegular14)
                            .foregroundColor(Color.c0x242527)
                        .frame(width: 280,alignment:.center)
                        .multilineTextAlignment(.center)
                        .padding(.top,10)
                        RadioButtonGroup(items: AuthArray, selectedId: AuthArray[AuthArraySelected]) { selected in
                            if self.showAuthType24GFlag
                            {
                                Detail.wireless24G[AuthROW].value = selected
                                self.showAuthType24GFlag = false
                            }
                            else if self.showAuthType5GFlag
                            {
                                Detail.wireless5G[AuthROW].value = selected
                                self.showAuthType5GFlag = false
                            }
                            else if self.showAuthType6GFlag
                            {
                                Detail.wireless6G[AuthROW].value = selected
                                self.showAuthType6GFlag = false
                            }
                            
                            self.blurAmount = 0
                        }
                        .padding(.leading,5)
                        .frame(width: 280,alignment: .leading)
                        
                    }
                    .frame(width: 280,alignment: .leading)
                }
                .frame(width:width*2/3+20,alignment: .leading)
                .onAppear{
                    self.blurAmount = 2
                }
            }.popup(isPresented: $showAuthType24GFlag, with: $showAuthType24GFlag) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width*2/3+10,height: 400,alignment: .center)
            .background(Color.white)
            .position(x:width/2,y:height/2)
            
       }
        else if showAuthType6GFlag {
            //print("showAuthTypeFlag tapped")
            ZStack(){
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                        
                ScrollView(.vertical) {
                    VStack(alignment:.leading) {
                        
                        Text("Choose your Wireless Authentication method")
                            .font(.InterRegular14)
                            .foregroundColor(Color.c0x242527)
                        .frame(width: 280,alignment:.center)
                        .multilineTextAlignment(.center)
                        .padding(.top,10)
                        RadioButtonGroup(items: Auth6GArray, selectedId: Auth6GArray[AuthArraySelected]) { selected in
                            
                                Detail.wireless6G[AuthROW].value = selected
                                self.showAuthType6GFlag = false
                           
                            self.blurAmount = 0
                        }
                        .padding(.leading,5)
                        .frame(width: 280,alignment: .leading)
                        
                    }
                    .frame(width: 280,alignment: .leading)
                }
                .frame(width:width*2/3+20,alignment: .leading)
                .onAppear{
                    self.blurAmount = 2
                }
            }.popup(isPresented: $showAuthType6GFlag, with: $showAuthType6GFlag) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width*2/3+10,height: 200,alignment: .center)
            .background(Color.white)
            .position(x:width/2,y:height/2)
            
       }
        else if self.showLocalTimeYear
                {
                    ZStack(){
                        RoundedRectangle(cornerRadius: 5)
                                .foregroundColor(.white)
                                .shadow(radius: 10)
                                
                        ScrollView(.vertical) {
                            VStack(alignment:.leading) {
                              
                                ForEach(["2017","2018","2019","2020","2021","2022","2023","2024","2025","2026","2027"], id: \.self) { i in
                                        Spacer()
                                        Button {
                                            self.showLocalTimeYear = false
                                            self.blurAmount = 0
                                            Detail.LocalTime[0].value = i
                                        } label: {
                                            Text(i)
                                                .frame(width: 100,height:40,alignment:.center)
                                                .font(.InterRegular14)
                                                .foregroundColor(Color.c0x242527)
                                            
                                        }
                                        .frame(width: 100,height:40,alignment:.center)
                                        .accessibilityIdentifier("LocalTimeYear\(i)")
                                        Divider()
                                    }
                               
                            }
                            .frame(width: 100,alignment: .center)
                            .background(Color.white)
                        }
                        .frame(width:width/4+20,alignment: .center)
                        .onAppear{
                            self.blurAmount = 2
                        }
                                 
                    }.popup(isPresented: $showLocalTimeYear, with: $showLocalTimeYear) { item in
                        
                    }
                    .frame(width:width/3,height: 200,alignment: .center)
                    .position(x:width/2,y:height/2)
                    
                }
        else if self.showLocalTimeMonth
                {
                    ZStack(){
                        RoundedRectangle(cornerRadius: 5)
                                .foregroundColor(.white)
                                .shadow(radius: 10)
                                
                        ScrollView(.vertical) {
                            VStack(alignment:.leading) {
                              
                                ForEach(["January","February","March","April","May","June","July","August","September","October","November","December"], id: \.self) { i in
                                        Spacer()
                                        Button {
                                            self.showLocalTimeMonth = false
                                            self.blurAmount = 0
                                            Detail.LocalTime[1].value = i
                                        } label: {
                                            Text(i)
                                                .frame(width: 100,height:40,alignment:.leading)
                                                .font(.InterRegular14)
                                                .foregroundColor(Color.c0x242527)
                                                .multilineTextAlignment(.leading)
                                            
                                        }
                                        .frame(width: 100,height:40,alignment:.leading)
                                        .padding(.leading,10)
                                        .accessibilityIdentifier("LocalTimeMonth\(i)")
                                        Divider()
                                    }
                               
                            }
                            .frame(width: 100,alignment: .center)
                            .background(Color.white)
                        }
                        .frame(width:width/4+20,alignment: .center)
                        .onAppear{
                            self.blurAmount = 2
                        }
                                 
                    }.popup(isPresented: $showLocalTimeMonth, with: $showLocalTimeMonth) { item in
                        
                    }
                    .background(
                        RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                    )
                    .frame(width:width/3,height: 200,alignment: .center)
                    .position(x:width/2,y:height/2)
                    
                }
                else if self.showLocalTimeDay
                {
                    ZStack(){
                        RoundedRectangle(cornerRadius: 5)
                                .foregroundColor(.white)
                                .shadow(radius: 10)
                                
                        ScrollView(.vertical) {
                            VStack(alignment:.leading) {
                              
                                ForEach(1..<32) { i in
                                        Spacer()
                                        Button {
                                            self.showLocalTimeDay = false
                                            self.blurAmount = 0
                                            Detail.LocalTime[2].value = String(i)
                                        } label: {
                                            Text("\(i)")
                                                .frame(width: 100,height:40,alignment:.center)
                                                .font(.InterRegular14)
                                                .foregroundColor(Color.c0x242527)
                                            
                                        }
                                        .frame(width: 100,height:40,alignment:.center)
                                        .accessibilityIdentifier("LocalTimeDay\(i)")
                                        Divider()
                                    }
                               
                            }
                            .frame(width: 100,alignment: .center)
                            .background(Color.white)
                        }
                        .frame(width:width/4+20,alignment: .center)
                        .onAppear{
                            self.blurAmount = 2
                        }
                                 
                    }.popup(isPresented: $showLocalTimeDay, with: $showLocalTimeDay) { item in
                        
                    }
                    .background(
                        RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                    )
                    .frame(width:width/3,height: 200,alignment: .center)
                    .position(x:width/2,y:height/2)
                    
                }
                else if self.showLocalTimeHours
                {
                    ZStack(){
                        RoundedRectangle(cornerRadius: 5)
                                .foregroundColor(.white)
                                .shadow(radius: 10)
                                
                        ScrollView(.vertical) {
                            VStack(alignment:.leading) {
                              
                                ForEach(1..<25) { i in
                                        Spacer()
                                        Button {
                                            self.showLocalTimeHours = false
                                            self.blurAmount = 0
                                            Detail.LocalTime[3].value = String(i)
                                        } label: {
                                            Text("\(i)")
                                                .frame(width: 100,height:40,alignment:.center)
                                                .font(.InterRegular14)
                                                .foregroundColor(Color.c0x242527)
                                            
                                        }
                                        .frame(width: 100,height:40,alignment:.center)
                                        .accessibilityIdentifier("LocalTimeHours\(i)")
                                        Divider()
                                    }
                               
                            }
                            .frame(width: 100,alignment: .center)
                            .background(Color.white)
                        }
                        .frame(width:width/4+20,alignment: .center)
                        .onAppear{
                            self.blurAmount = 2
                        }
                                 
                    }.popup(isPresented: $showLocalTimeHours, with: $showLocalTimeHours) { item in
                        
                    }
                    .background(
                        RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                    )
                    .frame(width:width/3,height: 200,alignment: .center)
                    .position(x:width/2,y:height/2)
                    
                }
                else if self.showLocalTimeMinutes
                {
                    ZStack(){
                        RoundedRectangle(cornerRadius: 5)
                                .foregroundColor(.white)
                                .shadow(radius: 10)
                                
                        ScrollView(.vertical) {
                            VStack(alignment:.leading) {
                              
                                ForEach(0..<61) { i in
                                        Spacer()
                                        Button {
                                            self.showLocalTimeMinutes = false
                                            self.blurAmount = 0
                                            Detail.LocalTime[4].value = String(i)
                                        } label: {
                                            Text("\(i)")
                                                .frame(width: 100,height:40,alignment:.center)
                                                .font(.InterRegular14)
                                                .foregroundColor(Color.c0x242527)
                                            
                                        }
                                        .frame(width: 100,height:40,alignment:.center)
                                        .accessibilityIdentifier("LocalTimeMinutes\(i)")
                                        Divider()
                                    }
                               
                            }
                            .frame(width: 100,alignment: .center)
                            .background(Color.white)
                        }
                        .frame(width:width/4+20,alignment: .center)
                        .onAppear{
                            self.blurAmount = 2
                        }
                                 
                    }.popup(isPresented: $showLocalTimeMinutes, with: $showLocalTimeMinutes) { item in
                        
                    }
                    .background(
                        RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                    )
                    .frame(width:width/3,height: 200,alignment: .center)
                    .position(x:width/2,y:height/2)
                    
                }
                else if self.showLocalTimeSeconds
                {
                    ZStack(){
                        RoundedRectangle(cornerRadius: 5)
                                .foregroundColor(.white)
                                .shadow(radius: 10)
                                
                        ScrollView(.vertical) {
                            VStack(alignment:.leading) {
                              
                                ForEach(0..<61) { i in
                                        Spacer()
                                        Button {
                                            self.showLocalTimeSeconds = false
                                            self.blurAmount = 0
                                            Detail.LocalTime[5].value = String(i)
                                        } label: {
                                            Text("\(i)")
                                                .frame(width: 100,height:40,alignment:.center)
                                                .font(.InterRegular14)
                                                .foregroundColor(Color.c0x242527)
                                            
                                        }
                                        .frame(width: 100,height:40,alignment:.center)
                                        .accessibilityIdentifier("LocalTimeSeconds\(i)")
                                        Divider()
                                    }
                               
                            }
                            .frame(width:100,alignment: .center)
                            .background(Color.white)
                        }
                        .frame(width:width/4+20,alignment: .center)
                        .onAppear{
                            self.blurAmount = 2
                        }
                                 
                    }.popup(isPresented: $showLocalTimeSeconds, with: $showLocalTimeSeconds) { item in
                        
                    }
                    .background(
                        RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                    )
                    .frame(width:width/3,height: 200,alignment: .center)
                    .position(x:width/2,y:height/2)
                    
                }
                else if self.showTimeZone
                {
                    ZStack(){
                        RoundedRectangle(cornerRadius: 5)
                                .foregroundColor(.white)
                                .shadow(radius: 10)
                                
                        ScrollView(.vertical) {
                            VStack(alignment:.leading) {
                              
                                ForEach(TimeZoneList, id: \.self) { i in
                                        Spacer()
                                        Button {
                                            self.showTimeZone = false
                                            self.blurAmount = 0
                                            self.timeZone = i
                                        } label: {
                                            MarqueeText(text:"\(i)")
                                                .frame(width: 280,height:40,alignment:.leading)
                                                .font(.InterRegular14)
                                                .foregroundColor(Color.c0x242527)
                                                .multilineTextAlignment(.leading)
                                            
                                        }
                                        .frame(width: 280,height:40,alignment:.leading)
                                        .padding(.leading,10)
                                        .accessibility(addTraits: .isButton)
                                        .accessibilityIdentifier("zone\(i)")
                                        Divider()
                                    }
                               
                            }
                            .frame(width: width*4/5,alignment: .center)
                            .background(Color.white)
                            
                        }
                        .frame(width:width*4/5+20,alignment: .center)
                        .onAppear{
                            self.blurAmount = 2
                        }
                                 
                    }.popup(isPresented: $showTimeZone, with: $showTimeZone) { item in
                        
                    }
                    .background(
                        RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                    )
                    .frame(width:width*4/5,height: 400,alignment: .center)
                    .position(x:width/2,y:height/2)
                    
                    
                }
        else if self.showNTPServerType
        {
            ZStack(){
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                        
                ScrollView(.vertical) {
                    VStack(alignment:.leading) {
                      // "0.pool.ntp.org","0.africa.pool.ntp.org","0.asia.pool.ntp.org","0.europe.pool.ntp.org","0.north-america.pool.ntp.org","0.oceania.pool.ntp.org","0.south-america.pool.ntp.org",""
                        ForEach(["Global","Africa","Asia","Europe","North America","Oceania","South America","User-Defined"], id: \.self) { i in
                                Spacer()
                                Button {
                                    self.showNTPServerType = false
                                    self.NTPServerType = i
                                    self.blurAmount = 0
                                    self.FromNTPServerlag = false
                                    if self.NTPServerType == "User-Defined"
                                    {
                                        self.FromNTPServerlag = true
                                        //self.FromNTPServer = "0.sophos.pool.ntp.org"
                                    }
                                    else if self.NTPServerType == "Global"
                                    {
                                        self.FromNTPServer = "0.sophos.pool.ntp.org"
                                    }
                                    else if self.NTPServerType == "Africa"
                                    {
                                        self.FromNTPServer = "0.africa.pool.ntp.org"
                                    }
                                    else if self.NTPServerType == "Asia"
                                    {
                                        self.FromNTPServer = "0.asia.pool.ntp.org"
                                    }
                                    else if self.NTPServerType == "Europe"
                                    {
                                        self.FromNTPServer = "0.europe.pool.ntp.org"
                                    }
                                    else if self.NTPServerType == "North America"
                                    {
                                        self.FromNTPServer = "0.north-america.pool.ntp.org"
                                    }
                                    else if self.NTPServerType == "Oceania"
                                    {
                                        self.FromNTPServer = "0.oceania.pool.ntp.org"
                                    }
                                    else if self.NTPServerType == "South America"
                                    {
                                        self.FromNTPServer = "0.south-america.pool.ntp.org"
                                    }
                                } label: {
                                    Text("\(i)")
                                        .frame(width: 100,height:40,alignment:.leading)
                                        .font(.InterRegular14)
                                        .foregroundColor(Color.c0x242527)
                                        .multilineTextAlignment(.leading)
                                    
                                }
                                .frame(width: 100,height:40,alignment:.leading)
                                .padding(.leading,10)
                                .accessibilityIdentifier("NTPServerType\(i)")
                                Divider()
                            }
                       
                    }
                    .frame(width:100,alignment: .center)
                    .background(Color.white)
                }
                .frame(width:width/4+20,alignment: .center)
                .onAppear{
                    self.blurAmount = 2
                }
                         
            }.popup(isPresented: $showNTPServerType, with: $showNTPServerType) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width/3,height: 200,alignment: .center)
            .position(x:width/2,y:height/2)
            
        }
        else if self.showwirelessClients
        {
            VStack() {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.c0x242527))
                    .scaleEffect(x: 2, y: 2, anchor: .center)
            }
            .frame(width:50.0,height: 50.0,alignment: .center)
            .background(Color.clear)
            .position(x:width/2,y:height/2)
            .onAppear
            {
                self.blurAmount = 2
            }
            
        }
        else if self.showWaitPingRequest
        {
            
            VStack() {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.c0x242527))
                    .scaleEffect(x: 2, y: 2, anchor: .center)
//                Text("\(self.sysReloadTime) seconds left")
//                .font(.InterMedium18)
//                .foregroundColor(Color.c0x242527)
//                .multilineTextAlignment(.center)
//                .padding(.top,10)
//                .onReceive(Just(sysReloadTime)) { Result in
//                    if self.sysReloadTimeFlag == false
//                    {
//                        NSLog("self.sysReloadTime Result: \(Result)")
//                        self.sysReloadTimeFlag = true
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//                            self.sysReloadTime -= 1
//                            self.sysReloadTimeFlag = false
//                            NSLog("self.sysReloadTime: \(self.sysReloadTime)")
//                        }
//                    }
//                }
            }
            .frame(width:50.0,height: 50.0,alignment: .center)
            .background(Color.clear)
            .position(x:width/2,y:height/2)
            .onAppear
            {
                self.blurAmount = 2
            }
//            .onReceive($showWaitPingRequest) { result in
//                if result == false {
//                    
//                    self.blurAmount = 0
//                    self.showWaitPingRequest = false
//                }
//            }
           
        }
        else if self.showWaitRequest
        {
   //         ZStack(){
            VStack() {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.c0x242527))
                    .scaleEffect(x: 2, y: 2, anchor: .center)
                if editFlag && self.sysReloadTime != 0 {
                    Text("\(self.sysReloadTime) seconds left")
                    .font(.InterMedium18)
                    .foregroundColor(Color.c0x242527)
                    .multilineTextAlignment(.center)
                    .padding(.top,30)
                    .onReceive(Just(sysReloadTime)) { Result in
                        if self.sysReloadTimeFlag == false
                        {
                            NSLog("self.sysReloadTime Result: \(Result)")
                            self.sysReloadTimeFlag = true
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                self.sysReloadTime -= 1
                                self.sysReloadTimeFlag = false
                                NSLog("self.sysReloadTime: \(self.sysReloadTime)")
                                
                            }
                        }
                    }

                }
                
            }
            .frame(width:width-40,height: 200.0,alignment: .center)
            .background(Color.clear)
            .position(x:width/2,y:height/2)
            .onAppear
            {
                self.blurAmount = 2
            }
//            }.popup(isPresented: $showWaitRequest, with: $showWaitRequest) { item in
//                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//                    self.blurAmount = 0.0
//                    self.showWaitRequest = false
//                }
//            }
        }
        else if self.showWIFIAlert
        {
            VStack() {
//                Text("Configuration is complete. Reloading now...\nPlease wait for \(delay) seconds.")
//                    .font(.InterMedium14)
//                    .foregroundColor(Color.c0x242527)
                
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.c0x242527))
                    .scaleEffect(x: 2, y: 2, anchor: .center)
                Text("\(self.sysReloadTime) seconds left")
                .font(.InterMedium18)
                .foregroundColor(Color.c0x242527)
                .multilineTextAlignment(.center)
                .padding(.top,30)
                .onReceive(Just(sysReloadTime)) { Result in
                    if self.sysReloadTimeFlag == false
                    {
                        NSLog("self.sysReloadTime Result: \(Result)")
                        self.sysReloadTimeFlag = true
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            self.sysReloadTime -= 1
                            self.sysReloadTimeFlag = false
                            NSLog("self.sysReloadTime: \(self.sysReloadTime)")
                        }
                    }
                }
                
            }
            .frame(width:width-40,height: 200.0,alignment: .center)
            .background(Color.clear)
            .position(x:width/2,y:height/2)
            .onAppear
            {
                self.blurAmount = 2.0
            }
            .onReceive(Detail.$postsysReloadResult) { result in
                NSLog("sysReload : \(result)")
                if result?.error_code == 0
                {
                    NSLog("sysReload : \(result?.data?.reload_time)")
                    let delay = Double((result?.data?.reload_time)!)!
                    NSLog("sysReload :\(delay) \(result?.data?.reload_time)")
                    if sysReloadTimeCout == false
                    {
                        self.sysReloadTime = Int(delay)
                        sysReloadTimeCout = true
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + delay ) {
                        if Detail.postsysReloadResult != nil {
                            self.blurAmount = 0
                            self.showWIFIAlert = false
                            Detail.postsysReloadResult = nil
                            self.save = true
                        }

                    }
                  }
            }
        }
        else if self.showWaitRadio
        {
            VStack() {
//                Text("Configuration is complete. Reloading now...\nPlease wait for \(delay) seconds.")
//                    .font(.InterMedium14)
//                    .foregroundColor(Color.c0x242527)
                
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.c0x242527))
                    .scaleEffect(x: 2, y: 2, anchor: .center)
            }
            .frame(width:width-40,height: 200.0,alignment: .center)
            .background(Color.clear)
            .position(x:width/2,y:height/2)
            .onAppear
            {
                self.blurAmount = 2.0
            }
//            .onReceive(Detail.$postsysReloadResult) { result in
//                NSLog("sysReload : \(result)")
//                if result?.error_code == 0
//                {
//                    NSLog("sysReload : \(result?.data?.reload_time)")
//                    delay = Double((result?.data?.reload_time)!)!
//                    NSLog("sysReload :\(delay) \(result?.data?.reload_time)")
//                    DispatchQueue.main.asyncAfter(deadline: .now() + delay ) {
//                        if Detail.postsysReloadResult != nil {
//                            self.blurAmount = 0
//                            self.showWaitRadio = false
//                            Detail.postsysReloadResult = nil
//                            self.save = true
//                        }
//
//                    }
//                  }
//            }
        }//else if self.showWaitRadio
        else if self.pskAlert {
            ZStack {
                VStack(spacing:0){
            Text("The pre-shared key should contain at least 8 alphanumeric characters")
                    .frame(width:width,height: 50, alignment: .center)
                    .font(.InterRegular14)
                    .foregroundColor(Color.white)
                    .background(Color.c0xDA3E00)
                    .shadow(radius: 10)
                    .multilineTextAlignment(.center)
                    .lineLimit(2)
                
            }
            }.popup(isPresented: $pskAlert, with: $pskAlert) { item in
               
            }
            .frame(width:width,height: 35)
            .position(x: width/2, y: 110)
            .onAppear {
             //   Detail.system[0].value = value
               // Detail.system[1].value = value1
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.pskAlert = false
                }
            }
        }
        else if self.SSIDAlert {
            ZStack {
                VStack(spacing:0){
            Text("The SSID should contain at least 1 character")
                    .frame(width:width,height: 50, alignment: .center)
                    .font(.InterRegular14)
                    .foregroundColor(Color.white)
                    .background(Color.c0xDA3E00)
                    .shadow(radius: 10)
                    .multilineTextAlignment(.center)
                    .lineLimit(2)
                
            }
            }.popup(isPresented: $SSIDAlert, with: $SSIDAlert) { item in
               
            }
            .frame(width:width,height: 35)
            .position(x: width/2, y: 110)
            .onAppear {
             //   Detail.system[0].value = value
               // Detail.system[1].value = value1
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.SSIDAlert = false
                }
            }
        }
        else if self.requestDate
        {
   //         ZStack(){
            VStack() {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.c0x242527))
                    .scaleEffect(x: 2, y: 2, anchor: .center)
               
                
            }
            .frame(width:width-40,height: 200.0,alignment: .center)
            .background(Color.clear)
            .position(x:width/2,y:height/2)
            .onAppear
            {
                self.blurAmount = 2
            }
//            }.popup(isPresented: $showWaitRequest, with: $showWaitRequest) { item in
//                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//                    self.blurAmount = 0.0
//                    self.showWaitRequest = false
//                }
//            }
        }
    }
}

