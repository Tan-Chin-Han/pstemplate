//
//  AppTemplateModel.swift
//  PSTemplate
//
//  Created by Edimax on 2022/1/20.
//

import SwiftUI
import Combine

class AppTemplateModel:ObservableObject {
    @Published var MyAccess = [MyAccessBody]()
    @Published var NewAccess = [NewAccessBody]()
    @Published var newCount:Bool = false
    @Published var myCount:Bool = false
    
    var APAddress = ""
    var DigestAccount = ""
    var DigestPw = ""
    
    @ObservedObject var request: PSNetwork = PSNetwork()
    var anyCancellable = Set<AnyCancellable>()
    private var responses = 0
    @Published var response = 0
    {
        willSet {
            self.objectWillChange.send()
        }
    }
    
    @Published var err = ""
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var showErr = false
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    
    @Published var getSystemResult:GETresponse?
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    init() {
       request.objectWillChange.sink { [weak self] (value) in

            guard let self = self else  {
                return
            }
            self.getSystemResult = self.request.t as? GETresponse
            self.objectWillChange.send()

       }
       .store(in: &anyCancellable)
        
    }
    /*System*/
    func getSystem(){
        
        request.url = "https://\(APAddress)/ap/info/system/basic"
        request.method = "GET"
        request.DigestAccount = DigestAccount
        request.DigestPw = DigestPw
        
        request.getResult(model: GETresponse.self) { result in
            print(result)
            if result == 200 {
                self.responses = result
                self.response = self.responses
                let resultBody = self.getSystemResult
                
            }
            else {
                if result == 400 {
                    self.showErr = true
                    self.err = "Invaild Respone Formate"
                    self.responses = result
                    self.response = self.responses
                    self.getSystemResult = GETresponse(error_code:  self.response , error_msg: self.err, data: nil)
                }
                else
                {
                    self.showErr = true
                    self.err = "Invalid Username/Password"
                    self.responses = result
                    self.response = self.responses
                    self.getSystemResult = GETresponse(error_code:  self.response , error_msg: self.err, data: nil)
                }
            }
        }
        
        //return (self.response==200 ? true:false)
    }
    func update() {
       
        if MyAccess.count == 0 {
            myCount = false
        }
        else
        {
            myCount = true
        }
        if NewAccess.count == 0 {
            newCount = false
        }
        else
        {
            newCount = true
        }
    }
    func updateProgress(){
        let fake = NewAccessBody(appname: "Simulate WiFi Station-AP Name 1", mac: "00:00:1e:00:01:af", health: "Good",client: "20", location: "192.168.0.25:10443",central: "1",wifiradio: "2",model: "AP6 420E",unknow: 4)
        NewAccess.append(fake)
        let fake0 = NewAccessBody(appname: "Simulate WiFi Station-App Name 2", mac: "00:00:2e:00:02:af", health: "Poor",client: "40", location: "192.168.0.25:10443",central: "0",wifiradio: "3",model: "AP6 420x",unknow: 4)
        NewAccess.append(fake0)
//        let fake1 = NewAccessBody(appname: "App Name 3", mac: "00:00:3e:00:03:af", health: "Good",client: "10", location: "192.168.0.25:10443",central: "0",wifiradio: "3",model: "AP6 420E",unknow: 7)
//        NewAccess.append(fake1)
//
//        let fake3 = NewAccessBody(appname: "App Name 4", mac: "00:00:4e:00:04:af", health: "Good",client: "", location: "192.168.0.25:10443",central: "0",wifiradio: "3",model: "AP6 420E",unknow: 7)
//        NewAccess.append(fake3)
//        let fake4 = NewAccessBody(appname: "App Name 5", mac: "00:00:5e:00:05:af", health: "Poor",client: "70", location: "192.168.0.25:10443",central: "0",wifiradio: "3",model: "AP6 420E",unknow: 7)
//        NewAccess.append(fake4)
//        let fake5 = NewAccessBody(appname: "App Name 6", mac: "00:00:5e:00:06:af", health: "Poor",client: "70", location: "192.168.0.25:10443",central: "0",wifiradio: "3",model: "AP6 420E",unknow: 7)
//        NewAccess.append(fake5)
        if MyAccess.count >= 1 {
            return
        }
        if NewAccess.count == 0 {
            newCount = false
        }
        else
        {
            newCount = true
        }
    }
    func updateMy(mac:String){
        if !NewAccess.isEmpty {
        let fake = NewAccess.filter { $0.mac!.contains(mac) }[0]
        MyAccess.append(MyAccessBody(appname: fake.appname , mac: fake.mac, health: fake.health,client: fake.client,location: fake.location,central: fake.central,wifiradio: fake.wifiradio,model: fake.model,unknow: fake.unknow))
        if let idx = NewAccess.firstIndex(where: { $0.mac! == fake.mac }) {
            NewAccess.remove(at: idx)
        }

        
        if MyAccess.count == 0 {
            myCount = false
        }
        else
        {
            myCount = true
        }
        if NewAccess.count == 0 {
            newCount = false
        }
        else
        {
            newCount = true
        }
        }
    }
    func updateMyAll()
    {
        NewAccess.forEach { point in
            MyAccess.append(MyAccessBody(appname: point.appname , mac: point.mac, health: point.health,client: point.client,location: point.location,central: point.central,wifiradio: point.wifiradio,model: point.model,unknow: point.unknow))
            
            let ssdp = SSDP(LOCATION: point.location!, SERVER: "", ST: "", USN: "", Vendor: "", Mac: point.mac!, Model:"", Name: point.appname , OperatorMode: point.central , Identity: "", FirmwareVersion: "", CPUUtilization: "", ConnectedUsers:"", SNR: "", MemoryUtilization: "", WiFiRadio: "", live:"", SOPHOS_INFO: "")
            let status =  ManageDatas().saveToMyAP(point.mac!,ssdp)
            print("save Myapp status: \(status)")
        }
        NewAccess.removeAll()
        
        if MyAccess.count == 0 {
            myCount = false
        }
        else
        {
            myCount = true
        }
        if NewAccess.count == 0 {
            newCount = false
        }
        else
        {
            newCount = true
        }
    }
}
