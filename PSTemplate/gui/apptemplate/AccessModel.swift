//
//  AppTemplateModel.swift
//  PSTemplate
//
//  Created by Edimax on 2022/1/20.
//

import SwiftUI

struct MyAccessBody: Encodable, Hashable {
    var appname:String?
    var mac:String?
    var health:String?
    var client:String?
    var location:String?
    var central:String?
    var wifiradio:String?
    var model:String?
    var unknow:Int?
    
    init(appname: String?, mac: String?,health:String?,client:String?,location:String?,central:String?,wifiradio:String?,model:String?,unknow:Int?) {
        self.appname = appname
        self.mac = mac
        self.health = health
        self.client = client
        self.location = location
        self.central = central
        self.wifiradio = wifiradio
        self.model = model
        self.unknow = unknow
    }
}
struct MyAccessResponse: Decodable {
    enum CodingKeys: String, CodingKey {
            case appname, mac, id , health,client,location,central   }
    let appname:String?
    let mac:String?
    let health:String?
    let client:String?
    let id:String?
    let location:String?
    let central:String?
    
    init(appname: String?, mac: String?,health:String?,client:String?,id: String?,location:String?,central:String?) {
        self.appname = appname
        self.mac = mac
        self.health = health
        self.client = client
        self.id = id
        self.location = location
        self.central = central
    }
}
struct NewAccessBody: Encodable, Hashable {
    let appname:String?
    let mac:String?
    let health:String?
    let client:String?
    let location:String?
    let central:String?
    var wifiradio:String?
    let model:String?
    var unknow:Int?
    
    init(appname: String?, mac: String?,health:String?,client:String?,location:String?,central:String?,wifiradio:String?,model:String?,unknow:Int?) {
        self.appname = appname
        self.mac = mac
        self.health = health
        self.client = client
        self.location = location
        self.central = central
        self.wifiradio = wifiradio
        self.model = model
        self.unknow = unknow
    }
}
struct NewAccessResponse: Decodable {
    enum CodingKeys: String, CodingKey {
            case appname, mac, id , health ,client  }
    let appname:String?
    let mac:String?
    let health:String?
    let client:String?
    let id:String?
    
    init(appname: String?, mac: String?,health:String?,client:String?,id: String?) {
        self.appname = appname
        self.mac = mac
        self.health = health
        self.client = client
        self.id = id
    }
}
struct login: Codable, Hashable  {
   // enum CodingKeys: String, CodingKey {
   //          case admin_account, admin_pw, keep }
    let admin_account:String?
    let admin_pw:String?
    let keep:Int?
    
    init(admin_account: String?, admin_pw: String?,keep:Int?) {
        self.admin_account = admin_account
        self.admin_pw = admin_pw
        self.keep = keep
    }
}

