//
//  ManageViews.swift
//  PSTemplate
//
//  Created by Edimax on 2021/12/24.
//

import SwiftUI
import Combine
import Foundation

final class ManageViews: ObservableObject {
    @Published var page = 0 {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var name = "" {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var newFlag = false {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var myFlag = false {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var newAccess = [NewAccessBody]() {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var myAccess = [MyAccessBody]() {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var accessDetail = MyAccessBody(appname: "", mac: "", health: "", client: "",location: "",central: "",wifiradio: "",model: "",unknow: 7 ) {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var APaccount = "" {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var APpw = "" {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var APaddress = "" {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var getLoginResult:GETresponse?
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var value = "" {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var value1 = "" {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var LANType: [Bool] = [] {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var myWireless = [WirelessBody]() {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var wirelessType: [Bool] = [] {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var Central = false
    {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var showAboutFlag = false {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var showSettingFlag = false {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var hideMenu = false {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var RebootFlag = false {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var SSDPDiscvoer : [String:String] = ["":""] {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var SSDPSearchAlert = false {
        willSet {
            self.objectWillChange.send()
        }
    }
    
    var anyCancellable = Set<AnyCancellable>()
    //init() {}
    static let shared = ManageViews()
    private init() { }
}
extension Color {
    init(hex: Int, opacity: Double = 1.0) {
        let red = Double((hex & 0xff0000) >> 16) / 255.0
        let green = Double((hex & 0xff00) >> 8) / 255.0
        let blue = Double((hex & 0xff) >> 0) / 255.0
        self.init(.sRGB, red: red, green: green, blue: blue, opacity: opacity)
    }
    static var healthExcellent: Color{
        let hex = 0x00851D //0x2BA50D
        return Color(hex: hex)
    }
    static var healthGood: Color{
        let hex = 0xFFAF11 //0xF6A122
        return Color(hex: hex)
    }
    static var healthPoor: Color{
        let hex = 0xDA3E00 //0xF62F22
        return Color(hex: hex)
    }
    static var c0x005CC8:Color { //0x1987CB WiFi mobile APP
        let hex = 0x005CC8
        return Color(hex: hex)
    }
    static var c0x005BC8:Color { //0x1987CB WiFi mobile APP
        let hex = 0x005BC8
        return Color(hex: hex)
    }
    static var c0x001A47:Color {
        let hex = 0x001A47
        return Color(hex: hex)
    }
    static var c0x242527:Color {
        let hex = 0x242527
        return Color(hex: hex)
    }
    static var c0x242527_30:Color {
        let hex = 0x242527
        return Color(hex: hex,opacity: 0.3)
    }
    static var c0x696A6B:Color {
        let hex = 0x696A6B
        return Color(hex: hex)
    }
    static var c0xCED1D5:Color {
        let hex = 0xCED1D5
        return Color(hex: hex)
    }
    static var c0x00851D:Color {
        let hex = 0x00851D
        return Color(hex: hex)
    }
    static var c0xDA3E00:Color {
        let hex = 0xDA3E00
        return Color(hex: hex)
    }
    static var c0xFFFFFF_10:Color {
        let hex = 0xFFFFFF
        return Color(hex: hex,opacity: 0.05)
    }
}
extension Font {
   // "Inter-Regular", "Inter-Medium", "Inter-SemiBold","Inter-Bold"
    static var InterRegular36: Font {
        Font.custom("Inter-Regular", fixedSize: 36.0)
    }
    static var InterRegular24: Font {
        Font.custom("Inter-Regular", fixedSize: 24.0)
    }
    static var InterRegular18: Font {
        Font.custom("Inter-Regular", fixedSize: 18.0)
    }
    static var InterRegular16: Font {
        Font.custom("Inter-Regular", fixedSize: 16.0)
    }
    static var InterRegular14: Font {
        Font.custom("Inter-Regular", fixedSize: 14.0)
    }
    static var InterRegular13: Font {
        Font.custom("Inter-Regular", fixedSize: 13.0)
    }
    static var InterRegular12: Font {
        Font.custom("Inter-Regular", fixedSize: 12.0)
    }
    static var InterRegular10: Font {
        Font.custom("Inter-Regular", fixedSize: 10.0)
    }
    static var InterMedium36: Font {
        Font.custom("Inter-Medium", fixedSize: 36.0)
    }
    static var InterMedium24: Font {
        Font.custom("Inter-Medium", fixedSize: 24.0)
    }
    static var InterMedium18: Font {
        Font.custom("Inter-Medium", fixedSize: 18.0)
    }
    static var InterMedium14: Font {
        Font.custom("Inter-Medium", fixedSize: 14.0)
    }
    static var InterMedium12: Font {
        Font.custom("Inter-Medium", fixedSize: 12.0)
    }
    static var InterMedium10: Font {
        Font.custom("Inter-Medium", fixedSize: 10.0)
    }
    static var InterSemiBold36: Font {
        Font.custom("Inter-SemiBold", fixedSize: 36.0)
    }
    static var InterSemiBold24: Font {
        Font.custom("Inter-SemiBold", fixedSize: 24.0)
    }
    static var InterSemiBold18: Font {
        Font.custom("Inter-SemiBold", fixedSize: 18.0)
    }
    static var InterSemiBold14: Font {
        Font.custom("Inter-SemiBold", fixedSize: 14.0)
    }
    static var InterSemiBold12: Font {
        Font.custom("Inter-SemiBold", fixedSize: 12.0)
    }
    static var InterSemiBold10: Font {
        Font.custom("Inter-SemiBold", fixedSize: 10.0)
    }
    static var InterBold36: Font {
        Font.custom("Inter-Bold", fixedSize: 36.0)
    }
    static var InterBold24: Font {
        Font.custom("Inter-Bold", fixedSize: 24.0)
    }
    static var InterBold18: Font {
        Font.custom("Inter-Bold", fixedSize: 18.0)
    }
    static var InterBold14: Font {
        Font.custom("Inter-Bold", fixedSize: 14.0)
    }
    static var InterBold12: Font {
        Font.custom("Inter-Bold", fixedSize: 12.0)
    }
    static var InterBold10: Font {
        Font.custom("Inter-Bold", fixedSize: 10.0)
    }

    
}
struct SearchTextFieldStyle: TextFieldStyle {
    @Binding var focused: Bool
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
        .padding(10)
        .foregroundColor(Color.c0x242527) //0xCDCED0
        .background(
            RoundedRectangle(cornerRadius: 5, style: .continuous)
                .stroke(focused ? Color(hex: 0x3F85C5) : Color.c0x696A6B, lineWidth: 1)
                //.stroke(Color.black.opacity(0.4), lineWidth: 1)
               // .stroke(focused ? Color(hex: 0x3F85C5) : Color(hex: 0xC2C6CA), lineWidth: 1)
                
        ).padding()
        
    }
}
struct ClearButton: ViewModifier
{
    @Binding var text: String

    public func body(content: Content) -> some View
    {
        ZStack(alignment: .trailing)
        {
            content

            if !text.isEmpty
            {
                Button(action:
                {
                    self.text = ""
                })
                {
                    Image("searchX")
                        .resizable()
                        .frame(width:14,height: 14)
                        .foregroundColor(Color(UIColor.opaqueSeparator))
                }
                .padding(.trailing, 30)
                .accessibilityIdentifier("searchX")
            }
            else
            {
                Button(action:
                {
                    self.text = ""
                })
                {
                    Image("search")
                        .resizable()
                        .frame(width:25,height:25)
                        .foregroundColor(Color(UIColor.opaqueSeparator))
                }
                .padding(.trailing, 30)
                .accessibilityIdentifier("search")
            }
        }
    }
}
struct MyTableTextFieldStyle: TextFieldStyle {
    @Binding var lastHoveredId: String
    var id: String
    var width: CGFloat
    var height: CGFloat
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
            .frame(maxWidth: .infinity,maxHeight: 40)
            .padding(10)
            .foregroundColor(self.lastHoveredId==id ? Color.c0x242527: Color.c0x242527)
        .background(
            RoundedRectangle(cornerRadius: 3, style: .continuous)
                //.stroke(Color.black.opacity(0.4), lineWidth: 1)
                .stroke(self.lastHoveredId==id ? Color.c0x005BC8 : Color.c0x696A6B, lineWidth: 1)
                
        )
    }
}
struct MyWiBandsTableTextFieldStyle: TextFieldStyle {
    @Binding var lastHoveredId: String
    var id: String
    var width: CGFloat
    var height: CGFloat
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
        .frame(width: width,height: height)
        .foregroundColor(self.lastHoveredId==id ? Color.c0x242527: Color.c0x242527)
        .padding(.leading,10)
        .background(
            
            RoundedRectangle(cornerRadius: 5, style: .continuous)
                //.stroke(Color.black.opacity(0.4), lineWidth: 1)
                .stroke(self.lastHoveredId==id ? Color.c0x005BC8 : Color.c0x696A6B, lineWidth: 1)
                .padding(.trailing,3)
        )
        
    }
}
struct MyNewTableTextFieldStyle: TextFieldStyle {
    @Binding var lastHoveredId: String
    var id: String
    var width: CGFloat
    var height: CGFloat
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
        .frame(width: width,height: height)
        .foregroundColor(self.lastHoveredId==id ? Color.c0x242527: Color.c0x242527)
        .padding(.leading,10)
        .background(
            RoundedRectangle(cornerRadius: 3, style: .continuous)
                //.stroke(Color.black.opacity(0.4), lineWidth: 1)
                .stroke(self.lastHoveredId==id ? Color.c0x005BC8 : Color.c0x696A6B, lineWidth: 1)
                .padding(.leading,-10)
               // .padding(.trailing,10)
        )
    }
}
struct LoginTextFieldStyle: TextFieldStyle {
    @Binding var lastHoveredId: String
    var id: String
    var width: CGFloat
    var height: CGFloat
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
        .frame(width: width,height: height)
        .foregroundColor(self.lastHoveredId==id ? Color(hex: 0x000000): Color.c0x696A6B)
        //.padding(.leading,10)
        .background(
            RoundedRectangle(cornerRadius: 5, style: .continuous)
                //.stroke(Color.black.opacity(0.4), lineWidth: 1)
                .stroke(self.lastHoveredId==id ? Color.c0x005BC8 : Color.c0x696A6B, lineWidth: 1)
                .padding(.leading,-10)
               // .padding(.trailing,10)
        )
    }
}
struct Arrow: Shape {
    // 1.
    func path(in rect: CGRect) -> Path {
        Path { path in
            let width = rect.width
            let height = rect.height
            
            // 2.
            path.addLines( [
                CGPoint(x: width * 0.82, y: height * 0.4),
                CGPoint(x: width * 0.9, y: height * 0.5),
                CGPoint(x: width * 0.82, y: height * 0.6),
                
            ])
            
            // 3.
            //path.closeSubpath()
        }
        
    }
}

struct New: Shape {
    // 1.
    func path(in rect: CGRect) -> Path {
        Path { path in
            let width = rect.width
            let height = rect.height
            
            // 2.
            path.addLines( [
                CGPoint(x: width * 0.63, y: height * 0.5),
                CGPoint(x: width * 0.77, y: height * 0.5),
                
            ])
            path.addLines( [
                CGPoint(x: width * 0.7, y: height * 0.4),
                CGPoint(x: width * 0.7, y: height * 0.6),
                
            ])
            // 3.
            //path.closeSubpath()
        }
    }
}
struct GradientProgressStyle<Stroke: ShapeStyle, Background: ShapeStyle>: ProgressViewStyle {
    var stroke: Stroke
    var fill: Background
    var caption: String = ""
    var cornerRadius: CGFloat = 3
    var height: CGFloat = 16
    var animation: Animation = .easeInOut
    
    func makeBody(configuration: Configuration) -> some View {
        let fractionCompleted = configuration.fractionCompleted ?? 0
        
        return VStack {
            ZStack(alignment: .topLeading) {
                GeometryReader { geo in
                    Rectangle()
                        .fill(fill)
                        .frame(maxWidth: geo.size.width * CGFloat(fractionCompleted))
                        .animation(animation)
                }
            }
            .frame(height: height)
            .cornerRadius(cornerRadius)
            .overlay(
                RoundedRectangle(cornerRadius: cornerRadius)
                        .stroke(stroke, lineWidth: 0)
            )
            
            if !caption.isEmpty {
                Text("\(caption)")
                    .font(.caption)
                    .cornerRadius(cornerRadius)
                    .background(Color.black.opacity(0.2))
            }
        }
    }
}

struct ProgressBar: View {
    @Binding var progress: Float
    
    var body: some View {
        ZStack {
            Circle()
                .stroke(lineWidth: 5.0)
                .opacity(0.3)
                .foregroundColor(Color.gray)
            
            Circle()
                .trim(from: 0.0, to: CGFloat(min(self.progress, 1.0)))
                .stroke(style: StrokeStyle(lineWidth: 5.0, lineCap: .round, lineJoin: .round))
                .foregroundColor(Color.gray)
                .rotationEffect(Angle(degrees: 270.0))
                .animation(.spring())
//            Text(String(format: "%.0f %%", min(self.progress, 1.0)*100.0))
//                .font(.largeTitle)
//                .bold()
        }
    }
}
struct CustomButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding(15) // *make it thinner*
            .frame(maxWidth: .infinity) // expand horizontally
            .foregroundColor(.purple)
            .background(
                Rectangle()
                    .stroke(Color.purple, lineWidth: 1)
                    .background(Color.c0x005BC8) // *change the button background color to black*
                    .cornerRadius(3)
            )
            //.padding(.horizontal, 5) // *margin of 10px on each side*
            .padding([.leading,.trailing],50)
            .opacity(configuration.isPressed ? 0.7 : 1)
    }
}

struct RoundedCorner: Shape {

    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners

    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}

/* scrollOnOverflow */
struct OverflowContentViewModifier: ViewModifier {
    @State private var contentOverflow: Bool = false
    
    func body(content: Content) -> some View {
        GeometryReader { geometry in
            content
            .background(
                GeometryReader { contentGeometry in
                    Color.clear.onAppear {
                        contentOverflow = contentGeometry.size.height > geometry.size.height
                    }
                }
            )
            .wrappedInScrollView(when: contentOverflow)
        }
    }
}

extension View {
    @ViewBuilder
    func wrappedInScrollView(when condition: Bool) -> some View {
        if condition {
            ScrollView {
                self
            }
        } else {
            self
        }
    }
}
extension View {
    func scrollOnOverflow() -> some View {
        modifier(OverflowContentViewModifier())
    }
}
struct GridStack<Content: View>: View {
    let rows: Int
    let columns: Int
    let content: (Int, Int) -> Content

    var body: some View {
        VStack(spacing:0) {
            ForEach(0 ..< rows, id: \.self) { row in
                HStack(spacing:0) {
                    ForEach(0 ..< columns, id: \.self) { column in
                        content(row, column)
                    }
                }
            }
        }
    }

    init(rows: Int, columns: Int, @ViewBuilder content: @escaping (Int, Int) -> Content) {
        self.rows = rows
        self.columns = columns
        self.content = content
    }
}
public let drag = DragGesture( coordinateSpace: .global).onChanged { A in
    NSLog("x : \(A.location.x) , y : \(A.location.y)")
}
    .onEnded { _ in }
  
struct customWiBandsToggleStyle: ToggleStyle {
    @Binding var edit: Bool
    func makeBody(configuration: Configuration) -> some View {
        return HStack {
            configuration.label
                .font(.InterRegular18)
                .foregroundColor(Color.c0x242527)
                //.foregroundColor(edit ? Color(hex:0x313030) : Color(hex:0xC4C4C4))
            Spacer()
            if edit {
                Image(configuration.isOn ? "disable" : "radioEnable")
                    .resizable()
                    .frame(width:18,height: 16)
                    .gesture(drag)
            }
            else
            {
                Image(configuration.isOn ? "revDisable" : "reradioEnable")
                    .resizable()
                    .frame(width:18,height: 16)
                    .gesture(drag)
            }
            Text("Radio")
                .font(.InterRegular14)
                .foregroundColor(edit ? Color.c0x242527 : Color.c0x242527_30)
                .gesture(drag)
//            Text("Off")
//                .font(.SophosSansRegular_14)
//                .foregroundColor(edit ? Color(hex:0x313030) : Color(hex:0xC4C4C4))
            Button(action: { configuration.isOn.toggle() } )
            {
                if edit {
                    Image(configuration.isOn ? "ToggleONedit":"ToggleOFFedit" )
                        .gesture(drag)
                           
                }
                else
                {
                    Image(configuration.isOn ? "ToggleON":"ToggleOFF")
                        .gesture(drag)
                }
            }
            
//            Text("On")
//                .font(edit ? .SophosSansSemiBold_14:.SophosSansRegular_14)
//                .foregroundColor(edit ? Color(hex:0x000000) : Color(hex:0xC4C4C4))
        }.frame(height: 45.0)
            .gesture(drag)
    }
}
struct customDefault1ToggleStyle: ToggleStyle {
    @Binding var edit: Bool
    func makeBody(configuration: Configuration) -> some View {
        return HStack {
            configuration.label
                .font(.InterRegular14)
                .foregroundColor(Color.c0x242527)
                .gesture(drag)
            Spacer()
            
//            Text("Off")
//                .font(.SophosSansRegular_14)
//                .foregroundColor(configuration.isOn ? Color(hex:0x313030) : Color(hex:0xC4C4C4))
            Button(action: { configuration.isOn.toggle() } )
            {
                if edit {
                    Image(configuration.isOn ? "ToggleONedit":"ToggleOFFedit" )
                        .gesture(drag)
                        
                }
                else
                {
                    Image(configuration.isOn ? "ToggleON":"ToggleOFF")
                        .gesture(drag)
                }
            }
//            Text("On")
//                .font(.SophosSansRegular_14)
//                .foregroundColor(configuration.isOn ? Color(hex:0x000000) : Color(hex:0xC4C4C4))
        }.frame(height: 25.0)
            .gesture(drag)
    }
}

struct customNTP1ToggleStyle: ToggleStyle {
    @Binding var edit: Bool
    func makeBody(configuration: Configuration) -> some View {
        return HStack {
            configuration.label
                .font(.InterRegular14)
                .foregroundColor(edit ? Color.c0x242527 : Color.c0x242527)
                .gesture(drag)
            Spacer()
            
//            Text("Disable")
//                .font(.SophosSansRegular_14)
//                .foregroundColor(edit ? Color(hex:0x313030) : Color(hex:0xC4C4C4))
//            //Image(configuration.isOn ? "on" : "off")
//            //    .onTapGesture { configuration.isOn.toggle() }
            Button(action: { configuration.isOn.toggle() } )
            {
                if edit {
                    Image(configuration.isOn ? "ToggleONedit":"ToggleOFFedit" )
                        .gesture(drag)
                        
                }
                else
                {
                    Image(configuration.isOn ? "ToggleON":"ToggleOFF")
                        .gesture(drag)
                }
            }
//            Text("Enable")
//                .font(.SophosSansRegular_14)
//                .foregroundColor(edit ? Color(hex:0x000000) : Color(hex:0xC4C4C4))
        }.frame(height: 25.0)
            .gesture(drag)
    }
}
struct customSettingsToggleStyle: ToggleStyle {
    @Binding var edit: Bool
    var index = 0
    @StateObject var datas = ManageDatas()
    func makeBody(configuration: Configuration) -> some View {
        return HStack {
            configuration.label
                .font(.InterRegular14)
                .foregroundColor(configuration.isOn ? Color.c0x242527 : Color.c0x242527)
            Spacer()
            
            
//            Button(action: { configuration.isOn.toggle() } )
//            {
//                RoundedRectangle(cornerRadius: 15, style: .circular)
//                    .fill(edit ? Color(hex:0x1987CB) : Color(hex:0xC4C4C4))
//                    .frame(width: 40, height: 20)
//                    .overlay(
//                        Circle()
//                            .fill(Color.white)
//                            .shadow(radius: 1, x: 0, y: 1)
//                            .padding(1.5)
//                            .offset(x: configuration.isOn ? 10 : -10))
//                    .animation(Animation.easeInOut(duration: 0.1))
//            }
            Button(action: {
                configuration.isOn.toggle()
               
                if index == 0
                {
                    datas.localStore.set(edit,forKey:"faceid")
                    if edit == false
                    {
                        datas.localStore.set(edit,forKey:"passcode")
                    }
                }
                else if index == 1
                {
                    datas.localStore.set(edit,forKey:"touchid")
                    if edit == false
                    {
                        datas.localStore.set(edit,forKey:"passcode")
                    }
                }
                else if index == 2
                {
                    datas.localStore.set(edit,forKey:"passcode")
                }
                
                
            } )
            {
                
                    Image(edit ? "ToggleONedit":"ToggleOFFedit" )
                        
               
            }
           
        }.frame(height: 45.0)
    }
}

//struct ColoredToggleStyle: ToggleStyle {
//    var label = ""
//    var onColor = Color(UIColor.green)
//    var offColor = Color(UIColor.systemGray5)
//    var thumbColor = Color.white
//
//    func makeBody(configuration: Self.Configuration) -> some View {
//        HStack {
//            Text(label)
//            Spacer()
//            Button(action: { configuration.isOn.toggle() } )
//            {
//                RoundedRectangle(cornerRadius: 16, style: .circular)
//                    .fill(configuration.isOn ? onColor : offColor)
//                    .frame(width: 50, height: 29)
//                    .overlay(
//                        Circle()
//                            .fill(thumbColor)
//                            .shadow(radius: 1, x: 0, y: 1)
//                            .padding(1.5)
//                            .offset(x: configuration.isOn ? 10 : -10))
//                    .animation(Animation.easeInOut(duration: 0.1))
//            }
//        }
//        .font(.title)
//        .padding(.horizontal)
//    }
//}
/*RaioGroup*/
struct ColorInvert: ViewModifier {

    @Environment(\.colorScheme) var colorScheme

    func body(content: Content) -> some View {
        Group {
            if colorScheme == .dark {
                content.colorInvert()
            } else {
                content
            }
        }
    }
}

struct AuthButtonStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .foregroundColor(configuration.isPressed ? Color.c0x005BC8:Color.c0x242527)
            .background(Color.white)
    }
}
struct RadioButton: View {

    @Environment(\.colorScheme) var colorScheme

    let id: String
    let callback: (String)->()
    let selectedID : String
    let size: CGFloat
    let color: Color
    let textSize: CGFloat

    init(
        _ id: String,
        callback: @escaping (String)->(),
        selectedID: String,
        size: CGFloat = 20,
        color: Color = Color.primary,
        textSize: CGFloat = 14
        ) {
        self.id = id
        self.size = size
        self.color = color
        self.textSize = textSize
        self.selectedID = selectedID
        self.callback = callback
    }

    var body: some View {
        Button(action:{
            self.callback(self.id)
        }) {
            HStack(alignment: .center, spacing: 10) {
                Image(self.selectedID == self.id ? "radioBtn" : "radioNoBtn")
                    .resizable()
                    .frame(width: 20, height: 20)
               Text(id)
                    .font(.InterRegular13)
                    .frame(width: 280,height:30,alignment:.leading)
                    .padding(.leading,5)
                    
                Spacer()
            }
        }
        .padding(.leading , 20)
        //.buttonStyle(AuthButtonStyle())
        Divider()
            .padding(.leading , 40)
            .padding(.trailing,20)
    }
}
struct RadioButtonGroup: View {

    let items : [String]

    @State var selectedId: String = ""

    let callback: (String) -> ()

    var body: some View {
        VStack {
            ForEach(0..<items.count) { index in
                RadioButton(self.items[index], callback: self.radioGroupCallback, selectedID: self.selectedId)
                    .accessibility(addTraits: .isButton)
                    .accessibilityIdentifier("auth\(index)")
                    .buttonStyle(AuthButtonStyle())
            }
        }
    }

    func radioGroupCallback(id: String) {
        selectedId = id
        callback(id)
    }
}
/*checkbox*/
struct CheckboxStyle: ToggleStyle {
    @Binding var isON:Bool
    func makeBody(configuration: Self.Configuration) -> some View {

        return HStack {
            Button(action: { configuration.isOn.toggle()
                
            } )
            {
                Image(systemName: isON ? "checkmark.square.fill":"square")
                .resizable()
                .frame(width: 24, height: 24)
                .padding(.leading,0)
                .foregroundColor(isON  ? Color.c0x005BC8 : Color.c0x005BC8)
                .font(.system(size: 20, weight: .regular, design: .default))
                
                configuration.label
                    .padding(.leading,-15)
            }
        }
        
        //.onTapGesture { configuration.isOn.toggle() }

    }
}
/*Ping Test*/
func timeString(time: Int) -> String {
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        //return String(format:"%02i minutes %02i seconds left", minutes, seconds)
    return String(format:"%1i seconds left", seconds)
 }
extension String {
func isIPv4() -> Bool {
    var sin = sockaddr_in()
    return self.withCString({ cstring in inet_pton(AF_INET, cstring, &sin.sin_addr) }) == 1
}

func isIPv6() -> Bool {
    var sin6 = sockaddr_in6()
    return self.withCString({ cstring in inet_pton(AF_INET6, cstring, &sin6.sin6_addr) }) == 1
}

func isIpAddress() -> Bool { return self.isIPv6() || self.isIPv4() }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }

    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }

    func sizeOfString(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        return self.size(withAttributes: fontAttributes)
    }
    var binaryToInt: Int { return Int(strtoul(self, nil, 2)) }
    var integerValue: Int { return Int(self) ?? 0 }
    func match(_ regex: String) -> [[String]] {
            let nsString = self as NSString
            return (try? NSRegularExpression(pattern: regex, options: []))?.matches(in: self, options: [], range: NSMakeRange(0, nsString.length)).map { match in
                (0..<match.numberOfRanges).map { match.range(at: $0).location == NSNotFound ? "" : nsString.substring(with: match.range(at: $0)) }
            } ?? []
        }
    func regex (pattern: String) -> [String] {
        do {
          let regex = try NSRegularExpression(pattern: pattern, options:[])
          let nsstr = self as NSString
          let all = NSRange(location: 0, length: nsstr.length)
          var matches : [String] = [String]()
            regex.enumerateMatches(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: all) {
            (result : NSTextCheckingResult?, _, _) in
            if let r = result {
                let result = nsstr.substring(with: r.range) as String
              matches.append(result)
            }
          }
          return matches
        } catch {
          return [String]()
        }
      }
}
extension Int {
    var binaryString: String { return String(self, radix: 2) }
}
//extension String: Identifiable {
//    public var id: String {
//        self
//    }
//    
//}
struct GeometryGetter: View {
    @Binding var rect: CGRect

    var body: some View {
        GeometryReader { geometry in
            Group { () -> AnyView in
                DispatchQueue.main.async {
                    self.rect = geometry.frame(in: .global)
                }

                return AnyView(Color.clear)
            }
        }
    }
}
struct MarqueeText : View {
@State var text = ""
@State private var animate = false
    private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)

var body : some View {
    let stringWidth = text.widthOfString(usingFont: UIFont.systemFont(ofSize: 15))
    return ZStack {
            GeometryReader { geometry in
                Text(self.text).lineLimit(1)
                    .font(.InterRegular14)
                    .foregroundColor(Color.c0x242527)
                    .font(.subheadline)
                    .offset(x: self.animate ? -stringWidth * 1 : 0)
                    .animation(self.animationOne)
                    .onAppear() {
                        if geometry.size.width < stringWidth {
                             self.animate = true
                        }
                     
                }
                .fixedSize(horizontal: true, vertical: false)
                .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .topLeading)
                
                Text(self.text).lineLimit(1)
                    .font(.InterRegular14)
                    .foregroundColor(Color.c0x242527)
                    .font(.subheadline)
                    .offset(x: self.animate ? 0 : stringWidth * 3)
                    .animation(self.animationOne)
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .topLeading)
            }
        }
}
}
struct MarqueeWiBandsText : View {
@State var text = ""
@State private var animate = false
    private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)

var body : some View {
    let stringWidth = text.widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    return ZStack {
            GeometryReader { geometry in
                
                    Text(self.text).lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(Color.c0x242527)
                        .font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 1 : 0)
                        .animation(self.animationOne)
                        .onAppear() {
                            if geometry.size.width < stringWidth {
                                 self.animate = true
                            }
                    }
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                  
                    Text(self.text).lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(self.animate ? Color.c0x242527:Color.clear)
                        .font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 2)
                        .animation(self.animationOne)
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
                
            }
    }
}
}
struct MarqueeSystemText : View {
    @StateObject var Detail = AppDetailModel()
    var width = CGFloat(150.0)
@State private var animate = false
    private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)

var body : some View {
    let stringWidth = Detail.system[1].value!.widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    if width - stringWidth < 0 {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) true")
        DispatchQueue.main.async {
         self.animate = true
            
        }
    }
    else if width - stringWidth >= 0
    {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) false")
        DispatchQueue.main.async {
        self.animate = false
        }
    }
    return ZStack {
          //  GeometryReader { geometry in
                
                Text(Detail.system[1].value!).lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(Color.c0x242527)
                       // .font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 1 : 0)
                        .animation(self.animate ? self.animationOne : .default )
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                  
                    Text(Detail.system[1].value!).lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(self.animate ? Color.c0x242527:Color.clear)
                        //.font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 2)
                        .animation(self.animate ? self.animationOne : .default )
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
                
       //     }
    }
}
}
struct MarqueeSystemDNSText : View {
    @StateObject var Detail = AppDetailModel()
    var width = CGFloat(150.0)
@State private var animate = false
    private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)

var body : some View {
    let stringWidth = Detail.system[9].value!.widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    if width - stringWidth < 0 {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) true")
        DispatchQueue.main.async {
         self.animate = true
            
        }
    }
    else if width - stringWidth >= 0
    {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) false")
        DispatchQueue.main.async {
        self.animate = false
        }
    }
    return ZStack {
          //  GeometryReader { geometry in
                
                Text(Detail.system[9].value!).lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(Color.c0x242527)
                       // .font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 1 : 0)
                        .animation(self.animate ? self.animationOne : .default )
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                  
                    Text(Detail.system[9].value!).lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(self.animate ? Color.c0x242527:Color.clear)
                        //.font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 2)
                        .animation(self.animate ? self.animationOne : .default )
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
                
       //     }
    }
}
}
struct MarqueeSystemIPV6Text : View {
    @StateObject var Detail = AppDetailModel()
    var width = CGFloat(150.0)
@State private var animate = false
    private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)

var body : some View {
    let stringWidth = Detail.system[11].value!.widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    if width - stringWidth < 0 {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) true")
        DispatchQueue.main.async {
         self.animate = true
            
        }
    }
    else if width - stringWidth >= 0
    {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) false")
        DispatchQueue.main.async {
        self.animate = false
        }
    }
    return ZStack {
          //  GeometryReader { geometry in
                
                Text(Detail.system[11].value!).lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(Color.c0x242527)
                       // .font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 1 : 0)
                        .animation(self.animate ? self.animationOne : .default )
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                  
                    Text(Detail.system[11].value!).lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(self.animate ? Color.c0x242527:Color.clear)
                        //.font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 2)
                        .animation(self.animate ? self.animationOne : .default )
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
                
       //     }
    }
}
}
struct MarqueeSystemIPV6LinkText : View {
    @StateObject var Detail = AppDetailModel()
    var width = CGFloat(150.0)
@State private var animate = false
    private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)

var body : some View {
    let stringWidth = Detail.system[12].value!.widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    if width - stringWidth < 0 {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) true")
        DispatchQueue.main.async {
         self.animate = true
            
        }
    }
    else if width - stringWidth >= 0
    {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) false")
        DispatchQueue.main.async {
        self.animate = false
        }
    }
    return ZStack {
          //  GeometryReader { geometry in
                
                Text(Detail.system[12].value!).lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(Color.c0x242527)
                       // .font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 1 : 0)
                        .animation(self.animate ? self.animationOne : .default )
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                  
                    Text(Detail.system[12].value!).lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(self.animate ? Color.c0x242527:Color.clear)
                        //.font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 2)
                        .animation(self.animate ? self.animationOne : .default )
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
                
       //     }
    }
}
}
struct MarqueeWiBands24GText : View {
@StateObject var Detail = AppDetailModel()
    @State var index:Int
    var width = CGFloat(150.0)
@State private var animate = false
    private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)

var body : some View {
    let stringWidth = "\(Detail.wireless24G[index].value!)".widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    if width - stringWidth < 0 {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) true")
        DispatchQueue.main.async {
         self.animate = true
            
        }
    }
    else if width - stringWidth >= 0
    {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) false")
        DispatchQueue.main.async {
        self.animate = false
        }
    }
    return ZStack {
          //  GeometryReader { geometry in
                
                Text("\(Detail.wireless24G[index].value!)").lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(Color.c0x242527)
                       // .font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 1 : 0)
                        .animation(self.animate ? self.animationOne : .default )
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                  
                    Text("\(Detail.wireless24G[index].value!)").lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(self.animate ? Color.c0x242527:Color.clear)
                        //.font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 2)
                        .animation(self.animate ? self.animationOne : .default )
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
                
       //     }
    }
}
}
struct MarqueeWiBands5GText : View {
@StateObject var Detail = AppDetailModel()
    @State var index:Int
    var width = CGFloat(150.0)
@State private var animate = false
    private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)

var body : some View {
    let stringWidth = "\(Detail.wireless5G[index].value!)".widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    if width - stringWidth < 0 {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) true")
        DispatchQueue.main.async {
         self.animate = true
            
        }
    }
    else if width - stringWidth >= 0
    {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) false")
        DispatchQueue.main.async {
        self.animate = false
        }
    }
    return ZStack {
          //  GeometryReader { geometry in
                
                Text("\(Detail.wireless5G[index].value!)").lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(Color.c0x242527)
                        //.font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 1 : 0)
                        .animation(self.animate ? self.animationOne : .default )
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                  
                    Text("\(Detail.wireless5G[index].value!)").lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(self.animate ? Color.c0x242527:Color.clear)
                        //.font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 2)
                        .animation(self.animate ? self.animationOne : .default )
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
                
       //     }
    }
}
}
struct MarqueeWiBands6GText : View {
@StateObject var Detail = AppDetailModel()
    @State var index:Int
    var width = CGFloat(150.0)
@State private var animate = false
    private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)

var body : some View {
    let stringWidth = "\(Detail.wireless6G[index].value!)".widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    if width - stringWidth < 0 {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless6G[index].value!) true")
        DispatchQueue.main.async {
         self.animate = true
            
        }
    }
    else if width - stringWidth >= 0
    {
       // NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless6G[index].value!) false")
        DispatchQueue.main.async {
        self.animate = false
        }
    }
    return ZStack {
           // GeometryReader { geometry in
                
                Text("\(Detail.wireless6G[index].value!)").lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(Color.c0x242527)
                       // .font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 1 : 0)
                        .animation(self.animate ? self.animationOne : .default )
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                  
                    Text("\(Detail.wireless6G[index].value!)").lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(self.animate ? Color.c0x242527:Color.clear)
                      //  .font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 2)
                        .animation(self.animate ? self.animationOne : .default )
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
                
         //   }
    }
}
}
struct MarqueeTimeZoneText : View {
    @StateObject var Detail = AppDetailModel()
    var width = CGFloat(150.0)
@State private var animate = false
    @State private var stringWidth = 200.0
    @State private var animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)
    @State private var bool = false
    @State private var time_zone = "(GMT-12:00) Eniwetok, Kwajalein"

var body : some View {
    
 //   var time_zone = "(GMT-12:00) Eniwetok, Kwajalein"
//    if Detail.getDateAndTimeResult != nil
//    {
//    time_zone = TimeZoneList[(Detail.getDateAndTimeResult?.data?.time_zone)!]
//    }
//    let stringWidth =  time_zone.widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    if !bool {
        //if Detail.getDateAndTimeResult != nil
        //{
        
        //}
        DispatchQueue.main.async {
            time_zone = Detail.TimeZone//TimeZoneList[(Detail.getDateAndTimeResult?.data?.time_zone)!]
        stringWidth = time_zone.widthOfString(usingFont: UIFont.systemFont(ofSize: 24))
        animationOne = Animation.linear(duration: Double(stringWidth) / 30).delay(3.5).repeatForever(autoreverses: false)
        bool = true
        }
    }
    if width - stringWidth < 0 {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) true")
        DispatchQueue.main.async {
         self.animate = true
            
        }
    }
    else if width - stringWidth >= 0
    {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) false")
        DispatchQueue.main.async {
        self.animate = false
        }
    }
    return ZStack {
          //  GeometryReader { geometry in
                
                Text(time_zone).lineLimit(1)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                       // .font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 0.8 : 0)
                        .animation(self.animate ? self.animationOne : .default )
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                  
                    Text(time_zone).lineLimit(1)
                        .font(.InterRegular14)
                        .foregroundColor(self.animate ? Color.c0x242527:Color.clear)
                        //.font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 0.8)
                        .animation(self.animate ? self.animationOne : .default )
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
                
       //     }
    }
}
}
struct MarqueeTitle0Text : View {
@State var text = ""
@State private var animate = false
private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)

var body : some View {
    let stringWidth = text.widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    return ZStack {
            GeometryReader { geometry in
                Text(self.text).lineLimit(1)
                    .font(.InterMedium12)
                    .foregroundColor(Color.black)
                    .font(.subheadline)
                    .offset(x: self.animate ? -stringWidth * 1 : 0)
                    .animation(self.animationOne)
                    .onAppear() {
                        if geometry.size.width < stringWidth {
                             self.animate = true
                        }
                     
                }
                .fixedSize(horizontal: true, vertical: false)
               // .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .topLeading)
                
                Text(self.text).lineLimit(1)
                    .font(.InterMedium12)
                    .foregroundColor(Color.black)
                    .font(.subheadline)
                    .offset(x: self.animate ? 0 : stringWidth * 3)
                    .animation(self.animationOne)
                    .fixedSize(horizontal: true, vertical: false)
                 //   .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .topLeading)
            }
        }
}
}
struct MarqueeTitleText : View {
@State var text = ""
@State private var animate = false
private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)

var body : some View {
    let stringWidth = text.widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    return ZStack {
            GeometryReader { geometry in
                Text(self.text).lineLimit(1)
                    .font(.InterMedium12)
                    .foregroundColor(Color.black)
                    .offset(x: self.animate ? -stringWidth * 1 : 0)
                    .animation(self.animationOne)
                    .onAppear() {
                        if geometry.size.width < stringWidth {
                             self.animate = true
                        }
                     
                }
                .fixedSize(horizontal: true, vertical: false)
               // .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .topLeading)
                
                Text(self.text).lineLimit(1)
                    .font(.InterMedium12)
                    .foregroundColor(Color.black)
                    .offset(x: self.animate ? 0 : stringWidth * 3)
                    .animation(self.animationOne)
                    .fixedSize(horizontal: true, vertical: false)
                 //   .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .topLeading)
            }
        }
}
}
struct MarqueeDetailTitleText : View {
    @StateObject var backPress = ManageViews.shared
    var width = CGFloat(150.0)
    @State private var animate = false
    @State private var stringWidth = 200.0
    @State private var animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)
    @State private var bool = false
  
var body : some View {
   
    if !bool {
        DispatchQueue.main.async {
        stringWidth = backPress.accessDetail.appname!.widthOfString(usingFont: UIFont.systemFont(ofSize: 24))
        animationOne = Animation.linear(duration: Double(stringWidth) / 30).delay(3.5).repeatForever(autoreverses: false)
        bool = true
        }
    }
    if width - stringWidth < 0 {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) true")
        DispatchQueue.main.async {
         self.animate = true
            
        }
    }
    else if width - stringWidth >= 0
    {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) false")
        DispatchQueue.main.async {
        self.animate = false
        }
    }
    return ZStack {
            //GeometryReader { geometry in
                Text(backPress.accessDetail.appname!).lineLimit(1)
                        .font(.InterBold24)
                        .foregroundColor(Color.white)
                       // .font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 1.5 : 0)
                        .animation(self.animate ? animationOne : .default )
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                  
                    Text(backPress.accessDetail.appname!).lineLimit(1)
                        .font(.InterBold24)
                        .foregroundColor(self.animate ? Color.white:Color.clear)
                        //.font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 1.5)
                        .animation(self.animate ? animationOne : .default )
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
           // }
        }
}
}
struct AdaptsToKeyboard: ViewModifier {
    @State var currentHeight: CGFloat = 0
    
    func body(content: Content) -> some View {
        GeometryReader { geometry in
            content
                .padding(.bottom, self.currentHeight)
                .onAppear(perform: {
                    NotificationCenter.Publisher(center: NotificationCenter.default, name: UIResponder.keyboardWillShowNotification)
                        .merge(with: NotificationCenter.Publisher(center: NotificationCenter.default, name: UIResponder.keyboardWillChangeFrameNotification))
                        .compactMap { notification in
                            withAnimation(.easeOut(duration: 0.16)) {
                                notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
                            }
                    }
                    .map { rect in
                        rect.height - geometry.safeAreaInsets.bottom
                    }
                    .subscribe(Subscribers.Assign(object: self, keyPath: \.currentHeight))
                    
                    NotificationCenter.Publisher(center: NotificationCenter.default, name: UIResponder.keyboardWillHideNotification)
                        .compactMap { notification in
                            CGFloat.zero
                    }
                    .subscribe(Subscribers.Assign(object: self, keyPath: \.currentHeight))
                })
        }
    }
}

extension View {
    func adaptsToKeyboard() -> some View {
        return modifier(AdaptsToKeyboard())
    }
}
extension Binding {
     func toUnwrapped<T>(defaultValue: T) -> Binding<T> where Value == Optional<T>  {
        Binding<T>(get: { self.wrappedValue ?? defaultValue }, set: { self.wrappedValue = $0 })
    }
}
let releaseDate = Date()
    
let stackDateFormat: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    return formatter
}()
func uptimeString(inputDate: String) -> String {
    NSLog("Systm information uptime0")
    let day = inputDate.components(separatedBy: " days ")
    let uptime = inputDate.regex(pattern: "\\d{2}")
    if uptime.count == 3
    {
        var uptime0 = 0, uptime1 = 0, uptime2 = 0
        uptime2 = Int(uptime[2])! + 1
        if uptime2 == 60
        {
            uptime2 = 0
            uptime1 = Int(uptime[1])! + 1
            if uptime1 == 60
            {
                uptime1 = 0
                uptime0 = Int(uptime[0])! + 1
                if uptime0 == 24
                {
                    if day.count == 2
                    {
                        return "\(Int(day[0])! + 1) days 00:\(String(format: "%02d",uptime1)):\(String(format: "%02d",uptime2))"
                    }
                    else
                    {
                        let day = inputDate.components(separatedBy: " day ")
                        if day.count == 2
                        {
                            return "2 days 00:\(String(format: "%02d",uptime1)):\(String(format: "%02d",uptime2))"
                        }
                        else
                        {
                            return "1 day 00:\(String(format: "%02d",uptime1)):\(String(format: "%02d",uptime2))"
                        }
                    }
                }
            }
            else
            {
                uptime0 = Int(uptime[0])!
            }
        }
        else
        {
            uptime0 = Int(uptime[0])!
            uptime1 = Int(uptime[1])!
        }
      //  print("\(String(format: "%02d",uptime0)):\(String(format: "%02d",uptime1)):\(String(format: "%02d",uptime2))")
      //  let day = inputDate.components(separatedBy: " days ")
        if day.count == 2
        {
            return "\(day[0]) days \(String(format: "%02d",uptime0)):\(String(format: "%02d",uptime1)):\(String(format: "%02d",uptime2))"
        }
        else
        {
            let day = inputDate.components(separatedBy: " day ")
            if day.count == 2
            {
                return "\(day[0]) day \(String(format: "%02d",uptime0)):\(String(format: "%02d",uptime1)):\(String(format: "%02d",uptime2))"
            }
        }
//        else
//        {
//            if inputDate.contains("day")
//            {
//                let day = inputDate.split(separator: "s")
//                return "\(String(format: "%02d",uptime0)):\(String(format: "%02d",uptime1)):\(String(format: "%02d",uptime2))"
//            }
//        }
        NSLog("Systm information uptime1")
        return "\(String(format: "%02d",uptime0)):\(String(format: "%02d",uptime1)):\(String(format: "%02d",uptime2))"
    }
    return inputDate
 }
func systemTimeString(inputDate: String) -> String {
    NSLog("Systm information system time0")
    let newinput = inputDate.split(separator: " ")
    let yymmdd = String(newinput[0]).replacingOccurrences(of: "/", with: "-")
    let uptime = String(newinput[1]).regex(pattern: "\\d{2}")
    if uptime.count == 3
    {
        var uptime0 = 0, uptime1 = 0, uptime2 = 0
        uptime2 = Int(uptime[2])! + 1
        if uptime2 == 60
        {
            uptime2 = 0
            uptime1 = Int(uptime[1])! + 1
            if uptime1 == 60
            {
                uptime1 = 0
                uptime0 = Int(uptime[0])! + 1
            }
            else
            {
                uptime0 = Int(uptime[0])!
            }
        }
        else
        {
            uptime0 = Int(uptime[0])!
            uptime1 = Int(uptime[1])!
        }
        NSLog("Systm information system time1")
        return "\(yymmdd) \(String(format: "%02d",uptime0)):\(String(format: "%02d",uptime1)):\(String(format: "%02d",uptime2))"
    }
    return inputDate
 }
extension Bundle {
    func decode(_ file: String) -> Astronaut {
        guard let url = self.url(forResource: file, withExtension: nil) else {
            fatalError("Failed to locate \(file) in bundle.")
        }

        guard let data = try? Data(contentsOf: url) else {
            fatalError("Failed to load \(file) from bundle.")
        }
//        let str = String(decoding: data, as: UTF8.self)
//        print("data : \(str)")
        do {
//            // Create JSON Encoder
//            let encoder = JSONEncoder()
//
//            // Encode Note
//            let jsondata = try encoder.encode(data)
            
            let loaded = try Astronaut.init(jsonData: data) 
            // Write/Set Data
            //initLogin.init()
//            let decoder = JSONDecoder()
//
//            guard let loaded = try? decoder.decode(Astronaut.self, from: jsondata) else {
//                fatalError("Failed to decode \(file) from bundle.")
//            }

            return loaded
        } catch {
            print("Unable to Encode Note (\(error))")
        }
//        let decoder = JSONDecoder()
//
//        guard let loaded = try? decoder.decode(Astronaut.self, from: data) else {
//            fatalError("Failed to decode \(file) from bundle.")
//        }

        return Astronaut(NetworkIPv4Config: nil)
    }
}
struct Astronaut: Codable, Hashable {
    let NetworkIPv4Config: Info?
    
    init(NetworkIPv4Config: Info?)
    {
        self.NetworkIPv4Config =  NetworkIPv4Config
    }
}
struct Info: Codable, Hashable {
    let type: String?
    let properties: content?
    init(type: String?,properties: content?)
    {
        self.type = type
        self.properties = properties
    }
}
struct content: Codable, Hashable {
    let dhcp_lease_time:content1?
    init(dhcp_lease_time:content1?)
    {
        self.dhcp_lease_time = dhcp_lease_time
    }
}
struct content1: Codable, Hashable {
    let description:String?
    init(description:String?)
    {
        self.description = description
    }
}
extension Sequence where Element: Hashable {
    func uniqued() -> [Element] {
        var set = Set<Element>()
        return filter { set.insert($0).inserted }
    }
}
extension Array where Element: Hashable {
    var uniques: Array {
        var buffer = Array()
        var added = Set<Element>()
        for elem in self {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
}
func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
    var buffer = [T]()
    var added = Set<T>()
    for elem in source {
        if !added.contains(elem) {
            buffer.append(elem)
            added.insert(elem)
        }
    }
    return buffer
}
//extension View {
//    /// Applies the given transform if the given condition evaluates to `true`.
//    /// - Parameters:
//    ///   - condition: The condition to evaluate.
//    ///   - transform: The transform to apply to the source `View`.
//    /// - Returns: Either the original `View` or the modified `View` if the condition is `true`.
//    @ViewBuilder func `if`<Content: View>(_ condition: @autoclosure () -> Bool, transform: (Self) -> Content) -> some View {
//        if condition() {
//            transform(self)
//        } else {
//            self
//        }
//    }
//}
extension Data {
    var hexString: String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}
