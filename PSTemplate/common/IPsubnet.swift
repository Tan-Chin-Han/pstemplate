//
//  IPsubnet.swift
//  PSTemplate
//
//  Created by EDIOS on 2022/4/11.
//

import SwiftUI
import Network

let SubnetMask = ["128.0.0.0","","",]
var subnetMask = "N/A"
var maxHosts: Int = 2
var SubnetBit = ""
var result: [String] = ["", "", "", "", "", ""]
var networkClass: String = "?"
var errorMessage = ""
var support6G = ["AP6 420E","AP6 840E"]
let SSIDMin = 1
let SSIDMax = 32
let WPAMin = 8
let WPAMax = 63
let nonAlphanumericCharacters = CharacterSet.alphanumerics.inverted
let SSID840Array = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"]
let SSID420Array = ["1","2","3","4","5","6","7","8"]
let LeaseTimeArray = ["One Hour","One Day","One Week","One Month","Forever"]
let webLeaseTimeArray = ["Half Hour","One Hour","Two Hours","Half Day","One Day","Two Days","One Week","Two Weeks","One Month","Forever"]
let TimeZoneList: [String] = ["(GMT-12:00) Eniwetok, Kwajalein, International Date Line West","(GMT-11:00) Midway Island, Samoa","(GMT-10:00) Hawaii","(GMT-09:00) Alaska","(GMT-08:00) Pacific Time (US & Canada)","(GMT-07:00) Arizona","(GMT-07:00) Chihuahua, La Paz, Mazatian","(GMT-07:00) Mountain Time (US & Canada)","(GMT-06:00) Central America","(GMT-06:00) Central Time (US & Canada)","(GMT-06:00) Guadalajara, Mexico City, Monterrey","(GMT-06:00) Saskatchewan","(GMT-05:00) Bogota, Lima, Quito","(GMT-05:00) Eastern Time (US & Canada)","(GMT-05:00) Indiana (East)","(GMT-04:00) Atlantic Time (Canada)","(GMT-04:00) Caracas, La Paz","(GMT-04:00) Santiago","(GMT-03:30) Newfoundland","(GMT-03:00) Brasilia","(GMT-03:00) Buenos Aires, Georgetown","(GMT-03:00) Greenland","(GMT-02:00) Mid-Atlantic","(GMT-01:00) Azores","(GMT-01:00) Cape Verde Is.","(GMT) Casablanca, Monrovia","(GMT) Greenwich Mean Time: Dublin, Edinburgh, Lisbon, London","(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna","(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague","(GMT+01:00) Brussels, Copenhagen, Madrid, Paris","(GMT+01:00) Sarajevo, Sofija, Warsaw, Zagreb, Skopje, Vilnius","(GMT+01:00) West Central Africa","(GMT+02:00) Athens, Istanbul, Minsk","(GMT+02:00) Bucharest","(GMT+02:00) Cairo","(GMT+02:00) Harare, Pretoria","(GMT+02:00) Helsinki, Riga, Tallinn","(GMT+02:00) Jerusalem","(GMT+03:00) Baghdad","(GMT+03:00) Kuwait, Riyadh","(GMT+03:00) Moscow, St. Petersburg, Volgograd","(GMT+03:00) Nairobi","(GMT+03:30) Tehran","(GMT+04:00) Abu Dhabi, Muscat","(GMT+04:00) Baku, Tbilisi, Yerevan","(GMT+04:30) Kabul","(GMT+05:00) Ekaterinburg","(GMT+05:00) Islamabad, Karachi, Tashkent","(GMT+05:30) Calcutta, Chennai, Mumbai, New Delhi","(GMT+05:45) Kathmandu","(GMT+06:00) Almaty, Novosibirsk","(GMT+06:00) Astana, Dhaka","(GMT+06:00) Sri, Jayawardenepura","(GMT+06:30) Rangoon","(GMT+07:00) Bangkok, Hanoi, Jakarta","(GMT+07:00) Krasnoyarsk","(GMT+08:00) Beijing,  Hong Kong","(GMT+08:00) Irkutsk, Ulaan Bataar","(GMT+08:00) Kuala Lumpur, Singapore","(GMT+08:00) Perth","(GMT+08:00) Taipei, Taiwan","(GMT+09:00) Osaka, Sapporo, Tokyo","(GMT+09:00) Seoul","(GMT+09:00) Yakutsk","(GMT+09:30) Adelaide","(GMT+09:30) Darwin","(GMT+10:00) Brisbane","(GMT+10:00) Canberra, Melbourne, Sydney","(GMT+10:00) Guam, Port Moresby","(GMT+10:00) Hobart","(GMT+10:00) Vladivostok","(GMT+11:00) Magadan, Solamon, New Caledonia","(GMT+12:00) Auckland, Wllington","(GMT+12:00) Fiji, Kamchatka, Marshall Is."]

func calculatePressed(subnetMask:String, IPAddress:String) -> [String] {
    switch subnetMask {
    case "255.255.255.252":
        SubnetBit = "/30"
        maxHosts = 2
    case "255.255.255.248":
        SubnetBit = "/29"
        maxHosts = 6
    case "255.255.255.240":
        SubnetBit = "/28"
        maxHosts = 14
    case "255.255.255.224":
        SubnetBit = "/27"
        maxHosts = 30
    case "255.255.255.192":
        SubnetBit = "/26"
        maxHosts = 62
    case "255.255.255.128":
        SubnetBit = "/25"
        maxHosts = 126
    case "255.255.255.0":
        SubnetBit = "/24"
        maxHosts = 254
    case "255.255.254.0":
        SubnetBit = "/23"
        maxHosts = 510
    case "255.255.252.0":
        SubnetBit = "/22"
        maxHosts = 1022
    case "255.255.248.0":
        SubnetBit = "/21"
        maxHosts = 2046
    case "255.255.240.0":
        SubnetBit = "/20"
        maxHosts = 4094
    case "255.255.224.0":
        SubnetBit = "/19"
        maxHosts = 8190
    case "255.255.192.0":
        SubnetBit = "/18"
        maxHosts = 16382
    case "255.255.128.0":
        SubnetBit = "/17"
        maxHosts = 32766
    case "255.255.0.0":
        SubnetBit = "/16"
        maxHosts = 65534
    default:
        maxHosts = 0
        SubnetBit = ""
    }
    // Max Hosts END
    
    // Converts to binary array
    // Binary BEGIN
    
    //func calculatePressed() -> [String] { // returns result array
        
        func fill(a: String) -> String { // Fills zeros at the begining for the AND/OR processes
            var result = a
            while result.count < 8 {
                result.insert("0", at: result.startIndex)
            }
            return result
        }
        
        func reverse() -> [String] { // Reverses the subnet mask for OR (broadcast address) process
            
            var reversedSubnet: [String] = ["", "", "", ""]
            var part = 0
            
            let binarySubnet = convert(address: subnetMask)
            
            while part < 4 {
                
                let numberSubnet: String = binarySubnet[part]
                var i = 0
                
                while i < 8 {
                    let indexSubnet = numberSubnet.index(numberSubnet.startIndex, offsetBy: i)
                    
                    if intToBool(num: Int(numberSubnet[indexSubnet].description)) {
                        reversedSubnet[part].append("0")
                    } else {
                        reversedSubnet[part].append("1")
                    }
                
                    i += 1
                }
            
                part += 1
            }
            
            return reversedSubnet
            
        }
        
    
        func convertFromBinary(array: [String]) -> String { // Converts from binary to usable String (e.g. "192.168.1.1")
            
            var result: String = ""
            var i = 0
            while i < 4 {
                
                result.append(String(array[i].binaryToInt))
                if i < 3 {
                    result.append(".")
                }
                
                i += 1
            }
            
            return result
            
        }
        
        func intToBool(num: Int?) -> Bool { // converts from binary Int to Bool
            var result: Bool
            if num == 0 {
                result = false
            } else {
                result = true
            }
            return result
        }
        
        func rawToString(address: String?) -> String { // 1. - Unwraps text field
            
            var output = "0"
            if let unwrapped = address {
                output = unwrapped
            }
            if output.isEmpty {
                errorMessage = "One of the text fields is empty. Check your input and try again."
                
            }
            return output
        }
        
        func fetchFromString(address: String) -> [Int] { // 2. - Fetching from string to Int array
            
            var intArray: [Int]
            if !address.contains(".") {
                intArray = [0, 0, 0, 0]
                result[5] = "One of your addresses does not contain a period \".\""
                
            } else {
                let rawArray = address.components(separatedBy: ".")
                intArray = rawArray.map { $0.integerValue }
                if intArray.count < 4 {
                    intArray = [0, 0, 0, 0]
                    errorMessage = "One (or both) of your addresses is not long enough."
                   
                }
            }
            return intArray
            
        }
        
        
        func convertToBinary(array: [Int]) -> [String] { // 3. - Converting Int array to Binary String array
            
            var binaryArray: [String] = []
            var i = 0
            
            while i < 4 {
                let part = array[i].binaryString
                let fixedPart = fill(a: part)
                binaryArray.insert(fixedPart, at: (i))
                i += 1
            }
            
            return binaryArray
            
        }
        
        func convert(address: String?) -> [String] { // returns binary address
            
            return convertToBinary(array: fetchFromString(address: rawToString(address: address)))
            
        }
        
        // Binary END
        
        // Network Address calculation (AND)
        
        // Network BEGIN
        
        let binaryIP = convert(address: IPAddress)
        let binarySubnet = convert(address: subnetMask)
        
        func networkAddress() -> String { // returns usable network address
            
            var networkAddress: [String] = ["", "", "", ""]
            
            var part = 0
            
            while part < 4 { // breaking down the array
                
                let numberIP: String = binaryIP[part]
                let numberSubnet: String = binarySubnet[part]
                
                var i = 0
                
                while i < 8 {
                    let indexIP = numberIP.index(numberIP.startIndex, offsetBy: i)
                    let indexSubnet = numberSubnet.index(numberSubnet.startIndex, offsetBy: i)
                    
                    if intToBool(num: Int(numberIP[indexIP].description)) && intToBool(num: Int(numberSubnet[indexSubnet].description)) {
                        networkAddress[part].append("1")
                        
                    } else {
                        networkAddress[part].append("0")
                    }
                    
                    i += 1
                }
                
                part += 1
            }
            
            
            return convertFromBinary(array: networkAddress)
        }
        
        // Network END
        
        // Broadcast Address calculation (OR)
        
        // Broadcast BEGIN
        
        func broadcastAddress() -> String {
            
            
            let reversedSubnet = reverse()
            var broadcastAddress: [String] = ["", "", "", ""]
            
            var part = 0
            
            while part < 4 {
            
                let numberSubnet: String = reversedSubnet[part]
                let numberIP: String = binaryIP[part]
                
                var i = 0
                
                while i < 8 {
                    let indexIP = numberIP.index(numberIP.startIndex, offsetBy: i)
                    let indexSubnet = numberSubnet.index(numberSubnet.startIndex, offsetBy: i)
             
                    if intToBool(num: Int(numberIP[indexIP].description)) || intToBool(num: Int(numberSubnet[indexSubnet].description)) {
                        broadcastAddress[part].append("1")
                        
                    } else {
                        broadcastAddress[part].append("0")
                    }
                    
                    i += 1
                }
            
                
            part += 1
            }
            
            return convertFromBinary(array: broadcastAddress)
        }
        
        // Broadcast END
        
        // Start and End Host Address Calculation
        
        // Start and End Address BEGIN
        
        func startHostAddress() -> String {
            
            var intArray: [Int] = fetchFromString(address: networkAddress())
            
            if intArray[3] < 256 {
                intArray[3] += 1
            } else {
                intArray[2] += 1
                intArray[3] = 0
            }
            
            return convertFromBinary(array: convertToBinary(array: intArray))
            
        }
        
        func endHostAddress() -> String {
            
            var intArray: [Int] = fetchFromString(address: broadcastAddress())
            
            if intArray[3] < 0 {
                intArray[2] -= 1
                intArray[3] = 255
            } else {
                intArray[3] -= 1
            }
            
            return convertFromBinary(array: convertToBinary(array: intArray))
        }
        
        // Start and End Address END
        
        // Checks if it is possible to calculate + determines the Network Class
        
        // Class BEGIN
        
        var ipAddress = fetchFromString(address: rawToString(address: IPAddress))
        
        switch ipAddress[0] {
        case 1...126:
            networkClass = "A"
            errorMessage = ""
        case 127:
            networkClass = "N/A"
            errorMessage = "IP adresses of type 127.x.x.x are reserved for loopback."
            
        case 128...191:
            networkClass = "B"
            errorMessage = ""
        case 192...223:
            networkClass = "C"
            errorMessage = ""
        case 224...239:
            networkClass = "D"
            errorMessage = "Class D is reserved for multicasting, therefore there's no need to extract host address and Class D doesn't have any subnet mask."
            
        case 240...255:
            networkClass = "E"
            errorMessage = "Class E is reserved for experimental purposes and therefore it's not equipped with any subnet mask."
            
        default:
            networkClass = "N/A"
            errorMessage = "Unknown error. Please, double check your input."
            
        }
        
        // Class END
    
        // Result BEGIN
        if (networkClass == "A" || networkClass == "B" || networkClass == "C"){
            result = [startHostAddress(), endHostAddress(), networkAddress(), broadcastAddress(), networkClass, errorMessage] // [0 - Start Host Address, 1 - End Host Address, 2 - Network Address, 3 - Broadcast Address, 4 - Network Class, 5 - Error Message]
            
            if !errorMessage.isEmpty {
               
            }
        } else {
            result = ["N/A", "N/A", "N/A", "N/A", networkClass, errorMessage]
            
        }
    return result
}
func IPToInt(ip: String) -> Int {
    let octets: [Int] = ip.split(separator: ".").map({Int($0)!})
    var numValue: Int = 0
    for (i, n) in octets.enumerated() {
        let p: Int = NSDecimalNumber(decimal: pow(256, (3-i))).intValue
        numValue += n * p
    }
    return numValue
}
