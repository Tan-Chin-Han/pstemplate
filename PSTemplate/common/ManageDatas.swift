//
//  ManageDatas.swift
//  PSTemplate
//
//  Created by EDIOS on 2022/3/29.
//

import SwiftUI
import CoreData

class ManageDatas: ObservableObject {
//    @Published var secure = PSKeyStore() {
//        willSet {
//            self.objectWillChange.send()
//        }
//    }
    @Published var localStore = UserDefaults() {
        willSet {
            self.objectWillChange.send()
        }
    }
    
    func decodeLocalStore<T : Decodable>(from data : Data) throws -> T
    {
        return try JSONDecoder().decode(T.self, from: data)
    }
    public func deleteMyap(_ name: String) -> Bool
    {
//        var database = localStore.dictionary(forKey: "MultiMyAP") as? [String:Data]
//
//        return ((database?.removeValue(forKey: name)) != nil)
        
        guard var database = localStore.dictionary(forKey: "MultiMyAP") as? [String:Data],
              let databaseCheck = database[name] else { return false}
        
        for (key, value) in database {
            if key == name {
                //database.removeValue(forKey: name)
               // database.remove(at: database.index(forKey: name)!)
               
                if let value = database.removeValue(forKey: name) {
                ///         print("The value \(value) was removed.")
                    print(database[name])
                    localStore.set(database, forKey: "MultiMyAP")
                    return true
                }
                else
                {
                    return false
                }
            }
        }
        
        return false
    }
    public func getMyAPMatch( _ name: String) -> SSDP
    {
        let a = Data("{\"LOCATION\":\"\", \"SERVER\":\"\", \"ST\":\"\", \"USN\":\"\", \"Vendor\":\"\", \"Mac\":\"\",\"Model\":\"\", \"Name\":\"\", \"OperatorMode\":\"\", \"Identity\":\"\", \"FirmwareVersion\":\"\", \"CPUUtilization\":\"\", \"ConnectedUsers\":\"\", \"SNR\":\"\", \"MemoryUtilization\":\"\", \"WiFiRadio\":\"\",\"live\":\"\",\"SOPHOS_INFO\":\"\"}".utf8)
        var t : Decodable?
        do {
            t = try SSDP.init(jsonData: a)
        }
        catch
        {
            print(error)
        }
        let database = localStore.dictionary(forKey: "MultiMyAP") as? [String:Data]
        guard let database = localStore.dictionary(forKey: "MultiMyAP") as? [String:Data],
              let databaseCheck = database[name] else {
                  let creat = SSDP(LOCATION: "", SERVER: "", ST: "", USN: "", Vendor: "", Mac: "", Model: "", Name: "", OperatorMode: "", Identity: "", FirmwareVersion: "", CPUUtilization: "", ConnectedUsers: "", SNR: "", MemoryUtilization: "", WiFiRadio: "", live: "", SOPHOS_INFO: "")
                  var initLogin: [String :Data]?
                  do {
                      // Create JSON Encoder
                      let encoder = JSONEncoder()

                      // Encode Note
                      let data = try encoder.encode(creat)

                      // Write/Set Data
                      //initLogin.init()
                      initLogin = [name:data]
                      localStore.set(initLogin, forKey: "MultiMyAP")

                  } catch {
                      print("Unable to Encode Note (\(error))")
                  }
                  
                  return t as! SSDP
                  
              }
        print("databaseCheck : \(databaseCheck)")
        do {
            
            let data = try! decodeLocalStore(from: databaseCheck) as SSDP

            return data

        } catch {
            print("Unable to Encode Note (\(error))")
        }
        
        return t as! SSDP
    }
    public func checkMyAPAmount()->[String:Data]
    {
        let database = localStore.dictionary(forKey: "MultiMyAP") as? [String:Data]
        return database ?? ["":Data()]
    }
    public func checkMyAPMatch( _ name: String, _ newInfo: SSDP) -> Bool
    {
        guard let database = localStore.dictionary(forKey: "MultiMyAP") as? [String:Data],
          let databaseCheck = database[name] else { return false }

        do {
            // Create JSON Encoder
            let encoder = JSONEncoder()

            // Encode Note
            let data = try encoder.encode(newInfo)
            return databaseCheck == data
        } catch {
            print("Unable to Encode Note (\(error))")
        }
        
       return false
    }
    public func saveToMyAP( _ name: String, _ info: SSDP) -> Bool
    {
       // var newEntry : [String: SSDP]
        var database = localStore.dictionary(forKey: "MultiMyAP") as? [String:Data]
        guard var database = localStore.dictionary(forKey: "MultiMyAP") as? [String:Data],
              let databaseCheck = database[name] else {
                  
                  if database == nil
                  {
                     // let creat = login(admin_account: "", admin_pw: "", keep: 0)
                      var initMyAP: [String :Data]?
                      do {
                          // Create JSON Encoder
                          let encoder = JSONEncoder()

                          // Encode Note
                          let data = try encoder.encode(info)

                          // Write/Set Data
                          //initLogin.init()
                          initMyAP = [name:data]
                          localStore.set(initMyAP, forKey: "MultiMyAP")
                          return true
                      } catch {
                          print("Unable to Encode Note (\(error))")
                      }
                  }
                  else
                  {
                      do {
                          // Create JSON Encoder
                          let encoder = JSONEncoder()

                          // Encode Note
                          let data = try encoder.encode(info)

                          // Write/Set Data
                          //initLogin.init()
                         
                          database!.updateValue(data, forKey: name)
                          localStore.set(database, forKey: "MultiMyAP")
                          return true
                      } catch {
                          print("Unable to Encode Note (\(error))")
                      }
                  }
                  
                  return false
                  
              }
        do {
            // Create JSON Encoder
            let encoder = JSONEncoder()

            // Encode Note
            let data = try encoder.encode(info)

            // Write/Set Data
            //initLogin.init()

            database.updateValue(data, forKey: name)
            localStore.set(database, forKey: "MultiMyAP")
            return true
        } catch {
            print("Unable to Encode Note (\(error))")
        }
       
        return false
    }
    public func deleteLogin(_ name: String) -> Bool
    {
       // var database = localStore.dictionary(forKey: "MultiLogin") as? [String:Data]
        guard var database = localStore.dictionary(forKey: "MultiLogin") as? [String:Data],
              let databaseCheck = database[name] else { return false}
        
        for (key, value) in database {
            if key == name {
                //database.removeValue(forKey: name)
               // database.remove(at: database.index(forKey: name)!)
               
                if let value = database.removeValue(forKey: name) {
                ///         print("The value \(value) was removed.")
                    print(database[name])
                    localStore.set(database, forKey: "MultiLogin")
                    return true
                }
                else
                {
                    return false
                }
            }
        }
        
        return false
        
       // return ((database.removeValue(forKey: name)) != nil)
    }
    public func getLoginMatch( _ name: String) -> login
    {
        let a = Data("{\"admin_account\":\"\",\"admin_pw\":\"\",\"keep\":0}".utf8)
        var t : Decodable?
        do {
            t = try login.init(jsonData: a)
        }
        catch
        {
            print(error)
        }
        var database = localStore.dictionary(forKey: "MultiLogin") as? [String:Data]
        guard let database = localStore.dictionary(forKey: "MultiLogin") as? [String:Data],
              let databaseCheck = database[name] else {
                  let creat = login(admin_account: "", admin_pw: "", keep: 0)
                  var initLogin: [String :Data]?
                  do {
                      // Create JSON Encoder
                      let encoder = JSONEncoder()

                      // Encode Note
                      let data = try encoder.encode(creat)

                      // Write/Set Data
                      //initLogin.init()
                      if database == nil
                      {
                          database = [name:data]
                      }
                      else
                      {
                      database![name] = data
                      }
                      localStore.set(database, forKey: "MultiLogin")

                  } catch {
                      print("Unable to Encode Note (\(error))")
                  }
                  
                  return t as! login
                  
              }
        print("databaseCheck : \(databaseCheck)")
        do {
            
            let data = try! decodeLocalStore(from: databaseCheck) as login

            return data

        } catch {
            print("Unable to Encode Note (\(error))")
        }
        
        return t as! login
    }
    public func checkLoginAmount()->[String:Data]
    {
        let database = localStore.dictionary(forKey: "MultiLogin") as? [String:Data]
        return database!
    }
    public func checkLoginMatch( _ name: String, _ number: String) -> Bool
    {
        guard let database = localStore.dictionary(forKey: "MultiLogin") as? [String:String],
          let databaseCheck = database[name] else { return false }

       return databaseCheck == number
    }
    public func saveToLogin( _ name: String, _ info: login) -> Bool
    {
       // var newEntry : [String: login]
        let database = localStore.dictionary(forKey: "MultiLogin") as? [String:Data]
        guard var database = localStore.dictionary(forKey: "MultiLogin") as? [String:Data],
              let databaseCheck = database[name] else {
                  
                  return false
                  
              }
        do {
            
            let data = try! decodeLocalStore(from: databaseCheck) as login
            if data.admin_account == ""
            {
                var initLogin: [String :Data]?
               
                    // Create JSON Encoder
                    let encoder = JSONEncoder()

                    // Encode Note
                    let data = try encoder.encode(info)

                    // Write/Set Data
                    //initLogin.init()
                   
                    database[name] = data
                    localStore.set(database, forKey: "MultiLogin")
                    return true
               
            }
            else
            {
                let encoder = JSONEncoder()

                // Encode Note
                let data = try encoder.encode(info)

                // Write/Set Data
                //initLogin.init()
               
                database.updateValue(data, forKey: name)
                localStore.set(database, forKey: "MultiLogin")
                return true
            }
        }
        catch {
           print("Unable to Encode Note (\(error))")
       }
//        do {
//            // Create JSON Encoder
//            let encoder = JSONEncoder()
//
//            // Encode Note
//            let data = try encoder.encode(info)
//
//            // Write/Set Data
//            //initLogin.init()
//
//            database.updateValue(data, forKey: name)
//            localStore.set(database, forKey: "MultiLogin")
//            return true
//        } catch {
//            print("Unable to Encode Note (\(error))")
//        }
       
        return false
    }
    public func saveToCurrentEable( _ name: String, _ info: SSDP) -> Bool
    {
       // var newEntry : [String: SSDP]
        var database = localStore.dictionary(forKey: "EableMyAP") as? [String:Data]
        guard var database = localStore.dictionary(forKey: "EableMyAP") as? [String:Data],
              let databaseCheck = database[name] else {
                  
                  if database == nil
                  {
                     // let creat = login(admin_account: "", admin_pw: "", keep: 0)
                      var initMyAP: [String :Data]?
                      do {
                          // Create JSON Encoder
                          let encoder = JSONEncoder()

                          // Encode Note
                          let data = try encoder.encode(info)

                          // Write/Set Data
                          //initLogin.init()
                          initMyAP = [name:data]
                          localStore.set(initMyAP, forKey: "EableMyAP")
                          return true
                      } catch {
                          print("Unable to Encode Note (\(error))")
                      }
                  }
                  else
                  {
                      do {
                          // Create JSON Encoder
                          let encoder = JSONEncoder()

                          // Encode Note
                          let data = try encoder.encode(info)

                          // Write/Set Data
                          //initLogin.init()
                         
                          database!.updateValue(data, forKey: name)
                          localStore.set(database, forKey: "EableMyAP")
                          return true
                      } catch {
                          print("Unable to Encode Note (\(error))")
                      }
                  }
                  
                  return false
                  
              }
        do {
            // Create JSON Encoder
            let encoder = JSONEncoder()

            // Encode Note
            let data = try encoder.encode(info)

            // Write/Set Data
            //initLogin.init()
           
            database.updateValue(data, forKey: name)
            localStore.set(database, forKey: "EableMyAP")
            return true
        } catch {
            print("Unable to Encode Note (\(error))")
        }
     
        return false
    }
    public func getCurrentEableMatch( _ name: String) -> SSDP
    {
        let a = Data("{\"LOCATION\":\"\", \"SERVER\":\"\", \"ST\":\"\", \"USN\":\"\", \"Vendor\":\"\", \"Mac\":\"\",\"Model\":\"\", \"Name\":\"\", \"OperatorMode\":\"\", \"Identity\":\"\", \"FirmwareVersion\":\"\", \"CPUUtilization\":\"\", \"ConnectedUsers\":\"\", \"SNR\":\"\", \"MemoryUtilization\":\"\", \"WiFiRadio\":\"\",\"live\":\"\",\"SOPHOS_INFO\":\"\"}".utf8)
        var t : Codable?
        do {
            t = try SSDP.init(jsonData: a)
        }
        catch
        {
            print(error)
        }
        let database = localStore.dictionary(forKey: "EableMyAP") as? [String:Data]
        guard let database = localStore.dictionary(forKey: "EableMyAP") as? [String:Data],
              let databaseCheck = database[name] else {
                  let creat = SSDP(LOCATION: "", SERVER: "", ST: "", USN: "", Vendor: "", Mac: "", Model: "", Name: "", OperatorMode: "", Identity: "", FirmwareVersion: "", CPUUtilization: "", ConnectedUsers: "", SNR: "", MemoryUtilization: "", WiFiRadio: "", live: "", SOPHOS_INFO: "")
                  var initLogin: [String :Data]?
                  do {
                      // Create JSON Encoder
                      let encoder = JSONEncoder()

                      // Encode Note
                      let data = try encoder.encode(creat)

                      // Write/Set Data
                      //initLogin.init()
                      initLogin = [name:data]
                      localStore.set(initLogin, forKey: "EableMyAP")
                      return creat as! SSDP
                  } catch {
                      print("Unable to Encode Note (\(error))")
                  }
                  
                  return t as! SSDP
                  
              }
        print("databaseCheck : \(databaseCheck)")
        do {
            
            let data = try! decodeLocalStore(from: databaseCheck) as SSDP

            return data

        } catch {
            print("Unable to Encode Note (\(error))")
        }
        
        return t as! SSDP
    }
}

